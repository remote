package remote.apps.cli;

/**
 * A command for login in to a testbed server.
 */
public class Login extends Command implements Command.Authentication {

	/**
	 * Create a command for login in to a server.
	 */
	public Login()
	{
		super("login", "Log in to server");
	}

	protected void run()
	{
		printf("Logged in successfully%n");
		exit();
	}

}
