package remote.apps.cli;

import remote.client.spi.service.MoteDataService;

/**
 * A command for listing motes in a testbed.
 */
public class LsMotes extends Command implements MoteDataService.Callback<MoteDataService.Response> {

	/**
	 * Create a command for listing motes.
	 */
	public LsMotes()
	{
		super("ls-motes", "List motes for a server");
	}

	protected void run()
	{
		getServiceManager().get(MoteDataService.class).getMoteData(this);
	}

	public void onSuccess(MoteDataService.Response response)
	{
		showHeader(response);
		showData(response);
		exit();
	}

	public void onError(Throwable caught)
	{
		die("Failed to get mote data", caught);
	}

	private static void showData(MoteDataService.Response response)
	{
		for (int i = 0; i < response.getMoteSize(); i++) {
			printf("%nMote %d%n", i);
			for (MoteDataService.Attribute attribute : MoteDataService.Attribute.values()) {
				if (attribute == MoteDataService.Attribute.unknown)
					continue;
				String name = attribute.name();
				String value = response.getAttribute(i, attribute, "<unknown>");

				printf("  %-20s : %s%n", name, value);
			}
		}
	}

	private static void showHeader(MoteDataService.Response response)
	{
		printf("Column headers:%n");

		showHeader("Name", "Title", "Class", "Visible");
		showHeader("----", "-----", "-----", "-------");

		response.visitDescriptors(new MoteDataService.Visitor() {

			public boolean visit(String name, String title, String klass, boolean visible)
			{
				showHeader(name, title, klass, Boolean.toString(visible));
				return true;
			}

		});
	}

	private static void showHeader(String name, String title, String klass, String visible)
	{
		printf("%-20s %-20s %-20s %-7s%n", name, title, klass, visible);
	}

}
