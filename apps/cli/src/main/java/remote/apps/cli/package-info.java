/** Command Line Interface.
 *
 * Contains tools which allow access to Re·Mote Testbed Services from
 * the shell.
 */
package remote.apps.cli;
