package remote.apps.cli;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;
import org.kohsuke.args4j.spi.OptionHandler;
import org.kohsuke.args4j.spi.Parameters;
import org.kohsuke.args4j.spi.Setter;

/**
 * Command line option parser with custom option handlers.
 */
public class OptionParser extends CmdLineParser {

	static {
		registerHandler(Command.class, CommandOptionHandler.class);
	}

	/**
	 * Creates a new command line parser.
	 * @param bean object with annotated options.
	 */
	public OptionParser(Object bean)
	{
		super(bean);
	}

	public static class CommandOptionHandler extends OptionHandler<Command> {

		/**
		 * Create a new sub command option handler.
		 *
		 * @param parser for the command line.
		 * @param option definition.
		 * @param setter for updating the option.
		 */
		public CommandOptionHandler(CmdLineParser parser, OptionDef option,
					    Setter<Command> setter)
		{
			super(parser, option, setter);
		}

		@Override
		public int parseArguments(final Parameters params) throws CmdLineException
		{
			String string = params.getParameter(0);

			for (Command cmd : Command.getCommands())
				if (cmd.getName().equals(string)) {
					setter.addValue(cmd);
					owner.stopOptionParsing();
					return 1;
				}

			throw new CmdLineException("Unknown command '" + string + "'");
		}

		@Override
		public String getDefaultMetaVariable()
		{
			return "command";
		}

	}

}
