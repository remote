package remote.apps.cli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.spi.FileOptionHandler;
import remote.client.console.MoteConsoleListener;
import remote.client.control.Mote;
import remote.client.control.MoteListener;
import remote.client.control.MoteManager;
import remote.client.control.MoteManagerListener;
import remote.client.control.MoteResult;
import remote.client.session.Session;
import remote.client.session.SessionListener;

/**
 * Command for running a program on a mote.
 */
public class MoteExec extends Command implements Command.Authentication,
						  MoteListener, MoteConsoleListener {

	@Argument(index = 0, metaVar = "moteid", required = true)
	private Integer moteid;

	@Argument(index = 1, metaVar = "program", required = true, handler=FileOptionHandler.class)
	private File program;

	/**
	 * Create a command for executing a mote program.
	 */
	public MoteExec()
	{
		super("mote-exec", "moteid program", "Execute mote program");
	}

	protected void run()
	{
		Session session = getSession();
		session.addEventListener(new SessionListener() {

			public void onError(Session session, Throwable caught)
			{
				die("Error occurred", caught);
			}

		});

		session.getMoteManager().addEventListener(new MoteManagerListener() {

			public void onUpdate(MoteManager manager, Collection<Mote> added,
					     Collection<Mote> deleted, Collection<Mote> changed)
			{
				Iterator<Mote> motes = manager.getMotes();

				while (motes.hasNext()) {
					Mote mote = motes.next();

					if (mote.getId() != moteid)
						continue;

					mote.addEventListener(MoteExec.this);
					mote.getConsole().addEventListener(MoteExec.this);
					mote.control();
				}

			}

		});
	}

	private String readFile(File file)
	{
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException ex) {
			die("File not found", ex);
		}

		StringBuilder builder = new StringBuilder();
		char[] buffer = new char[1024];
		int bytes;
		try {
			while ((bytes = reader.read(buffer)) > 0)
				builder.append(String.valueOf(buffer, 0, bytes));
		} catch (IOException ex) {
			die("Failed to read " + file.toString(), ex);
		}

		return builder.toString();
	}

	public void onControl(Mote mote)
	{
		mote.program(readFile(program));
	}

	public void onProgram(Mote mote, MoteResult result)
	{
		if (result != MoteResult.SUCCESS)
			die("Failed to program mote");
		mote.start();
	}

	public void onStart(Mote mote, MoteResult result)
	{
		if (result != MoteResult.SUCCESS)
			die("Failed to start mote");
	}

	public void onStop(Mote mote, MoteResult result)
	{
		die("onStop was called");
	}

	public void onReset(Mote mote, MoteResult result)
	{
		die("onReset was called");
	}

	public void onLost(Mote mote)
	{
		die("Mote was lost");
	}

	public void onRead(Mote mote, byte[] data)
	{
		System.out.write(data, 0, data.length);
		System.out.flush();
	}

	public void onWrite(Mote mote)
	{
		die("onWrite was called");
	}

}
