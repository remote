package remote.apps.cli;

import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringOptionHandler;
import remote.apps.core.ApplicationProfileManager;
import remote.util.Version;

/**
 * Provides the basic system support required by the extendable client runtime
 * library.
 */
public class Main {

	@Option(name = "-v", aliases = { "--version" }, usage = "print version information")
	private boolean version;
	@Option(name = "-h", aliases = { "--help" }, usage = "print usage help")
	private boolean help;
	@Option(name = "-f", aliases = { "--config" }, usage = "testbed configuration profile",
		handler=StringOptionHandler.class, metaVar="name")
	private String profile;
	@Argument(index = 0, metaVar = "command", handler=OptionParser.CommandOptionHandler.class)
	private Command command = null;
	@Argument(index = 1)
	private List<String> arguments = new LinkedList<String>();

	private final OptionParser parser = new OptionParser(this);
	private static final Main option = new Main();
	private static final String CMDHOME =
		System.getProperty("user.home") + "/.remote-testbed/";

	/**
	 * Main entry point for the command line tools.
	 * @param args given on the command line.
	 */
	public static void main(String[] args)
	{
		PrintStream out = System.err;
		HashMap<String, String> settings = new HashMap<String, String>();

		try {
			option.parser.parseArgument(args);

			if (option.help || option.version || option.command == null) {
				out = System.out;
			} else {
				ApplicationProfileManager.load(CMDHOME, settings, option.profile);
				option.command.run(settings, option.arguments.toArray(new String[0]));
			}

		} catch (CmdLineException error) {
			if (!option.help && !option.version) {
				out.println(error.getMessage());
				out.println();
			}

		} catch (IOException error) {
			if (option.profile == null)
				out.printf("Failed to load default profile " +
					   "from %s/default.properties%n", CMDHOME);
			else
				out.println(error.getMessage());
			System.exit(1);
		}

		out.println("Re·Mote Command Line Tools version " + Version.STRING);
		if (option.version)
			System.exit(0);
		out.println();
		out.println("Usage: remote-cli [options] command [arguments]");
		out.println();
		out.println("Options:");
		option.parser.printUsage(out);

		out.printf("%nCommands:%n");
		for (Command cmd : Command.getCommands())
			out.printf(" %-15s %s%n", cmd.getName(), cmd.getDescription());

		System.exit(out == System.err ? 1 : 0);
	}

}
