package remote.apps.cli;

import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.FileOptionHandler;
import org.kohsuke.args4j.spi.StringOptionHandler;

/**
 * Command for running a script from a file.
 */
public class RunScript extends Command implements Command.Authentication {

	@Option(name = "-l", aliases = { "--list" },
		usage = "list available script engines")
	private boolean listEngines;
	@Option(name = "-e", aliases = { "--engine" }, usage = "use script engine",
		handler=StringOptionHandler.class, metaVar="name")
	private String engineName;
	@Argument(index = 0, metaVar = "program", handler = FileOptionHandler.class)
	private File script;
	@Argument(index = 1)
	private List<String> args = new LinkedList<String>();

	/**
	 * Create a run-script command.
	 */
	public RunScript()
	{
		super("run-script", "script", "Run script from file");
	}

	protected void run()
	{
		ScriptEngineManager manager = new ScriptEngineManager();

		if (listEngines || script == null) {
			printf("Supported scripting engines:%n");
			for (ScriptEngineFactory factory : manager.getEngineFactories()) {
				printf("  %s (%s)%n",
				       factory.getEngineName(),
				       factory.getEngineVersion());
				printf("    Language: %s (%s)%n",
				       factory.getLanguageName(),
				       factory.getLanguageVersion());
				printf("    Extensions:");
				for (String extension : factory.getExtensions())
					printf(" %s", extension);
				printf("%n");
			}

			exit();
			return;
		}

		ScriptEngine engine = null;
		if (engineName == null) {
			String[] seqs = script.getName().split("[.]");
			if (seqs.length < 2)
				die("Failed to get script extension from '" + script.getName() + "'");
			String extension = seqs[seqs.length - 1];
			engine = manager.getEngineByExtension(extension);
			if (engine == null)
				die("No scripting engine supports extension '" + extension + "'");
		} else {
			engine = manager.getEngineByName(engineName);
			if (engine == null)
				die("No scripting engine named '" + engineName + "'");
		}

		engine.put("command", this);
		engine.put("session", getSession());
		engine.put("args", args.toArray(new String[0]));

		try {
			engine.eval("importPackage(java.lang);");
			engine.eval("importPackage(java.io);");
			engine.eval("importPackage(Packages.remote.client.control);");
			engine.eval("importPackage(Packages.remote.client.console);");
			engine.eval("importPackage(Packages.remote.client.session);");
			engine.eval(new FileReader(script));
		} catch (Throwable error) {
			die("Script threw an exception", error);
		}
	}

}
