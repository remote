package remote.apps.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Map;
import remote.apps.core.ApplicationProfileManager;
import remote.apps.core.ApplicationAuthenticator;
import remote.client.spi.service.AuthenticationService;

class CommandLineAuthenticator implements ApplicationAuthenticator.Visitor {

	private final BufferedReader stdin;
	private final PrintStream stdout;
	private final Map<String, String> settings;

	protected CommandLineAuthenticator(Map<String, String> settings, InputStream input, PrintStream output)
	{
		super();
		this.settings = settings;
		stdout = output;
		stdin = new BufferedReader(new InputStreamReader(input));
	}

	public String onAuthenticatorVisit(AuthenticationService.Info info)
	{
		String input = ApplicationProfileManager.getAuth(info, settings);
		if (input != null) return input;
		stdout.print(info.getText() + ": ");
		try {
			input = stdin.readLine();
		} catch (IOException ex) {
			return null;
		}
		return input;
	}

	public void onAuthenticatorError(Throwable caught)
	{
		caught.printStackTrace(stdout);
	}

	public void onAuthenticatorComplete()
	{
	}

	public boolean onAuthenticatorResult(boolean result)
	{
		if (!result) stdout.println("Authentication failed!");
		return true;
	}
}
