package remote.apps.cli;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Map;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.Option;
import org.openide.util.Lookup;
import remote.apps.core.ApplicationAuthenticator;
import remote.apps.core.ApplicationSystem;
import remote.client.session.Session;
import remote.client.spi.ServiceEvent;
import remote.client.spi.ServiceEventListener;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceSession;
import remote.client.xrt.ServiceManagerBuilder;
import remote.client.xrt.SessionBuilder;

/**
 * Provides the entry point for executing CLI commands and
 * acts as a base class for all subcommands.
 */
public abstract class Command {

	/* Where to print informational messages to. */
	private static PrintStream out = System.out;

	/*
	 * Command class members.
	 */
	private final String name;
	private final String arguments;
	private final String description;
	private final OptionParser parser = new OptionParser(this);
	private ApplicationSystem system;
	private ServiceManager serviceManager;
	private Session session;
	private volatile boolean running = true;

	/**
	 * Default command line options.
	 */
	@Option(name = "-V", aliases = { "--verbose" }, usage = "be verbose")
	private boolean verbose;
	@Option(name = "-h", aliases = { "--help" }, usage = "print usage help")
	private boolean help;

	/**
	 * Tagging interface that should be implemented by commands,
	 * which need authentication.
	 */
	protected interface Authentication {
	}

	/**
	 * Create a new subcommand.
	 * @param name of the subcommand.
	 * @param description of the subcommand.
	 */
	protected Command(String name, String description)
	{
		this(name, "", description);
	}

	/**
	 * Create a new subcommand. 
	 * @param name of the subcommand.
	 * @param arguments of the subcommand.
	 * @param description of the subcommand.
	 */
	protected Command(String name, String arguments, String description)
	{
		this.name = name;
		this.arguments = arguments;
		this.description = description;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	/**
	 * Get all known commands.
	 * @return collection of all known commands.
	 */
	public static Collection<? extends Command> getCommands()
	{
		return Lookup.getDefault().lookupAll(Command.class);
	}

	/**
	 * Method for executing the actual command. If the command implements
	 * the {@link Authentication} interface, run will be called when
	 * authentication has been successfully completed.
	 */
	protected abstract void run();

	void run(Map<String, String> settings, String[] args)
	{
		try {
			parser.parseArgument(args);
		} catch (CmdLineException exception) {
			if (!help)
				error(exception.getMessage());
		}

		if (help)
			usage();

		system = new ApplicationSystem(out);

		ServiceManagerBuilder builder = ServiceManagerBuilder.create(system);
		if (verbose)
			builder.listener(system.newLogger());

		if (this instanceof ApplicationAuthenticator.Visitor)
			builder.listener(new AuthEventListener((ApplicationAuthenticator.Visitor) this));
		if (this instanceof Command.Authentication)
			builder.listener(new AuthEventListener(settings));

		builder.providers(system.getServiceProviders());
		builder.settings(settings);
		serviceManager = builder.build();

		/* The AuthEventListener hook added above will call run(). */
		if (!(this instanceof Command.Authentication))
			run();

		while (running)
			try {
				if (system.hasTasks())
					system.waitTasks(30000);
				else
					Thread.sleep(1000);
			} catch (InterruptedException ex) {
				continue;
			}
		serviceManager.shutdown();
		System.exit(0);
	}

	protected static void printf(String format, Object... args)
	{
		out.printf(format, args);
	}

	private void usage()
	{
		parser.setUsageWidth(80);
		printf("Usage: remote-cli %s [options] %s%n%n", name, arguments);
		printf("%s%n%nOptions:%n", getDescription());
		parser.printUsage(out);
		System.exit(out == System.err ? 1 : 0);
	}

	/**
	 * Error out with message and the command usage. This should be used
	 * if the wrong arguments are passed.
	 * @param msg to print to stderr.
	 */
	protected void error(String msg)
	{
		out = System.err;
		printf("%s%n%n", msg);
		usage();
	}

	/**
	 * Exit the program with message.
	 * @param msg to print to stderr.
	 */
	public void die(String msg)
	{
		System.err.println(msg);
		System.exit(1);
	}

	/**
	 * Exit the program with message.
	 * @param msg to print to stderr.
	 * @param caught error that is causing	 the program to exit.
	 */
	protected void die(String msg, Throwable caught)
	{
		System.err.println(msg + ": " + caught.getMessage());
		caught.printStackTrace(System.err);
		System.exit(1);
	}

	/**
	 * Get the service manager for this command.
	 * @return the service manager.
	 */
	public ServiceManager getServiceManager()
	{
		return serviceManager;
	}

	/**
	 * Get the session object.
	 * @return the session.
	 */
	public Session getSession()
	{
		return session;
	}

	/**
	 * Shutdown services and exit the command.
	 */
	public void exit()
	{
		running = false;
	}

	private class AuthEventListener implements ServiceEventListener {

		private final ApplicationAuthenticator auth;

		AuthEventListener(ApplicationAuthenticator.Visitor visitor)
		{
			this.auth = ApplicationAuthenticator.create(visitor);
		}

		AuthEventListener(Map<String, String> settings)
		{
			this(new CommandLineAuthenticator(settings, System.in, out) {

				@Override
				public boolean onAuthenticatorResult(boolean result)
				{
					result = super.onAuthenticatorResult(result);
					if (result)
						run();
					return result;
				}

			});
		}

		public <T extends ServiceEvent> void onEvent(T serviceEvent)
		{
			if (!(serviceEvent instanceof ServiceSession.Event))
				return;

			ServiceSession.Event event = (ServiceSession.Event) serviceEvent;

			if (event instanceof ServiceSession.Event.Init) {
				auth.authenticate(serviceManager, event.getSession());
			} else if (event instanceof ServiceSession.Event.Auth) {
				session = SessionBuilder.create(system).
						session(event.getSession()).
						manager(serviceManager).build();
			}
		}

	}

}
