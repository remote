package remote.apps.cli;

import remote.apps.core.ApplicationAuthenticator;
import remote.client.spi.service.AuthenticationService;

/**
 * A command for listing authentication information.
 */
public class LsAuth extends Command implements ApplicationAuthenticator.Visitor {

	private boolean showHeader = true;

	/**
	 * Create a command for listing authentication information.
	 */
	public LsAuth()
	{
		super("ls-auth", "List authentication info for a server");
	}

	protected void run()
	{
	}

	private void showAuth(String label, String type, String help)
	{
		printf("%-20s %-20s %-20s%n", label, help, type);
	}

	public String onAuthenticatorVisit(AuthenticationService.Info info)
	{
		if (showHeader) {
			showHeader = false;
			showAuth("Name", "Type", "Help");
			showAuth("----", "----", "----");
		}
		showAuth(info.getText(), info.getType(), info.getHelp());
		return null;
	}

	public void onAuthenticatorError(Throwable caught)
	{
		die("Authentication error", caught);
	}

	public void onAuthenticatorComplete()
	{
		exit();
	}

	public boolean onAuthenticatorResult(boolean result)
	{
		return false;
	}

}
