/*
 * Script that mimics the behavior of the mote-exec subcommand.
 */

function readFile(path)
{
	var reader = new BufferedReader(new FileReader(path))
	var content = ''

	while (true) {
		var line = reader.readLine()
		if (line)
			content += line + "\n";
		else
			return content;
	}
}

function usage(msg)
{
	command.die(msg + "\n\nUsage: mote-exec.js mote-id program")
}

if (args[0] == undefined)
	usage("Mote ID required as the first argument")
if (args[1] == undefined)
	usage("Mote program request as the second argument")

var moteListener = new MoteListener() {

	onControl: function(mote)
	{
		println("Programming mote")
		mote.program(readFile(args[1]))
	},

	onProgram: function(mote, result)
	{
		println("Programming result is " + result)
		mote.start();
	},

	onStart: function(mote, result)
	{
		println("Start result is " + result)
	},

	onLost: function(mote)
	{
		command.die("Mote was lost")
	}

}

var moteConsoleListener = new MoteConsoleListener() {

	onRead: function(mote, data)
	{
		System.out.write(data, 0, data.length)
		System.out.flush()
	},

	onWrite: function(mote) {}

}

var moteManagerListener = new MoteManagerListener() {

	onUpdate: function(moteManager, newMotes, delMotes, changedMotes)
	{
		var motes = moteManager.motes;

		while (motes.hasNext()) {
			var mote = motes.next()
			if (mote.id == args[0]) {
				mote.addEventListener(moteListener)
				mote.console.addEventListener(moteConsoleListener)
				mote.control();
				return
			}
		}

		command.die("No mote matching ID '" + args[0] + "'")
	}

}

var sessionListener = new SessionListener() {

	onError: function(session, error)
	{
		command.die("Error occured: " + error.message)
	}

}

session.addEventListener(sessionListener)
session.moteManager.addEventListener(moteManagerListener)

if (!session.moteManager.update()) {
	println("Failed to update MoteManger")
	command.exit()
}
