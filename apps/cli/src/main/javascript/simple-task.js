/*
 * Simple script that uses the service manager to execute a task.
 */

print("Starting the script\n");

var service = command.getServiceManager();
var task = new java.lang.Runnable() {

	run: function() {
		print("I'm in ur task\n");
		java.lang.Thread.sleep(2);
		print("Exiting ur command\n");
		command.exit();
     	}

}

var job = service.addTask(task);
