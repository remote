/*
 * Script that mimics the behavior of the ls-motes subcommand.
 */

function foreach(iterator, apply)
{
	while (iterator.hasNext())
		apply(iterator.next());
}

var hostNum = 1;
var moteNum = 1;
var hostMap = {};

function printHost(host)
{
	hostMap[host] = hostNum++;
	println("Host " + hostMap[host]);
	println("  IP:    " + host.ip);
	println("  DNS:   " + host.dns);

	var motes = 0;
	foreach(host.motes, function() { motes++; });

	println("  Motes: " + motes);
}

function printMote(mote)
{
	println("Mote " + moteNum++);
	println("  ID:    " + mote.id);
	println("  Host:  " + hostMap[mote.host]);
}

session.moteManager.addEventListener(new MoteManagerListener() {

	onUpdate: function(moteManager, newMotes, delMotes, changedMotes)
	{
		foreach(moteManager.hosts, printHost);
		foreach(moteManager.motes, printMote);
		command.exit();
	}

});

if (!session.moteManager.update()) {
	println("Failed to update MoteManger");
	command.exit();
}
