package remote.apps.core;

import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceSession;
import remote.client.spi.service.AuthenticationService;

/**
 * Manager for authenticating a session.
 */
public class ApplicationAuthenticator implements AuthenticationService.AuthenticationCallback
{

	private final Visitor visitor;

	protected ApplicationAuthenticator(Visitor visitor)
	{
		this.visitor = visitor;
	}

	/**
	 * Create a new authenticator.
	 * @param visitor that will provide authentication credential values.
	 * @return the new authenticator.
	 */
	public static ApplicationAuthenticator create(Visitor visitor)
	{
		return new ApplicationAuthenticator(visitor);
	}

	/**
	 * Cause the authenticator to authenticate the given session.
	 *
	 * @param manager
	 * @param session
	 */
	public void authenticate(ServiceManager manager, ServiceSession session)
	{
		manager.get(AuthenticationService.class).authenticate(session, this);
	}

	public interface Visitor {

		String onAuthenticatorVisit(AuthenticationService.Info info);

		void onAuthenticatorError(Throwable caught);

		void onAuthenticatorComplete();

		boolean onAuthenticatorResult(boolean result);

	}

	public void onAuthenticate(AuthenticationService.Info[] infos)
		throws AuthenticationService.UnsupportedException
	{
		for (AuthenticationService.Info info : infos) {
			String input = visitor.onAuthenticatorVisit(info);
			if (input == null)
				continue;
			if (info instanceof AuthenticationService.Name)
				((AuthenticationService.Name) info).setName(input);
			else if (info instanceof AuthenticationService.Password)
				((AuthenticationService.Password) info).setPassword(input);
			else
				info.unsupported();
		}
		visitor.onAuthenticatorComplete();
	}

	public void onSuccess(Boolean result)
	{
		visitor.onAuthenticatorResult(result.booleanValue());
	}

	public void onError(Throwable caught)
	{
		visitor.onAuthenticatorError(caught);
	}

}
