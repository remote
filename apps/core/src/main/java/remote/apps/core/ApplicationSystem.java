package remote.apps.core;

import java.io.PrintStream;
import java.util.Collection;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import remote.client.spi.ServiceEvent;
import remote.client.spi.ServiceEventListener;
import remote.client.spi.ServiceProvider;
import remote.client.xrt.RuntimeSystem;
import remote.util.Cancellable;

/**
 * Basic implementation of the runtime system using the netbeans lookup and
 * request processor.
 */
public class ApplicationSystem implements RuntimeSystem
{

	private final PrintStream out;
	private final RequestProcessor scheduler = new RequestProcessor("cli");
	private RequestProcessor.Task lastTask;

	/**
	 * Create a new application runtime system.
	 * @param out stream where messages should be logged.
	 */
	public ApplicationSystem(PrintStream out)
	{
		this.out = out;
	}

	public void handleError(Throwable error)
	{
		out.printf("Error occurred:%n");
		error.printStackTrace(out);
		System.exit(-1);
	}

	public Cancellable postTask(Runnable runnable)
	{
		final RequestProcessor.Task task = scheduler.post(runnable);
		lastTask = task;
		task.addTaskListener(new TaskListener() {

			public void taskFinished(Task task)
			{
				if (task == lastTask)
					lastTask = null;
			}

		});
		return new Cancellable() {

			public boolean isDone()
			{
				return task.isFinished();
			}

			public void cancel()
			{
				task.cancel();
			}

		};
	}

	public void stopTasks()
	{
		scheduler.stop();
	}

	/**
	 * Check if there are tasks executing.
	 * 
	 * @return check if there are tasks in the runqueue.
	 */
	public boolean hasTasks()
	{
		return lastTask != null;
	}

	/**
	 * Wait until all posted tasks have completed.
	 * 
	 * @param timeout in milliseconds to wait before returning.
	 * @return true if all tasks really finished, false if timout exceeded.
	 * @throws InterruptedException if system was interrupted while waiting.
	 */
	public boolean waitTasks(long timeout) throws InterruptedException
	{
		return hasTasks() ? lastTask.waitFinished(timeout) : true;
	}

	/**
	 * Lookup all service providers.
	 * @return a collection of all service providers.
	 */
	public Collection<? extends ServiceProvider> getServiceProviders()
	{
		return Lookup.getDefault().lookupAll(remote.client.spi.ServiceProvider.class);
	}

	/**
	 * Create a new service event logger.
	 * @return the new service event logger.
	 */
	public ServiceEventListener newLogger()
	{
		return new ServiceEventListener() {

			public <T extends ServiceEvent> void onEvent(T event)
			{
				out.printf("[%-30s] %s%n",
					   event.getSource().getClass().getSimpleName(),
					   event.getMessage());
			}

		};
	}

}
