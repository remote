package remote.apps.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;
import remote.client.spi.service.AuthenticationService;

/**
 * Manager for reading and using testbed profiles.
 */
public class ApplicationProfileManager
{

	private static void loadProperties(HashMap<String, String> map, InputStream input)
		throws IOException
	{
		Properties properties = new Properties();

		try {
			properties.loadFromXML(input);
		} catch (InvalidPropertiesFormatException ipfe) {
			properties.load(input);
		}

		Enumeration<?> enumeration = properties.propertyNames();
		while (enumeration.hasMoreElements()) {
			String prop = enumeration.nextElement().toString();
			map.put(prop, properties.getProperty(prop));
		}
	}

	/**
	 * Load a profile from the home directory or the network.
	 *
	 * @param home directory in which to search for the profile.
	 * @param map to where the profile should be saved.
	 * @param profile name to use.
	 * @throws java.io.IOException if the profile could not be found or an
	 *	error occurs while reading.
	 */
	public static void load(String home, HashMap<String, String> map, String profile)
		throws IOException
	{
		String profileName = profile == null ? "default" : profile;
		String[] localConfigs = {
			profileName, home + profileName + ".properties"
		};

		for (String config : localConfigs) {
			File file = new File(config);
			if (!file.exists())
				continue;
			try {
				loadProperties(map, new FileInputStream(file));
				return;
			} catch (FileNotFoundException ex) {
				continue;
			}
		}

		if (profileName.contains("://"))
			loadProperties(map, new URL(profileName).openStream());
		throw new IOException("Failed to load profile '" + profileName + "'");
	}

	/**
	 * Get an saved authentication value from a profile map.
	 *
	 * @param info about the authentication credential.
	 * @param map containing the profile.
	 * @return the authentication value or null if no value exists.
	 */
	public static String getAuth(AuthenticationService.Info info,
				     Map<String, String> map)
	{
		if (map == null)
			return null;
		for (int index = 0; true; index++) {
			String type = map.get("authentication." + index + ".type");
			String name = map.get("authentication." + index + ".name");

			if (type == null || name == null)
				return null;
			if (!type.equalsIgnoreCase(info.getType()))
				continue;
			if (!name.equalsIgnoreCase(info.getText()))
				continue;
			return map.get("authentication." + index + ".value");
		}
	}


}
