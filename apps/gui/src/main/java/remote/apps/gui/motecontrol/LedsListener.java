package remote.apps.gui.motecontrol;

public interface LedsListener {
	public void ledsChanged(LedsInputStream leds);
}
