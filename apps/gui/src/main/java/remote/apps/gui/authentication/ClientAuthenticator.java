package remote.apps.gui.authentication;

import java.rmi.RemoteException;

import remote.axis.bindings.authentication.*;

public class ClientAuthenticator implements Authenticator_PortType {

	AuthenticatorServiceLocator authLocator;

	public ClientAuthenticator(String server)
	{
		authLocator = new AuthenticatorServiceLocator();
		String url ="http://"+server+":8080/remote-webapp/services/authentication";
		authLocator.setAuthenticatorEndpointAddress(url);
	}

	public Credential[] getEmptyCredentials() throws RemoteException {
		Credential[] creds = null;
		try
		{
			creds = authLocator.getAuthenticator().getEmptyCredentials();
		} catch (javax.xml.rpc.ServiceException e)
		{
			e.printStackTrace();
		}
		return creds;
	}

	public boolean authenticate(String session_id, Credential[] credentials) throws RemoteException {
		boolean result = false;
		try
		{
			result = authLocator.getAuthenticator().authenticate(session_id,credentials);
		} catch (javax.xml.rpc.ServiceException e)
		{
			e.printStackTrace();
		}
		return result;
	}

}
