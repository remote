package remote.apps.gui.moteaccess;

import java.rmi.RemoteException;
import remote.axis.bindings.moteaccess.MoteAccessServiceLocator;
import remote.axis.bindings.moteaccess.MoteAccess_PortType;

public class ClientMoteAccess implements MoteAccess_PortType {

	MoteAccessServiceLocator masLocator;

	public ClientMoteAccess(String server)
	{
		masLocator = new MoteAccessServiceLocator();
		String url = "http://" + server + ":8080/remote-webapp/services/moteaccess";
		masLocator.setMoteAccessEndpointAddress(url);

	}

	public boolean[] getPrivileges(long[] mote_ids, String session_id) throws RemoteException
	{
		boolean[] results = null;
		try {
			results = masLocator.getMoteAccess().getPrivileges(mote_ids, session_id);
		} catch (javax.xml.rpc.ServiceException e) {
			e.printStackTrace();
		}
		return results;
	}

	public void dropPrivileges(long[] mote_ids, String session_id) throws RemoteException
	{
		try {
			masLocator.getMoteAccess().dropPrivileges(mote_ids, session_id);
		} catch (javax.xml.rpc.ServiceException e) {
			e.printStackTrace();
		}
	}

}
