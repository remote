package remote.apps.gui.motedata;

import java.util.Hashtable;
import remote.axis.bindings.motedata.MoteDataServiceLocator;
import remote.axis.bindings.motedata.MoteDataTable;
import remote.apps.gui.motecontrol.Session;

public class MoteDataWrapper {
	private static MoteDataServiceLocator motedataLocator = new MoteDataServiceLocator();

	private static MoteDataTable moteList = null;
	private static SimpleTableWrapper moteTable = null;
	private static Hashtable moteIndex = null;
	private static Session session = null;

	public static Table getMoteTable() {
		if (moteTable == null)
		{
			moteTable = new SimpleTableWrapper(getMoteList());
		}
		return moteTable;
	}

	public static Hashtable getMoteIndex() {
		return moteIndex;
	}

	public static void updateMoteList() {
		try
		{
			moteList = motedataLocator.getMoteData().getMoteData(session.getSessionId());
			((SimpleTableWrapper)getMoteTable()).updateTable(moteList);
			moteIndex = createIndex(moteList,0);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static MoteDataTable getMoteList()
	{
		if (moteList == null)
		{
			try
			{
				moteList = motedataLocator.getMoteData().getMoteData(session.getSessionId());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return moteList;
	}

	static Hashtable createIndex(MoteDataTable table, int col)
	{
		Hashtable index = new Hashtable();
		for (int i = 0 ; i < table.getData().length ; i ++ )
		{
			index.put(table.getData()[i][col], table.getData()[i]);
		}
		return index;
	}

	public static void setSession(Session session) {
		MoteDataWrapper.session = session;
		String url ="http://"+session.getServerConnection().getConnectionInfo().getServerName()+":8080/remote-webapp/services/motedata";
		motedataLocator.setMoteDataEndpointAddress(url);
	}
}
