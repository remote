package remote.apps.gui.motecontrol;

public interface SessionEventListener {

	public void eventFired(Session client,SessionEvent event);
}
