package remote.apps.gui.motecontrol;

public interface MoteControlStatusListener {

	public void moteControlStatusChange(Mote mote);
}
