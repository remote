package remote.apps.gui.motecontrol;

public interface SimpleMoteStatusListener {

	public void simpleMoteStatusChange(SimpleMote mote);
}
