package remote.apps.gui.util;
import remote.apps.gui.motedata.TableRow;

public interface TableRowOrdering {

	public abstract Comparable getKey(TableRow row);
	public abstract boolean isTotal();
	public abstract String toString();
}
