package remote.util;

/** Version utility. */
public final class Version implements Comparable<Version> {

	/** Current library major version. */
	public static final int MAJOR = 2;

	/** Current library minor version. */
	public static final int MINOR = 0;

	/** Current library qualifier. */
	public static final String QUALIFIER = "-SNAPSHOT";

	/** Current library version string. */
	public static final String STRING = MAJOR + "." + MINOR + QUALIFIER;

	/** Current library version. */
	public static final Version CURRENT = new Version(MAJOR, MINOR, QUALIFIER);

	private final int major;
	private final int minor;
	private final String qualifier;

	/** Formatted version string.
	 *
	 * This is a combination of major and minor version separated
	 * by a dot.
	 */
	private final String string;

	/** Create a version.
	 *
	 * @param major	The major version.
	 * @param minor	The minor version.
	 * @param qualifier Version qualifier.
	 */
	public Version(int major, int minor, String qualifier)
	{
		this.major = major;
		this.minor = minor;
		this.qualifier = qualifier;
		this.string = major + "." + minor + qualifier;
	}

	/** Get major version.
	 *
	 * @return	The major version.
	 */
	public int getMajor()
	{
		return major;
	}

	/** Get minor version.
	 *
	 * @return	The minor version.
	 */
	public int getMinor()
	{
		return minor;
	}

	/** Get version qualifier.
	 *
	 * @return	The version qualifier.
	 */
	public String getQualifier()
	{
		return qualifier;
	}

	/** Get version string.
	 *
	 * @return	The version string.
	 */
	public String getString()
	{
		return string;
	}

	/** Check if the library supports a specific version.
	 *
	 * @param other The version with which to compare.
	 * @return	Negative number, zero, or positive number indicating
	 *		whether the specified object has a lower, equal, or
	 *		higher version.
	 */
	public int compareTo(Version other)
	{
		if (major != other.major)
			return other.major - major;
		if (minor != other.minor)
			return other.minor - minor;
		return qualifier.compareTo(other.qualifier);
	}

}
