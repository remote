package remote.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Adapter for providing an iterator for a collection of a type extending the
 * desired iterable type, e.g. exposing an internal MoteModel collection as a
 * Mote interface iterator. Null values are not supported.
 *
 * @param <T> The type that will be exposed.
 * @param <R> The internal extending type that is iterable.
 */
public class IteratorAdapter<T, R extends T> implements Iterator<T> {

	private final Iterator<R> iterator;
	private R next = null;

	public IteratorAdapter(Iterable<R> iterable)
	{
		this.iterator = iterable.iterator();
	}

	public boolean acceptNext(R next)
	{
		return true;
	}

	private R findNext()
	{
		while (iterator.hasNext()) {
			R item = iterator.next();
			if (acceptNext(item))
				return item;
		}

		return null;
	}

	public boolean hasNext()
	{
		if (next == null)
			next = findNext();
		return next != null;
	}

	public T next()
	{
		R result = next == null ? findNext() : next;

		next = null;
		if (result == null)
			throw new NoSuchElementException();
		return result;
	}

	public void remove()
	{
		throw new UnsupportedOperationException();
	}

}
