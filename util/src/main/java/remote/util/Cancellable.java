package remote.util;

/**
 * End execution of a job or request.
 */
public interface Cancellable {

	/**
	 * Check whether a request is pending or done.
	 *
	 * @return True if the request is done executing.
	 */
	boolean isDone();

	/**
	 * Cancel a pending request.
	 */
	void cancel();

}
