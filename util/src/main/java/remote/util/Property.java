package remote.util;

/**
 * Configuration property.
 * 
 * @param <T> Type of the property value.
 */
public interface Property<T> {

	/**
	 * Get the property name.
	 * @return the property name.
	 */
	public String getName();

	/**
	 * Get the property label.
	 * @return the property label.
	 */
	public String getLabel();

	/**
	 * Get the property value.
	 * @return the property value.
	 */
	public T getValue();

	/**
	 * Utility for converting configuration opaque values to a desired type.
	 */
	public static abstract class Converter {

		protected abstract <T> T getValue(Object value, T defaultValue);

		private static final Integer valueOfInteger(Object value)
		{
			if (value instanceof Integer)
				return (Integer) value;
			return Integer.valueOf(value.toString());
		}

		private static final Long valueOfLong(Object value)
		{
			if (value instanceof Long)
				return (Long) value;
			return Long.valueOf(value.toString());
		}

		private static final String valueOfString(Object value)
		{
			if (value instanceof String)
				return (String) value;
			return String.valueOf(value);
		}

		/**
		 * Convert property to specific type or return a default value.
		 * @param <T> the desired mote property type.
		 * @param value the transparent value.
		 * @param defaultValue to return if conversion fails
		 * @return converted value or the default value.
		 */
		@SuppressWarnings("unchecked")
		public <T> T valueOf(Object value, T defaultValue)
		{
			try {
				if (value == null)
					return defaultValue;
				if (value.getClass() == defaultValue.getClass())
					return (T) value;
				if (defaultValue instanceof java.lang.Integer)
					return (T) valueOfInteger(value);
				if (defaultValue instanceof java.lang.Long)
					return (T) valueOfLong(value);
				if (defaultValue instanceof java.lang.String)
					return (T) valueOfString(value);
				return getValue(value, defaultValue);
			} catch (Throwable error) {
				return defaultValue;
			}
		}

	}

}
