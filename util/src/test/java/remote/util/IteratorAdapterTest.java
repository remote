package remote.util;

import java.util.ArrayList;
import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import remote.util.IteratorAdapter;
import static org.junit.Assert.*;

public class IteratorAdapterTest {

	interface AInterface {

		String getB();

		int getC();

	}

	class AClass implements AInterface {

		protected String B;
		protected int C;

		AClass(String B, int C)
		{
			this.B = B;
			this.C = C;
		}

		public String getB()
		{
			return B;
		}

		public int getC()
		{
			return C;
		}

	}

	class AClassExt extends AClass {

		AClassExt(String B)
		{
			super(B, B.length());
		}

		@Override
		public String getB()
		{
			return B + "+";
		}

	}

	@Test
	public void interfaceIteratorAdapter()
	{
		ArrayList<AClass> list = new ArrayList<AClass>();
		list.add(new AClass("B", 0xC));
		list.add(new AClass("B", 0xC));
		list.add(new AClass("B", 0xC));
		Iterator<AInterface> it = new IteratorAdapter<AInterface, AClass>(list);
		int size = 0;

		while (it.hasNext()) {
			AInterface next = it.next();
			assertEquals(next.getB(), "B");
			assertEquals(next.getC(), 0xC);
			size++;
		}

		assertEquals(3, size);
	}

	@Test
	public void classIteratorAdapter()
	{
		ArrayList<AClassExt> list = new ArrayList<AClassExt>();
		list.add(new AClassExt("B"));
		list.add(new AClassExt("B"));
		list.add(new AClassExt("B"));
		Iterator<AClass> it = new IteratorAdapter<AClass, AClassExt>(list);
		int size = 0;

		while (it.hasNext()) {
			AClass next = it.next();
			assertEquals(next.getB(), "B+");
			assertEquals(next.getC(), 1);
			size++;
		}

		assertEquals(3, size);
	}

	@Test
	public void interfaceIteratorAdapterWithAccept()
	{
		ArrayList<AClass> list = new ArrayList<AClass>();
		list.add(new AClass("A", 0xA));
		list.add(new AClass("B", 0xB));
		list.add(new AClass("C", 0xC));
		Iterator<AInterface> it = new IteratorAdapter<AInterface, AClass>(list) {

			@Override
			public boolean acceptNext(AClass item)
			{
				return item.getB().equals("B");
			}

		};
		int size = 0;

		while (it.hasNext()) {
			AInterface next = it.next();
			assertEquals(next.getB(), "B");
			assertEquals(next.getC(), 0xB);
			size++;
		}

		assertEquals(1, size);
	}

	@Test
	public void classIteratorAdapterWithAccept()
	{
		ArrayList<AClassExt> list = new ArrayList<AClassExt>();
		list.add(new AClassExt("A"));
		list.add(new AClassExt("B"));
		list.add(new AClassExt("C"));
		list.add(new AClassExt("B"));
		list.add(new AClassExt("D"));
		Iterator<AClass> it = new IteratorAdapter<AClass, AClassExt>(list) {

			@Override
			public boolean acceptNext(AClassExt item)
			{
				return item.getB().equals("B+");
			}

		};
		int size = 0;

		while (it.hasNext()) {
			AClass next = it.next();
			assertEquals(next.getB(), "B+");
			assertEquals(next.getC(), 1);
			size++;
		}

		assertEquals(2, size);
	}

}
