package remote.util;

import org.junit.*;
import static org.junit.Assert.*;

public class VersionTest {

	@Test
	public void string()
	{
		String version = String.format("%s.%s%s", Version.MAJOR, Version.MINOR, Version.QUALIFIER);

		assertEquals(version, Version.STRING);
	}

	@Test
	public void compareTo()
	{
		Version library = new Version(Version.MAJOR, Version.MINOR, Version.QUALIFIER);
		Version initial = new Version(0, 0, "");
		Version nextMinor = new Version(Version.MAJOR, Version.MINOR + 1, "");
		Version nextMajor = new Version(Version.MAJOR + 1, Version.MINOR, "");

		assertEquals(Version.CURRENT.compareTo(Version.CURRENT), 0);
		assertEquals(Version.CURRENT.compareTo(library), 0);
		assertTrue(Version.CURRENT.compareTo(initial) < 0);
		assertTrue(Version.CURRENT.compareTo(nextMinor) > 0);
		assertTrue(Version.CURRENT.compareTo(nextMajor) > 0);
	}

}
