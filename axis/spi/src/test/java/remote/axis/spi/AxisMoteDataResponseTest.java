package remote.axis.spi;

import java.util.EnumSet;
import org.junit.Test;
import remote.axis.bindings.motedata.MoteDataHeader;
import remote.axis.bindings.motedata.MoteDataTable;
import remote.client.spi.service.MoteDataService.Attribute;
import remote.client.spi.service.MoteDataService.Visitor;
import remote.client.spi.service.MoteDataService.Response;
import static org.junit.Assert.*;

public class AxisMoteDataResponseTest {

	abstract class Builder {

		MoteDataHeader[] headers;
		Object[][] data;

		Builder(int headersSize, int dataSize)
		{
			headers = new MoteDataHeader[headersSize];
			for (int i = 0; i < headersSize; i++)
				headers[i] = new MoteDataHeader();

			data = new Object[dataSize][headersSize];
			for (int k = 0; k < dataSize; k++)
				for (int j = 0; j < headersSize; j++)
					data[k][j] = new Object();
			fill();
		}

		abstract void fill();

		Response build()
		{
			MoteDataTable table = new MoteDataTable();
			table.setHeaders(headers);
			table.setData(data);
			return AxisMoteDataResponse.from(table);
		}

	}

	class DummyBuilder extends Builder {

		DummyBuilder(int a, int b)
		{
			super(a, b);
		}

		void fill()
		{
		}

	}

	@Test
	public void visitPropertyInfo()
	{
		Response response = new Builder(Attribute.values().length, 0) {

			void fill()
			{
				for (Attribute attr : Attribute.values()) {
					MoteDataHeader header = headers[attr.ordinal()];
					String name = attr == Attribute.unknown ? "asdf" : attr.name();

					header.setName(name);
					header.setTitle(name.toUpperCase());
					header.setValueclass(String.class.getName());
					header.setVisible(true);
				}
			}

		}.build();
		final EnumSet<Attribute> attrSet = EnumSet.noneOf(Attribute.class);

		Visitor visitAll = new Visitor() {

			public boolean visit(String name, String title, String klass, boolean visible)
			{
				attrSet.add(Attribute.match(name));
				assertNotSame(name, title);
				assertEquals(name, title.toLowerCase());
				assertEquals(klass, "java.lang.String");
				assertTrue(visible);
				return true;
			}

		};

		response.visitDescriptors(visitAll);

		for (Attribute attr : Attribute.values())
			assertTrue(attrSet.contains(attr));
	}

	@Test
	public void visitPropertyInfoStop()
	{
		Response response = new Builder(Attribute.values().length, 0) {

			void fill()
			{
				for (Attribute attr : Attribute.values()) {
					MoteDataHeader header = headers[attr.ordinal()];
					String name = attr == Attribute.unknown ? "asdf" : attr.name();

					header.setName(name);
					header.setTitle(name.toUpperCase());
					header.setValueclass(String.class.getName());
					header.setVisible(true);
				}
			}

		}.build();
		final EnumSet<Attribute> attrSet = EnumSet.noneOf(Attribute.class);

		Visitor visitAll = new Visitor() {

			public boolean visit(String name, String title, String klass, boolean visible)
			{
				attrSet.add(Attribute.match(name));
				return attrSet.size() < 3;
			}

		};

		response.visitDescriptors(visitAll);

		assertEquals(3, attrSet.size());
	}

	@Test
	public void hasProperty()
	{
		final EnumSet<Attribute> attrSet = EnumSet.allOf(Attribute.class);
		attrSet.remove(Attribute.unknown);
		attrSet.remove(Attribute.platform);
		attrSet.remove(Attribute.macaddress);

		Response response = new Builder(Attribute.values().length, 0) {

			void fill()
			{
				for (Attribute attr : Attribute.values()) {
					MoteDataHeader header = headers[attr.ordinal()];
					String name = attr.name();

					if (!attrSet.contains(attr))
						name = attr.name() + attr.ordinal();

					header.setName(name);
					header.setTitle(name.toUpperCase());
					header.setValueclass(String.class.getName());
					header.setVisible(true);
				}
			}

		}.build();

		for (Attribute attr : Attribute.values())
			if (attrSet.contains(attr))
				assertTrue(response.hasAttribute(attr));
	}

	@Test
	public void getMotesSize()
	{
		assertEquals(0, new DummyBuilder(1, 0).build().getMoteSize());
		assertEquals(6, new DummyBuilder(1, 6).build().getMoteSize());
	}

	@Test
	public void getProperty()
	{
		final EnumSet<Attribute> attrSet = EnumSet.allOf(Attribute.class);
		attrSet.remove(Attribute.unknown);
		Response response = new Builder(attrSet.size(), 1) {

			void fill()
			{
				for (Attribute attr : Attribute.values()) {
					if (!attrSet.contains(attr))
						continue;

					MoteDataHeader header = headers[attr.ordinal()];
					String name = attr.name();

					header.setName(name);
					header.setTitle(name.toUpperCase());
					header.setValueclass(String.class.getName());
					header.setVisible(true);

					if (attr.ordinal() > 3)
						data[0][attr.ordinal()] = attr.name();
					else
						data[0][attr.ordinal()] = Integer.valueOf(attr.ordinal());
				}
			}

		}.build();

		for (Attribute attr : Attribute.values())
			if (!attrSet.contains(attr))
				continue;
			else if (attr.ordinal() > 3)
				assertEquals(attr.name(), response.getAttribute(0, attr, "asdf"));
			else
				assertEquals(attr.ordinal(), response.getAttribute(0, attr, -1));
	}

}
