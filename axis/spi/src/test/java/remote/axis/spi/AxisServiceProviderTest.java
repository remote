package remote.axis.spi;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import remote.client.spi.Service;
import remote.client.spi.ServiceContext;
import remote.client.spi.ServiceEvent;
import remote.client.spi.ServiceEventListener;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceProperty;
import remote.client.spi.ServiceProvider;
import remote.client.spi.service.AuthenticationService;
import remote.client.spi.service.MoteAccessService;
import remote.client.spi.service.MoteControlService;
import remote.client.spi.service.MoteDataService;
import remote.util.Cancellable;
import static org.junit.Assert.*;

public class AxisServiceProviderTest {

	@SuppressWarnings("unchecked")
	static final List<Class<? extends Service>> supported = Arrays.asList(
		AuthenticationService.class,
		MoteDataService.class,
		MoteAccessService.class);
	@SuppressWarnings("unchecked")
	static final List<Class<? extends Service>> unsupported = Arrays.asList(
		Service.class,
		MoteControlService.class);

	@Test
	public void hasService()
	{
		ServiceProvider provider = new AxisServiceProvider();

		assertTrue(provider.hasService(AuthenticationService.class));
		assertTrue(provider.hasService(MoteDataService.class));
		assertTrue(provider.hasService(MoteAccessService.class));
		assertFalse(provider.hasService(MoteControlService.class));
		assertFalse(provider.hasService(Service.class));
	}

	class TestServiceContextManager implements ServiceManager {

		protected int gotProperty;
		protected int shutdown;
		protected int gotContext;
		protected int firedEvent;
		protected int tasks;

		public <T> ServiceProperty<T> addProperty(final String name, final T defaultValue, final String label)
		{
			if (name.equals("axis.location")) {
				gotProperty++;
				return new ServiceProperty<T>() {

					public String getName()
					{
						return name;
					}

					public String getLabel()
					{
						return label;
					}

					public T getValue()
					{
						return defaultValue;
					}
					
				};
			}
			return null;
		}

		public void addEventListener(ServiceEventListener listener)
		{
			throw new RuntimeException("addEventListener called");
		}

		public void removeEventListener(ServiceEventListener listener)
		{
			throw new RuntimeException("removeEventListener called");
		}

		public <T extends ServiceEvent> void fireEvent(T event)
		{
			firedEvent++;
		}

		public <T> Service.Task addRequest(Service.Callback<T> callback, Request<T> task)
		{
			tasks++;
			return null;
		}

		public boolean check(int i)
		{
			return false;
		}

		public <T extends Service> T get(Class<T> service)
		{
			throw new UnsupportedOperationException("Not supported yet.");
		}

		public void shutdown()
		{
			throw new UnsupportedOperationException("Not supported yet.");
		}

		public Cancellable addTask(Runnable task)
		{
			tasks++;
			return null;
		}

		public void error(ServiceContext context, Throwable caught)
		{
			throw new UnsupportedOperationException("Not supported yet.");
		}

	}

	@Test
	public void getContext()
	{
		TestServiceContextManager manager = new TestServiceContextManager() {

			@Override
			public boolean check(int i)
			{
				switch (i) {
				case 0:
					return gotProperty > 0;
				default:
					throw new RuntimeException("Unhandled check");
				}
			}

		};
		ServiceProvider provider = new AxisServiceProvider();
		ServiceContext context = provider.getContext(manager);

		assertTrue(manager.check(0));
	}

	@Test
	public void getContextService()
	{
		TestServiceContextManager manager = new TestServiceContextManager();
		ServiceProvider provider = new AxisServiceProvider();
		ServiceContext context = provider.getContext(manager);

		for (Class<? extends Service> service : supported)
			assertNotNull(context.getService(service));
		for (Class<? extends Service> service : unsupported)
			assertNull(context.getService(service));
	}

	@Test
	public void getContextEquals()
	{
		TestServiceContextManager manager = new TestServiceContextManager();
		ServiceProvider provider = new AxisServiceProvider();
		ServiceContext context = provider.getContext(manager);

		for (Class<? extends Service> service : supported)
			assertEquals(context.getService(service),
				     context.getService(service));
	}

}
