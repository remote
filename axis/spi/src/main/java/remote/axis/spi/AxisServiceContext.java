package remote.axis.spi;

import remote.axis.bindings.authentication.AuthenticatorServiceLocator;
import remote.axis.bindings.authentication.Credential;
import remote.axis.bindings.moteaccess.MoteAccessServiceLocator;
import remote.axis.bindings.motedata.MoteDataServiceLocator;
import remote.axis.bindings.motedata.MoteDataTable;
import remote.client.spi.Service;
import remote.client.spi.ServiceContext;
import remote.client.spi.ServiceEvent;
import remote.client.spi.ServiceException;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceProperty;
import remote.client.spi.ServiceSession;
import remote.client.spi.service.AuthenticationService;
import remote.client.spi.service.MoteAccessService;
import remote.client.spi.service.MoteDataService;

/**
 * SPI Context for serving Axis requests.
 */
class AxisServiceContext implements ServiceContext, AuthenticationService,
				    MoteAccessService, MoteDataService {

	private static final String AXIS_LOCATION = "axis.location";
	private final ServiceManager manager;
	private final ServiceProperty<String> location;
	private AuthenticatorServiceLocator authentication;
	private MoteAccessServiceLocator moteaccess;
	private MoteDataServiceLocator motedata;
	private ServiceSession session;

	AxisServiceContext(ServiceManager manager)
	{
		this.manager = manager;
		this.location = manager.addProperty(AXIS_LOCATION,
						    "http://localhost:8080/axis/services/",
						    "Axis service location");
	}

	static boolean hasService(Class<? extends Service> service)
	{
		return service == AuthenticationService.class ||
			service == MoteAccessService.class ||
			service == MoteDataService.class;
	}

	@SuppressWarnings("unchecked")
	public <T extends Service> T getService(Class<T> service)
	{
		if (service == AuthenticationService.class) {
			if (authentication == null)
				authentication = new AuthenticatorServiceLocator();
			return (T) this;
		}
		if (service == MoteAccessService.class) {
			if (moteaccess == null)
				moteaccess = new MoteAccessServiceLocator();
			return (T) this;
		}
		if (service == MoteDataService.class) {
			if (motedata == null)
				motedata = new MoteDataServiceLocator();
			return (T) this;
		}
		return null;
	}

	private void fireEvent(ServiceEvent<?> event)
	{
		manager.fireEvent(event);
	}

	private abstract class AxisRequest<T> implements ServiceManager.Request<T> {

		protected String id() throws Throwable
		{
			if (session == null)
				throw new ServiceException("No session attached");
			if (!session.isConnected())
				throw new ServiceException("Session not connected");
			return session.getId();
		}

		protected String url(String serviceName)
		{
			return location.getValue() + serviceName;
		}

		protected long[] ids(int[] ids)
		{
			long[] motes = new long[ids.length];
			for (int j = 0; j < ids.length; j++)
				motes[j] = ids[j];
			return motes;
		}

	}

	private Boolean authenticate(AuthenticationCallback callback, Credential[] credentials, String id)
		throws Throwable
	{
		Info[] auth = new Info[credentials.length];

		for (int i = 0; i < credentials.length; i++) {
			final Credential cred = credentials[i];

			if (cred.isHidden())
				auth[i] = new Password(cred.getLabel(), cred.getLabel(), cred.isHidden()) {

					@Override
					public String getPassword()
					{
						return cred.getValue();
					}

					@Override
					public void setPassword(String name)
					{
						cred.setValue(name);
					}

				};
			else
				auth[i] = new Name(cred.getLabel(), cred.getLabel()) {

					@Override
					public String getName()
					{
						return cred.getValue();
					}

					@Override
					public void setName(String name)
					{
						cred.setValue(name);
					}

				};
		}

		callback.onAuthenticate(auth);
		Boolean result = authentication.getAuthenticator().authenticate(id, credentials);
		if (result)
			fireEvent(ServiceSession.Event.Auth.with(session));
		return result;
	}

	public Task authenticate(ServiceSession session, final AuthenticationCallback callback)
	{
		this.session = session;
		return manager.addRequest(callback, new AxisRequest<Boolean>() {

			public Boolean run() throws Throwable
			{
				Credential[] creds;

				authentication.setAuthenticatorEndpointAddress(url("authentication"));
				creds = authentication.getAuthenticator().getEmptyCredentials();

				return authenticate(callback, creds, id());
			}

	       });
	}

	public Task getMoteData(Callback<MoteDataService.Response> callback)
	{
		return manager.addRequest(callback, new AxisRequest<MoteDataService.Response>() {

			public MoteDataService.Response run() throws Throwable
			{
				String id = session == null ? "" : id();
				motedata.setMoteDataEndpointAddress(url("motedata"));
				MoteDataTable table = motedata.getMoteData().getMoteData(id);

				return AxisMoteDataResponse.from(table);
			}

		});
	}

	public Task getPrivileges(final int[] motes, Callback<Boolean[]> callback)
	{
		return manager.addRequest(callback, new AxisRequest<Boolean[]>() {

		       public Boolean[] run() throws Throwable
		       {
			       Boolean[] result = new Boolean[motes.length];
			       String id = session.getId();
			       int i = 0;

			       moteaccess.setMoteAccessEndpointAddress(url("moteaccess"));

			       for (boolean a : moteaccess.getMoteAccess().getPrivileges(ids(motes), id()))
				       result[i++] = a;
			       return result;
		       }

	       });
	}

	public Task dropPrivileges(final int[] motes, Callback<Void> callback)
	{
		return manager.addRequest(callback, new AxisRequest<Void>() {

		       public Void run() throws Throwable
		       {
			       moteaccess.setMoteAccessEndpointAddress(url("moteaccess"));
			       moteaccess.getMoteAccess().dropPrivileges(ids(motes), id());
			       return null;
		       }

	       });
	}

	public <T extends ServiceEvent> void onEvent(T event)
	{
	}

	public void shutdown()
	{
	}

}
