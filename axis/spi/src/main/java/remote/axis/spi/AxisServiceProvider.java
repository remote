package remote.axis.spi;

import remote.client.spi.Service;
import remote.client.spi.ServiceContext;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceProvider;

/**
 * The main entry point for the Axis SPI.
 */
public class AxisServiceProvider implements ServiceProvider {

	public boolean hasService(Class<? extends Service> service)
	{
		return AxisServiceContext.hasService(service);
	}

	public ServiceContext getContext(ServiceManager manager)
	{
		return new AxisServiceContext(manager);
	}

}
