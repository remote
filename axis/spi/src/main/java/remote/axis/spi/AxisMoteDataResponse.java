package remote.axis.spi;

import remote.axis.bindings.motedata.MoteDataHeader;
import remote.axis.bindings.motedata.MoteDataTable;
import remote.client.spi.ServiceResultException;
import remote.client.spi.service.MoteDataService;

/**
 * Wrapper around a MoteDataTable.
 */
class AxisMoteDataResponse implements MoteDataService.Response {

	private static final int UNKNOWN_PROPERTY_INDEX = -1;
	private final Object[][] data;
	private final MoteDataHeader[] header;
	private final MoteDataService.Attribute[] descriptors;
	private final int[] propertyIndex;

	private AxisMoteDataResponse(MoteDataTable table)
	{
		this.data = table.getData();
		this.header = table.getHeaders();

		this.descriptors = new MoteDataService.Attribute[header.length];
		for (int i = 0; i < descriptors.length; i++)
			descriptors[i] = MoteDataService.Attribute.match(header[i].getName());

		this.propertyIndex = new int[MoteDataService.Attribute.values().length];
		for (MoteDataService.Attribute prop : MoteDataService.Attribute.values())
			propertyIndex[prop.ordinal()] = findProperty(header, prop.name());
	}

	private int findProperty(MoteDataHeader[] header, String name)
	{
		for (int i = 0; i < header.length; i++) {
			if (header[i] != null &&
			    header[i].getName() != null &&
			    header[i].getName().equals(name))
				return i;
		}

		return UNKNOWN_PROPERTY_INDEX;
	}

	static MoteDataService.Response from(MoteDataTable table)
	{
		/* Sanity check the result from the wire. */
		for (MoteDataHeader header : table.getHeaders())
			if (header == null)
				throw new ServiceResultException();

		for (Object[] objArray : table.getData()) {
			if (objArray == null)
				throw new ServiceResultException();

			for (Object obj : objArray)
				if (obj == null)
					throw new ServiceResultException();
		}

		return new AxisMoteDataResponse(table);
	}

	public void visitDescriptors(MoteDataService.Visitor visitor)
	{
		for (MoteDataHeader h : header)
			if (!visitor.visit(h.getName(), h.getTitle(),
					   h.getValueclass(), h.isVisible()))
				return;
	}

	public boolean hasAttribute(MoteDataService.Attribute property)
	{
		return propertyIndex[property.ordinal()] != UNKNOWN_PROPERTY_INDEX;
	}

	public int getMoteSize()
	{
		return data.length;
	}

	@SuppressWarnings("unchecked")
	public <T> T getAttribute(int index, MoteDataService.Attribute property, T defaultValue)
	{
		if (index < 0 || index > getMoteSize())
			return defaultValue;

		int i = propertyIndex[property.ordinal()];
		if (i == UNKNOWN_PROPERTY_INDEX)
			return defaultValue;

		return MoteDataService.Attribute.valueOf(data[index][i], defaultValue);
	}

}
