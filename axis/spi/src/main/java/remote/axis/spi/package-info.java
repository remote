/** Axis Service Provider Implementation.
 *
 * Provides the authentication, moteaccess and motedata SPI interfaces.
 */
package remote.axis.spi;
