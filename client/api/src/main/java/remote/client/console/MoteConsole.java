package remote.client.console;

/**
 * Provides an interface for reading and writing data to the mote console.
 * For reading the event listener should be used. No caching is performed.
 */
public interface MoteConsole {

	/**
	 * Write data to the mote console.
	 * @param data to write to the mote console.
	 */
	void write(byte[] data);

	/**
	 * Add an event listener. Only one listener can be registered at
	 * a time.
	 * @param listener to register for receiving events.
	 */
	void addEventListener(MoteConsoleListener listener);

	/**
	 * Remove event listener if any exists.
	 */
	void removeEventListener();

}
