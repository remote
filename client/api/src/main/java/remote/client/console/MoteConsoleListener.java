package remote.client.console;

import remote.client.control.Mote;

/**
 * Provides an interface for receiving events for the mote console.
 */
public interface MoteConsoleListener extends java.util.EventListener
{

	/**
	 * Data has been received from the mote console.
	 * @param mote from which data has been received.
	 * @param data that has been received.
	 */
	void onRead(Mote mote, byte[] data);

	/**
	 * Data has been written to the mote console.
	 * @param mote for which data was written.
	 */
	void onWrite(Mote mote);

}
