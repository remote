package remote.client.testbed;

import remote.util.Version;

/**
 * Representation of a testbed and the basic information about it.
 * Also acts as an interface for creating
 * sessions.
 */
public interface Testbed {

	/**
	 * The supported version of the Re·Mote Testbed Framework.
	 *
	 * @return the supported version.
	 */
	Version getVersion();

	/**
	 * The name of the testbed.
	 *
	 * @return the name of the testbed.
	 */
	String getName();

	/**
	 * Website URL for the testbed.
	 *
	 * @return the URL of the testbed website.
	 */
	String getWebsite();

	/**
	 * Where to look for properties updates for the testbed.
	 *
	 * @return the URL where testbed updates can be tracked.
	 */
	String getUpdate();

}
