package remote.client.session;

/**
 * Session listener adapter that can be extended in order
 * to simplify callbacks that only need to override one
 * of the methods.
 */
public class SessionListenerAdapter implements SessionListener {

	public void onError(Session session, Throwable caught)
	{
	}

}
