package remote.client.session;

/** Session event listener.
 *
 * This interface contains callbacks that are signaled for the
 * various session events.
 */
public interface SessionListener extends java.util.EventListener {

	/**
	 * Session error event.
	 *
	 * Signals that an error was reported during a service request
	 * or that the connection of the session has been lost.
	 * After this event no more session operations are possible
	 * and motes belonging to the session will no longer be
	 * available.
	 *
	 * Some possible errors are:
	 * <dl>
	 * <dt>{@link AuthenticationFailureException}</dt>
	 * <dd>The session could not be authenticated.</dd>
	 * <dt>{@link ServiceResultException}</dt>
	 * <dd>The session could not be authenticated.</dd>
	 * </dl>
	 *
	 * @param session	The disconnected session.
	 * @param caught	A description of the problem.
	 */
	void onError(Session session, Throwable caught);

}
