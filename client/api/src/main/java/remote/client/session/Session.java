package remote.client.session;

import remote.client.control.MoteManager;

/**
 * Mote control session interface.
 */
public interface Session {

	/**
	 * Get the mote manager for this session.
	 *
	 * Manages information about the motes in the testbed.
	 *
	 * @return The mote manager.
	 */
	MoteManager getMoteManager();

	/**
	 * Is the session connected?
	 *
	 * @return True if the session is connected.
	 */
	boolean isConnected();

	/**
	 * Is the session authenticated?
	 *
	 * @return True if the session is connected and Has been authenticated.
	 */
	boolean isAuthenticated();

	/**
	 * Add an event listener. Only one listener can be registered at
	 * a time.
	 * @param listener to register for receiving events.
	 */
	void addEventListener(SessionListener listener);

	/**
	 * Remove event listener if any exists.
	 */
	void removeEventListener();

	/**
	 * Close the session and all related resources.
	 *
	 * Revokes control of all privileged motes.
	 *
	 * @throws IllegalStateException
	 *	If the session has already been cosed.
	 */
	void close() throws IllegalStateException;

}
