package remote.client.control;

import java.util.Collection;

/**
 * Mote manager listener adapter that can be extended in order
 * to simplify callbacks that only need to override one
 * of the methods.
 */
public class MoteManagerListenerAdapter implements MoteManagerListener {

	public void onUpdate(MoteManager manager, Collection<Mote> newMotes, Collection<Mote> delMotes, Collection<Mote> changedMotes)
	{
	}

}
