package remote.client.control;

/**
 * Mote listener adapter that can be extended in order
 * to simplify callbacks that only need to override one
 * of the methods.
 */
public class MoteListenerAdapter implements MoteListener {

	public void onControl(Mote mote)
	{
	}

	public void onStart(Mote mote, MoteResult result)
	{
	}

	public void onStop(Mote mote, MoteResult result)
	{
	}

	public void onReset(Mote mote, MoteResult result)
	{
	}

	public void onProgram(Mote mote, MoteResult result)
	{
	}

	public void onLost(Mote mote)
	{
	}

}
