package remote.client.control;

import java.util.Iterator;

/**
 * Manages information about the motes in the testbeds.
 */
public interface MoteManager {

	/** Iterator for all motes.
	 *
	 * @return		Iterator for all motes.
	 */
	Iterator<Mote> getMotes();

	/** Iterator for controlled motes.
	 *
	 * @return		Iterator for controlled motes.
	 */
	Iterator<Mote> getControlledMotes();

	/** Iterator for mote hosts.
	 *
	 * @return		Iterator for all mote hosts.
	 */
	Iterator<MoteHost> getHosts();

	/** Iterator for mote sites.
	 *
	 * @return		Iterator for all mote sites.
	 */
	Iterator<MoteSite> getSites();

	/**
	 * Update the mote information of the manager. On completion the
	 * {@link MoteManagerListener#onUpdate onUpdate} callback in the
	 * {@link MoteManagerListener} is called.
	 *
	 * @return True if a new update was started.
	 */
	boolean update();

	/**
	 * Create a new mote list.
	 *
	 * @return		The new mote list.
	 */
	MoteList newMoteList();

	/**
	 * Add an event listener. Only one listener can be registered at
	 * a time.
	 * @param listener to register for receiving events.
	 */
	void addEventListener(MoteManagerListener listener);

	/**
	 * Remove event listener if any exists.
	 */
	void removeEventListener();

}
