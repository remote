package remote.client.control;

import java.util.Iterator;

/**
 * Motes are attached to mote hosts, which are responsible for programming
 * and performing the other mote control tasks.
 */
public interface MoteHost {

	/**
	 * Get the IP address of the mote host.
	 * @return The IP address of the mote host.
	 */
	String getIp();

	/**
	 * Get the DNS host name of the mote host.
	 * @return The DNS host name.
	 */
	String getDns(); // getHostname()

	/**
	 * Get an iterator for all motes attached to the mote host.
	 * @return Mote iterator.
	 */
	Iterator<Mote> getMotes();

	/**
	 * Get an iterator for all sites associated with the mote host.
	 * @return Mote site iterator.
	 */
	Iterator<MoteSite> getSites();

}
