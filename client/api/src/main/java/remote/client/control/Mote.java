package remote.client.control;

import remote.client.console.MoteConsole;

/**
 * One mote interface
 */
public interface Mote extends MoteControl {

	/** Get mote ID.
	 *
	 * @return	The mote ID.
	 */
	int getId();

	/** Get the mote status.
	 *
	 * @return	The mote status.
	 */
	MoteStatus getStatus();

	/** Get last command.
	 *
	 * @return	The command that was last executed.
	 */
	MoteCommand getLastCommand();

	/** Get the mote host.
	 *
	 * @return	The host to which the mote is connected.
	 */
	MoteHost getHost();

	/** Get the mote site.
	 *
	 * @return	The site where the mote is located.
	 */
	MoteSite getSite();

	/** Get the mote console.
	 *
	 * @return	The mote console.
	 */
	MoteConsole getConsole();

}
