package remote.client.control;

import java.util.Collection;

/**
 * Provides callbacks for the mote manager.
 */
public interface MoteManagerListener extends java.util.EventListener {

	/**
	 * After each update this callback will inform about new motes
	 * and motes that are no longer available.
	 *
	 * @param manager	The mote manager.
	 * @param newMotes	Motes that have been added since last update.
	 * @param delMotes	Motes that are no longer available.
	 * @param changedMotes	Motes where availability has changed.
	 */
	void onUpdate(MoteManager manager, Collection<Mote> newMotes,
		      Collection<Mote> delMotes, Collection<Mote> changedMotes);

}
