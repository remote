package remote.client.control;

/** Basic mote control interface.
 *
 * The interface is defined to be usable both for
 * blocking and non-blocking clients.
 */
public interface MoteControl {

	/**
	 * Start the mote.
	 * @return FAILURE if the mote could not be started.
	 */
	MoteResult start();

	/**
	 * Stop the mote.
	 * @return FAILURE if the mote could not be stopped.
	 */
	MoteResult stop();

	/**
	 * Reset the mote.
	 * @return FAILURE if the mote could not be reset.
	 */
	MoteResult reset();

	/**
	 * Program the mote.
	 * @param binary to program the mote with.
	 * @return FAILURE if the mote could not be programmed.
	 */
	MoteResult program(String binary);

	/**
	 * Cancel any pending command.
	 * @return FAILURE if no command was canceled.
	 */
	MoteResult cancel();

	/**
	 * Get control over this mote.
	 *
	 * @return The result of the request.
	 */
	MoteResult control();

	/**
	 * Is the mote available for control.
	 *
	 * @return	True if the mote is not in use.
	 */
	boolean isAvailable();

	/**
	 * Is the mote controlled.
	 *
	 * @return	True if the mote is controlled by this session.
	 */
	boolean isControlled();

	/**
	 * Release control of the mote.
	 */
	void release();

	/**
	 * Add an event listener. Only one listener can be registered at a time.
	 * @param listener to register for receiving events.
	 */
	void addEventListener(MoteListener listener);

	/**
	 * Remove event listener if any exists.
	 */
	void removeEventListener();

}