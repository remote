package remote.client.control;

/** Mote commands. */
public enum MoteCommand {

	/** Reset the mote. */
	RESET,
	/** Start the mote. */
	START,
	/** Stop the mote. */
	STOP,
	/** Program the mote. */
	PROGRAM,
	/** Get mote status. */
	STATUS,
	/** Unknown (last) command. */
	UNKNOWN;

	@Override
	public String toString()
	{
		return name().toLowerCase();
	}

}
