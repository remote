package remote.client.control;

/** Mote command results. */
public enum MoteResult {

	/** The result is success. */
	SUCCESS,
	/** The result is failure. */
	FAILURE,
	/** The command is not supported. */
	NOT_SUPPORTED,
	/** Waiting for completion and the result. */
	UNKNOWN;

	@Override
	public String toString()
	{
		return name().toLowerCase();
	}

}
