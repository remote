package remote.client.control;

import java.util.Collection;

/**
 * Provides an interface for controlling a list of motes.
 */
public interface MoteList extends MoteControl, Collection<Mote> {

	/**
	 * {@inheritDoc}
	 *
	 * @return True if all motes in the list are controlled, false otherwise.
	 */
	boolean isControlled();

	/**
	 * {@inheritDoc}
	 *
	 * @return True if all motes in the list are available, false otherwise.
	 */
	boolean isAvailable();

}
