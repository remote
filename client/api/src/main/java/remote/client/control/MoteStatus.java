package remote.client.control;

/** Mote status type. */
public enum MoteStatus {

	/** The mote is no longer available. */
	UNAVAILABLE,
	/** The mote is powered off. */
	STOPPED,
	/** The mote is running. */
	RUNNING,
	/** The mote is being programmed. */
	PROGRAMMING,
	/** The status of the mote is unknown. */
	UNKNOWN;

	@Override
	public String toString()
	{
		return name().toLowerCase();
	}

}
