package remote.client.control;

/**
 * Mote event notification interface. Changes and responses
 * to mote control commands are notified using this interface.
 */
public interface MoteListener extends java.util.EventListener {

	/**
	 * Control over the mote was acquired.
	 *
	 * @param mote		The lost mote.
	 */
	void onControl(Mote mote);

	/**
	 * The start command completed.
	 *
	 * @param mote		The mote.
	 * @param result	The result of the command.
	 */
	void onStart(Mote mote, MoteResult result);

	/**
	 * The stop command completed.
	 *
	 * @param mote		The mote.
	 * @param result	The result of the command.
	 */
	void onStop(Mote mote, MoteResult result);

	/**
	 * The reset command completed.
	 *
	 * @param mote		The mote.
	 * @param result	The result of the command.
	 */
	void onReset(Mote mote, MoteResult result);

	/**
	 * The program command completed.
	 *
	 * @param mote		The mote.
	 * @param result	The result of the command.
	 */
	void onProgram(Mote mote, MoteResult result);

	/**
	 * The mote was lost. Either the mote is no longer
	 * available or another user acquired control over
	 * the mote.
	 *
	 * @param mote		The lost mote.
	 */
	void onLost(Mote mote);

}
