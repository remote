package remote.client.control;

import java.util.Iterator;

/**
 * The location of each mote in the testbed is provided as a mote site.
 */
public interface MoteSite {

	/**
	 * Get the site ID.
	 * @return The site ID.
	 */
	int getId();

	/**
	 * Get the site name.
	 * @return The site name.
	 */
	String getName();

	/**
	 * Get an iterator for all motes associated with the site.
	 *
	 * Note: normally, only a single mote is associated with any given site.
	 * However, when no site has been configured a mote is associated with a
	 * "default mote site".
	 *
	 * @return A mote iterator.
	 */
	Iterator<Mote> getMotes();

}
