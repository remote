package remote.client.spi.motedata;

import org.junit.Test;
import remote.client.spi.service.MoteDataService.Attribute;
import remote.client.spi.service.MoteDataService.Usage;
import static org.junit.Assert.*;

public class MoteDataServiceTest {

	@Test
	public void match()
	{
		for (Attribute prop : Attribute.values())
			assertEquals(prop, Attribute.match(prop.name()));
		assertEquals(Attribute.unknown, Attribute.match("MOTE_ID"));
		assertEquals(Attribute.unknown, Attribute.match("abc"));
		assertEquals(Attribute.unknown, Attribute.match("123"));
	}

	@Test
	public void valueofString()
	{
		assertEquals("1", Attribute.valueOf(new Integer(1), "asdf"));
	}

	@Test
	public void valueofLong()
	{
		assertEquals(Long.valueOf(1), Attribute.valueOf("1", Long.valueOf(-1)));
		assertEquals(Long.valueOf(1), Attribute.valueOf(Integer.valueOf(1), Long.valueOf(-1)));
	}

	@Test
	public void valueofUsage()
	{
		for (Usage usage : Usage.values()) {
			assertEquals(usage,
				     Attribute.valueOf(usage.name(), Usage.UNKNOWN));
			assertEquals(usage,
			             Attribute.valueOf(usage.name().toLowerCase(), Usage.UNKNOWN));
		}

		assertEquals(Usage.UNKNOWN, Attribute.valueOf("asdf", Usage.UNKNOWN));
	}

}