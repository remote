package remote.client.spi;

import java.util.EventObject;

/**
 * An event for signaling service state changes. The service event system
 * can be used by service providers for intercommunication. E.g. one system
 * can advertise a session and another can advertise authentication of that
 * session.
 *
 * @param <T> Type enclosed in the event.
 */
public class ServiceEvent<T> extends EventObject {

	private final String text;
	/**
	 * Value with information about the event.
	 */
	protected final T value;

	protected ServiceEvent(T value, String text)
	{
		super(value);
		this.value = value;
		this.text = text;
	}

	/**
	 * Get the event message.
	 * @return the event message.
	 */
	public String getMessage()
	{
		return text;
	}

}
