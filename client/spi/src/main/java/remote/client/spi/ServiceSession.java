package remote.client.spi;

/**
 * Low-level session abstraction for the service infrastructure.
 */
public interface ServiceSession {

	/**
	 * Get the session ID.
	 *
	 * @return the session ID.
	 */
	String getId();

	/**
	 * Is the session connected?
	 *
	 * @return true if the session is connected.
	 */
	boolean isConnected();

	/**
	 * Is the session authenticated?
	 *
	 * @return true if the session is authenticated.
	 */
	boolean isAuthenticated();

	/**
	 * Service session related events.
	 */
	public static class Event extends ServiceEvent<ServiceSession> {

		private Event(ServiceSession session, String text)
		{
			super(session, text);
		}

		/**
		 * Get the session related to this event.
		 * @return the session.
		 */
		public ServiceSession getSession()
		{
			return value;
		}

		/**
		 * Event for publishing creation of a session.
		 */
		public static class Init extends Event {

			private Init(ServiceSession session)
			{
				super(session, "Created session " + session.getId());
			}

			/**
			 * Create event for a session.
			 * @param session for this event.
			 * @return the new event.
			 */
			public static Event.Init with(ServiceSession session)
			{
				return new Init(session);
			}

		}

		/**
		 * Event for signaling authentication of a session.
		 */
		public static class Auth extends Event {

			private Auth(ServiceSession session)
			{
				super(session, "Authenticated session " + session.getId());
			}

			/**
			 * Create authentication event for a session.
			 * @param session for this event.
			 * @return the new event.
			 */
			public static Event.Auth with(ServiceSession session)
			{
				return new Auth(session);
			}

		}

		/**
		 * Event for signaling that a session is closing.
		 */
		public static class Exit extends Event {

			private Exit(ServiceSession session)
			{
				super(session, "Closing session " + session.getId());
			}

			/**
			 * Close event for a session.
			 * @param session for this event.
			 * @return the new event.
			 */
			public static Event.Exit with(ServiceSession session)
			{
				return new Exit(session);
			}

		}

	}
	
}
