package remote.client.spi;

/**
 * Unchecked exception thrown when an unexpected result was
 * returned from a service request.
 */
public class ServiceResultException extends IllegalArgumentException {

	/**
	 * Creates a new instance of ServiceResultException.
	 */
	public ServiceResultException()
	{
		this("Unexpected service request result.");
	}

	/**
	 * Creates a new instance of ServiceResultException with error message.
	 *
	 * @param message The error message.
	 */
	public ServiceResultException(String message)
	{
		super(message);
	}

}
