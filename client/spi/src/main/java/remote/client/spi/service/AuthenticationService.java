package remote.client.spi.service;

import remote.client.spi.Service;
import remote.client.spi.ServiceSession;

/**
 * Service for session authentication.
 *
 * <p> The service permits multiple variations of the number of
 * credential fields clients should provide and clients should
 * support this gracefully. It can choose not support all possible
 * combinations. Clients should assume that credentials that have
 * been marked hidden may contain information sensitive to the user
 * and they should process these credentials accordingly. </p>
 *
 * <p>Some clients will not need to deal with authentication directly, because
 * it takes place upfront, e.g. in the case of a web client where the user
 * is first presented with a login prompt. For this reason the authentication
 * package is optional and separated from the rest of the client API. It also
 * has been designed assuming a threaded architecture, where the authentication
 * process can take place in a separate "modal" GUI thread without blocking
 * the main application. This means that web clients will not be able to use
 * it, but as already mentioned they can use other means to accomplish this.</p>
 *
 * <p>Inspired by the Java Authentication and Authorization Service (JAAS).</p>
 * @see <a href="http://java.sun.com/javase/6/docs/api/javax/security/auth/callback/package-summary.html">
 *	the javax.security.auth.callback package</a>

 */
public interface AuthenticationService extends Service {

	public interface AuthenticationCallback extends Callback<Boolean> {

		/**
		 * Request for the client to authenticate the session. This method
		 * may be called multiple times during an authentication exchange if
		 * supplementary credentials are needed.
		 *
		 * @param infos containing information on how to authenticate.
		 * @throws UnsupportedException
		 *	if a callback is not supported by a handler.
		 */
		void onAuthenticate(Info[] infos)
			throws UnsupportedException;

	}

	/** Authenticate session using the given handler.
	 *
	 * Authenticate an already created session using the handler to
	 * request credentials from the client. On success, the result is a
	 * Boolean object indicating whether the session was authenticated.
	 *
	 * @param session that needs authentication.
	 * @param callback for communicating the response and requesting
	 *	credentials from the client.
	 * @return the service task.
	 * @throws IllegalArgumentException
	 *	if the session is already authenticated.
	 */
	Task authenticate(ServiceSession session, AuthenticationCallback callback)
		throws IllegalArgumentException;

	/**
	 * Base interface for authentication credentials.
	 */
	public class Info {

		private final String text;
		private final String help;
		private final String type;

		protected Info(String type, String text, String help)
		{
			this.type = type;
			this.help = help;
			this.text = text;
		}

		/**
		 * Utility method to mark an authentication item unsupported.
		 * @throws UnsupportedException unconditionally.
		 *	
		 */
		public void unsupported() throws UnsupportedException
		{
			throw new UnsupportedException(this);
		}

		/**
		 * Get help / tooltip message for this callback. The help text can
		 * optionally be presented to the user, e.g. upon request or when
		 * hovering over an input field.
		 *
		 * @return help / tooltip message text.
		 */
		public String getHelp()
		{
			return help;
		}

		/**
		 * Get message / prompt text describing the purpose of the callback.
		 * The text should <b>always</b> be presented to the user.
		 *
		 * @return the text which should be presented to the user.
		 */
		public String getText()
		{
			return text;
		}

		/**
		 * Get the type of the callback. The text should not be presented to
		 * the user, but can be used when reading credentials from a
		 * configuration file.
		 *
		 * @return the type of the callback.
		 */
		public String getType()
		{
			return type;
		}

	}

	/**
	 * Interface to exchange name or identity information. This can for example
	 * be used to request a user name and/or a project name.
	 */
	public abstract class Name extends Info {

		public Name(String text, String help)
		{
			super("name", text, help);
		}

		/**
		 * Get currently set name.
		 *
		 * @return the name.
		 */
		public abstract String getName();

		/**
		 * Set name to new value.
		 *
		 * @param name containing the new value.
		 */
		public abstract void setName(String name);

	}

	/**
	 * Interface to exchange password or hidden user input.
	 */
	public abstract class Password extends Info {

		private final boolean hidden;

		public Password(String text, String help, boolean hidden)
		{
			super("password", text, help);
			this.hidden = hidden;
		}

		/**
		 * Get current password.
		 *
		 * @return the password or null if no password has been set.
		 */
		public abstract String getPassword();

		/**
		 * Set the password to a new value.
		 *
		 * @param password to use as the new value.
		 */
		public abstract void setPassword(String password);

		/**
		 * Should the text field hide the user input. Whether the password
		 * can be echoed verbatim to the display or if it should be hidden
		 * or obscured.
		 *
		 * @return true if the password should be hidden.
		 */
		public boolean isHidden()
		{
			return hidden;
		}

	}

	/**
	 * Exception thrown when a client does not support a given
	 * authentication callback.
	 */
	public static class UnsupportedException extends Exception {

		private final Info info;

		public UnsupportedException(Info info)
		{
			super("Authentication info " + info.getClass().getName() + " unsupported.");
			this.info = info;
		}

		/**
		 * Get the unsupported proxy object.
		 *
		 * @return the unsupported handler.
		 */
		public Info getInfo()
		{
			return info;
		}

	}

}
