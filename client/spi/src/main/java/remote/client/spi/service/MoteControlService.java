package remote.client.spi.service;

import remote.client.spi.Service;

/**
 * Service for controlling motes.
 */
public interface MoteControlService extends Service {

	/**
	 * Mote control service status codes.
	 */
	public enum Status {

		/** The status of the mote is unknown. */
		UNKNOWN(0),
		/** The mote is powered off. */
		STOPPED(1),
		/** The mote is running. */
		RUNNING(2),
		/** The mote is being programmed. */
		PROGRAMMING(3),
		/** The mote is not controlled. */
		AVAILABLE(4),
		/** The mote is not found or no longer available. */
		UNAVAILABLE(5),
		/** The mote is controlled by another entity. */
		OCCUPIED(6);
		private int code;

		Status(int code)
		{
			this.code = code;
		}

		/**
		 * Get the low-level status code.
		 *
		 * @return The status code.
		 */
		public int getCode()
		{
			return code;
		}

	}

	/**
	 * Mote control service command result codes.
	 */
	public enum Result {

		/** The command succeeded. */
		SUCCESS(0),
		/** The command failed. */
		FAILURE(1),
		/** The command is not supported. */
		NOT_SUPPORTED(2);
		private int code;

		Result(int code)
		{
			this.code = code;
		}

		/**
		 * Get the result code.
		 *
		 * @return The low-level code.
		 */
		public int getCode()
		{
			return code;
		}

	}

	/**
	 * Mote control service command codes.
	 */
	public enum Command {

		/** Get mote status command. */
		STATUS(0),
		/** Start command. */
		START(1),
		/** Stop command. */
		STOP(2),
		/** Reset command. */
		RESET(3),
		/**
		 * Program command.
		 *
		 * Expects the program binary to use when programming as the
		 * an additional argument to {@link MoteControlService#execute}.
		 */
		PROGRAM(4),
		/** Invalid command. */
		INVALID(5);
		private int code;

		Command(int code)
		{
			this.code = code;
		}

		/**
		 * Get the command code.
		 *
		 * @return The low-level code.
		 */
		public int getCode()
		{
			return code;
		}

	}

	/**
	 * Result wrapper for the command, result and status information
	 * that the mote control server returns when executing a command.
	 */
	public interface Response {

		/** Get the command that was executed.
		 *
		 * @return		The mote command.
		 */
		public Command getCommand();

		/** Get result.
		 *
		 * @return		The mote control result.
		 */
		public Result getResult();

		/** Get mote status.
		 *
		 * @return		The mote status.
		 */
		public Status getStatus();

	}

	/**
	 * Request mote control.
	 *
	 * For each mote that are queried, an associated boolean value is
	 * returned indicating whether the privileges were granted for the
	 * mote or denied.
	 *
	 * @param ids of the motes to control.
	 * @param callback for communicating the response.
	 * @return the service task.
	 */
	Task requestControl(int[] ids, Callback<Boolean[]> callback);

	/**
	 * Release mote control.
	 *
	 * @param ids of the motes that should not be controlled.
	 * @param callback for communicating the response.
	 * @return the service task.
	 */
	Task releaseControl(int[] ids, Callback<Void> callback);

	/**
	 * Execute a mote command.
	 *
	 * On success, the result object is an array of
	 * {@link Response} objects with an
	 * entry for each mote.
	 *
	 * @param command to execute.
	 * @param ids of the motes for which to execute command.
	 * @param args additional arguments.
	 * @param callback for communicating the response.
	 * @return the service task.
	 */
	Task execute(Command command, int[] ids, String[] args, Callback<Response[]> callback);

}
