package remote.client.spi;

/**
 * A context providing access to instanciate services and
 * administrate them.
 */
public interface ServiceContext extends ServiceEventListener {

	/**
	 * Create an instance of a service. Note, that the same instance might
	 * be returned from to consecutive calls on the same context.
	 *
	 * @param <T> The type of the service.
	 * @param service The class of the service.
	 * @return the service instance or null if the service is not supported.
	 */
	<T extends Service> T getService(Class<T> service);

	/**
	 * Shutdown the service context.
	 */
	void shutdown();

	/**
	 * Service context related events.
	 */
	public static class Event extends ServiceEvent<ServiceContext> {

		private Event(ServiceContext context, String text)
		{
			super(context, text);
		}

		/**
		 * Get the service context.
		 * @return the service context.
		 */
		public ServiceContext getContext()
		{
			return value;
		}

		/**
		 * Event for notifying that a service context has been initalized.
		 */
		public static class Init extends Event {

			private Init(ServiceContext context)
			{
				super(context, "Initializing service context");
			}

			/**
			 * Create event for a service context.
			 * @param context for this event.
			 * @return the new event.
			 */
			public static Event.Init with(ServiceContext context)
			{
				return new Init(context);
			}

		}

		/**
		 * Event for notifying that a service context is shutting down.
		 */
		public static class Exit extends Event {

			private Exit(ServiceContext context)
			{
				super(context, "Shutting down service context");
			}

			/**
			 * Create event for a service context.
			 * @param context for this event.
			 * @return the new event.
			 */
			public static Event.Exit with(ServiceContext context)
			{
				return new Exit(context);
			}

		}

	}

}

