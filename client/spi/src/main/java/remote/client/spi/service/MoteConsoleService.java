package remote.client.spi.service;

import remote.client.spi.Service;

/**
 * Service for accessing the mote console.
 */
public interface MoteConsoleService extends Service {

	/**
	 * Callback for receiving console data.
	 */
	interface ReadCallback extends Callback<Void> {

		/**
		 * Signal that data has been read from the mote console.
		 * @param id of the mote.
		 * @param data that has been read from the mote console.
		 */
		void onRead(int id, byte[] data);

	}
	
	/**
	 * Subscribe to receive mote console data. Data as well as errors
	 * occuring during the life time of the task will be delivered
	 * using the callback.
	 *
	 * @param ids of the motes from which to read data.
	 * @param callback for receiving console data and errors.
	 * @return the service task.
	 */
	Task read(int[] ids, ReadCallback callback);

	/**
	 * Write data to the mote console.
	 *
	 * @param ids of the motes for which to write data.
	 * @param data to write to the mote console.
	 * @param callback for communicating the response.
	 * @return the service task.
	 */
	Task write(int[] ids, byte[] data, Callback<Void> callback);

}
