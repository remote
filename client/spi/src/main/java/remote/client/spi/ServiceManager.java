package remote.client.spi;

import remote.util.Cancellable;

/**
 * The service manager provides an way for connecting
 * to the different services provided by a testbed. It keeps track
 * of what methods of transport, protocols and RPC the testbed
 * supports and acts as an intermediate layer between service
 * providers (the actual connectors to services) and session
 * management.
 *
 * Each session has its own service manager.
 */
public interface ServiceManager {

	/**
	 * Get a service property by name from the service configuration.
	 *
	 * @param <T> Type of the property
	 * @param name of the property.
	 * @param defaultValue to assign when not found.
	 * @param label of the property.
	 * @return the property or null if no such property exists.
	 */
	<T> ServiceProperty<T> addProperty(String name, T defaultValue, String label);

	/**
	 * Get service instance.
	 *
	 * Create an instance of a service. Note, that the same instance might
	 * be returned from to consequetive calls on the same context.
	 * @param <T> The type of the service.
	 * @param service The class of the service.
	 * @return the service instance or null if the service is not supported.
	 */
	<T extends Service> T get(Class<T> service);

	/**
	 * Notify event listeners.
	 *
	 * @param <T> Service type of the event source.
	 * @param event to be notified to event listeners.
	 */
	<T extends ServiceEvent> void fireEvent(T event);

	/**
	 * Runnable service request interface.
	 * @param <T> Return type of the service request.
	 */
	interface Request<T> {

		/**
		 * Run a service request.
		 * @return the result.
		 * @throws Throwable service error to be passed upstream via
		 *	{@link ServiceCallback#onError(java.lang.Throwable)}.
		 */
		T run() throws Throwable;

	}

	/**
	 * Schedule a request to run in the future.
	 *
	 * @param <T> Type of the request.
	 * @param task that will perform and handle the request.
	 * @param callback for communicating the response.
	 * @return the request to be served.
	 */
	<T> Service.Task addRequest(Service.Callback<T> callback, Request<T> task);

	/**
	 * Schedule a task to run in the future.
	 *
	 * @param task that will perform the work.
	 * @return object for job control.
	 */
	Cancellable addTask(Runnable task);

	/**
	 * Report a fatal error causing a context to exit.
	 * 
	 * @param context where the error occurred.
	 * @param caught error that caused it to exit.
	 */
	void error(ServiceContext context, Throwable caught);

	/**
	 * Shutdown the service manager.
	 */
	void shutdown();

	/**
	 * Service manager related events.
	 */
	public static class Event extends ServiceEvent<ServiceManager> {

		private Event(ServiceManager manager, String text)
		{
			super(manager, text);
		}

		/**
		 * Get the service context manager who fired the event.
		 * @return the service context manager who fired the event.
		 */
		public ServiceManager getManager()
		{
			return value;
		}

		/**
		 * Event for notifying that the service manager has been initalized.
		 */
		public static class Init extends Event {

			private Init(ServiceManager manager)
			{
				super(manager, "Initialized service manager");
			}

			/**
			 * Create event for a service context manager.
			 * @param manager for this event.
			 * @return the new event.
			 */
			public static Event.Init with(ServiceManager manager)
			{
				return new Init(manager);
			}

		}

		/**
		 * Event for notifying that the service manager is shutting down.
		 */
		public static class Exit extends Event {

			private Exit(ServiceManager manager)
			{
				super(manager, "Shutting down service manager");
			}

			/**
			 * Create event for a service context manager.
			 * @param manager for this event.
			 * @return the new event.
			 */
			public static Event.Exit with(ServiceManager manager)
			{
				return new Exit(manager);
			}

		}

	}

}
