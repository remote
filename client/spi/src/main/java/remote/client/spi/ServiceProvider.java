package remote.client.spi;

/**
 * Provides a way to query and create instances from a service provider.
 */
public interface ServiceProvider {

	/**
	 * Create a new service context to which new services can be attached.
	 * @param manager The context manager the provider context should belong to.
	 * @return the new context.
	 */
	ServiceContext getContext(ServiceManager manager);

	/**
	 * Is a service supported by this factory.
	 * @param service that is being queried.
	 * @return true if the service is supported.
	 */
	boolean hasService(Class<? extends Service> service);

}
