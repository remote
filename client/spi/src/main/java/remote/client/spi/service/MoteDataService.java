package remote.client.spi.service;

import remote.client.spi.Service;
import remote.util.Property;

/**
 * Service for getting mote information.
 *
 * <p> This web service provides clients with a method to get status
 * and other information about motes in the testbed. Clients can
 * use this information to present the user with mote attributes
 * about mote availability, MAC address, etc. </p>
 *
 * <p> Clients are required to pass their session ID in order to allow
 * the web service to list client specific information, such as
 * mote availability and status, correctly. </p>
 *
 * <p> Clients should make as few assumptions about the result as
 * possible, since it can contain mote attributes unknown to the
 * client. It can use the various meta data information on each
 * attribute by inspecting the associated ColumnHeader.	</p>
 *
 * <p> Information about all motes is returned and there are no
 * possibilities for the client to set limits, for example on
 * the number of motes from which to have results. </p>
 *
 * <P>The mote data can be anything from availability information, mote
 * attributes such as MAC and net address, as well as site attributes
 * such as position.</P>
 */
public interface MoteDataService extends Service {

	/**
	 * Get status information about all motes in the testbed.
	 *
	 * Get all info about the testbed motes including mote and location
	 * attributes in a single table.
	 *
	 * @param callback for communicating the response.
	 * @return the service task.
	 */
	Task getMoteData(Callback<Response> callback);

	/**
	 * The various known mote attributes given
	 * in the MoteDataService result.
	 */
	public enum Attribute {

		mote_id(Long.class),
		mote_usage(String.class),
		site_id(Long.class),
		site(String.class),
		hostip(String.class),
		hostdns(String.class),
		netaddress(String.class),
		macaddress(String.class),
		platform(String.class),
		unknown(Object.class);

		private Class classType;

		private static final Property.Converter converter = new Property.Converter() {

			@SuppressWarnings("unchecked")
			public <T> T getValue(Object value, T defaultValue)
			{
				if (defaultValue instanceof Usage)
					return (T) Usage.from(value);
				return defaultValue;
			}
			
		};

		private Attribute(Class classType)
		{
			this.classType = classType;
		}

		/**
		 * Lookup a attribute by name.
		 *
		 * @param name The name of the attribute. May be null.
		 * @return The matching attribute or {@link Attribute#unknown}.
		 */
		public static Attribute match(String name)
		{
			try {
				return name == null ? unknown : valueOf(name);
			} catch (IllegalArgumentException e) {
				return unknown;
			}
		}

		/**
		 * Convert mote attribute to specific type or return a default value.
		 * @param <T> the desired mote attribute type.
		 * @param value the transparent value.
		 * @param defaultValue to return if conversion fails
		 * @return converted value or the default value.
		 */
		public static <T> T valueOf(Object value, T defaultValue)
		{
			return converter.valueOf(value, defaultValue);
		}

	}

	public enum Usage {

		CONTROLLED,
		AVAILABLE,
		OCCUPIED,
		UNKNOWN;

		public static Usage match(String name)
		{
			for (Usage usage : values())
				if (usage.name().equalsIgnoreCase(name))
					return usage;

			return UNKNOWN;
		}

		public static final Usage from(Object value)
		{
			if (value instanceof Usage)
				return (Usage) value;
			return Usage.match(value.toString());
		}


	}

	/**
	 * Visitor interface which should be provided by callers who
	 * want to visit the attribute meta information.
	 */
	public interface Visitor {

		/**
		 * Visit the descriptor information for one mote.
		 * @param name of the attribute.
		 * @param title of the attribute.
		 * @param klass name of the attribute.
		 * @param visible whether this should be visible in a UI.
		 * @return true if the visitor should continue.
		 */
		boolean visit(String name, String title, String klass,
			      boolean visible);

	}

	/**
	 * Interface for getting the mote data result from MoteDataService.
	 */
	public interface Response {

		/**
		 * Visit all attribute descriptors.
		 * @param visitor to call for each attribute meta information.
		 */
		void visitDescriptors(Visitor visitor);

		/**
		 * Does the response contain a given attribute?
		 * @param attribute to test for.
		 * @return true if the response contains the given attribute.
		 */
		boolean hasAttribute(Attribute attribute);

		/**
		 * Get the number of motes contained in the response.
		 * @return the number of motes.
		 */
		int getMoteSize();

		/**
		 * Get mote attribute value.
		 * @param <T> The type of the expected value.
		 * @param index of the mote in the response. 
		 * @param attribute for which to return value.
		 * @param defaultValue to return if the attribute is not found.
		 * @return the converted attribute value or the provided default
		 *	value if conversion failed.
		 */
		<T> T getAttribute(int index, Attribute attribute, T defaultValue);

	}

}
