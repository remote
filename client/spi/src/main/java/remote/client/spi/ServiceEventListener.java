package remote.client.spi;

import java.util.EventListener;

/**
 * Provides a listener for service events.
 */
public interface ServiceEventListener extends EventListener {

	/**
	 * Signal a service event.
	 *
	 * @param <T> Type of the service event.
	 * @param event that is being signaled.
	 */
	<T extends ServiceEvent> void onEvent(T event);

}
