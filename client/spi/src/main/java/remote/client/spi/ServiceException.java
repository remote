package remote.client.spi;

/**
 * Unchecked exception thrown when an error occurs while
 * processing a service request.
 */
public class ServiceException extends RuntimeException {

	/**
	 * Creates a new instance of ServiceException.
	 *
	 * @param msg The exception message.
	 */
	public ServiceException(String msg)
	{
		super(msg);
	}

	/**
	 * Creates a new instance of ServiceException.
	 */
	public ServiceException()
	{
		this("Service request failure.");
	}

}
