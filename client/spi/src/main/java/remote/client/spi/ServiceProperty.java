package remote.client.spi;

import remote.util.Property;

/**
 * Shared context property.
 * 
 * @param <T> Type of the property value.
 */
public interface ServiceProperty<T> extends Property<T> {
}

