package remote.client.spi.service;

import remote.client.spi.Service;

/**
 * Service for managing mote usage privileges. Provides clients with
 * an interface for taking and dropping control over motes.
 * 
 * The set of motes the client wants to get access to can either be
 * chosen by a user if the client runs in interactive mode, or can be
 * preconfigured if the client runs in automated mode.
 */
public interface MoteAccessService extends Service {

	/**
	 * Acquire mote control privileges.
	 *
	 * For each mote that are queried using an associated boolean
	 * value is returned indicating whether the privileges were
	 * granted for the mote or denied.
	 *
	 * @param motes		IDs of motes to control.
	 * @param callback for communicating the response.
	 * @return the service task.
	 */
	Task getPrivileges(int[] motes, Callback<Boolean[]> callback);

	/**
	 * Drop mote control privileges using specific session.
	 *
	 * @param motes		IDs of motes for which to drop control.
	 * @param callback for communicating the response.
	 * @return the service task.
	 */
	Task dropPrivileges(int[] motes, Callback<Void> callback);

}
