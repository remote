package remote.client.spi;

import remote.util.Cancellable;

/** Tagging interface all services should extend. */
public interface Service {

	/**
	 * Task for job control.
	 */
	public interface Task extends Cancellable {
	}

	/**
	 * Generic service listener used for signaling success or failure of
	 * a service call. Inspired by GWT's AsyncCallback and Axis2's
	 * AxisCallback interface.
	 *
	 * @param <T> Type of the service result object.
	 * @see <a href="http://ws.apache.org/axis2/1_3/api/org/apache/axis2/client/async/AxisCallback.html">org.apache.axis2.client.async.AxisCallback</a>
	 * @see <a href="http://google-web-toolkit.googlecode.com/svn/javadoc/1.4/com/google/gwt/user/client/rpc/AsyncCallback.html">com.google.gwt.user.client.rpc.AsyncCallback</a>
	 */
	public interface Callback<T> extends java.util.EventListener {

		/** Success callback.
		 *
		 * This gets called when a result is successfully received.
		 *
		 * @param result	Result object.
		 */
		void onSuccess(T result);

		/** Error callback.
		 *
		 * This gets called ONLY when an internal processing exception occurs.
		 *
		 * @param caught	Caught exception.
		 */
		void onError(Throwable caught);

	}

	/**
	 * Service callback adapter that can be extended in order
	 * to simplify callbacks that only need to override one
	 * of the methods.
	 *
	 * @param <T> Type of the service result object.
	 */
	public class CallbackAdapter<T> implements Callback<T> {

		public void onSuccess(T result)
		{
		}

		public void onError(Throwable caught)
		{
		}

	}

}
