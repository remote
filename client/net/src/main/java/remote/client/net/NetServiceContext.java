package remote.client.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import remote.client.spi.Service;
import remote.client.spi.ServiceException;
import remote.client.spi.ServiceContext;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceEvent;
import remote.client.spi.ServiceProperty;
import remote.client.spi.ServiceSession;
import remote.client.spi.service.MoteConsoleService;
import remote.client.spi.service.MoteControlService;
import remote.mci.net.MsgPayload;
import remote.mci.net.MsgUint32;
import remote.mci.net.clientserver.ClientMsg;
import remote.mci.net.clientserver.ClientMsgType;
import remote.mci.net.clientserver.MsgClientCommand;
import remote.mci.net.clientserver.MsgClientConfirm;
import remote.mci.net.clientserver.MsgMoteIdList;
import remote.mci.net.clientserver.MsgResult;
import remote.mci.net.clientserver.MsgSession;
import remote.mci.net.motecontrol.MoteMsg;
import remote.mci.net.motecontrol.MoteMsgType;
import remote.mci.net.motecontrol.MsgCommand;
import remote.mci.net.motecontrol.MsgStatus;

/**
 * SPI Context for serving network requests.
 */
class NetServiceContext implements ServiceContext, MoteControlService,
				   MoteConsoleService {

	private static final String MCS_HOST = "mcs.host";
	private static final String MCS_PORT = "mcs.port";
	private final ServiceProperty<String> host;
	private final ServiceProperty<Integer> port;
	private final ServiceManager manager;
	private Connection connection = new Connection();
	private Session session;

	NetServiceContext(ServiceManager manager)
	{
		this.manager = manager;
		this.host = manager.addProperty(MCS_HOST, "localhost", "MCI hostname");
		this.port = manager.addProperty(MCS_PORT, 10000, "MCI port number");
		manager.addTask(newConnectTask());
	}

	static <T extends Service> boolean hasService(Class<T> service)
	{
		return service == MoteControlService.class ||
			service == MoteConsoleService.class;
	}

	@SuppressWarnings("unchecked")
	public <T extends Service> T getService(Class<T> service)
	{
		return hasService(service) ? (T) this : null;
	}

	private void fireEvent(ServiceEvent<?> event)
	{
		manager.fireEvent(event);
	}

	private void error(Throwable error)
	{
		manager.error(this, error);
	}

	public Task requestControl(int[] motes, Callback<Boolean[]> callback)
	{
		return new GetControlTask(motes, callback);
	}

	public Task releaseControl(int[] motes, Callback<Void> callback)
	{
		return new DropControlTask(motes, callback);
	}

	public Task execute(Command command, int[] motes, String[] args, Callback<Response[]> callback)
	{
		return new CommandControlTask(motes, command, args, callback);
	}

	public Task read(int[] motes, ReadCallback callback)
	{
		return new ReadConsoleTask(motes, callback);
	}

	public Task write(int[] motes, byte[] data, Callback<Void> callback)
	{
		return new WriteConsoleTask(motes, data, callback);
	}

	public void shutdown()
	{
		try {
			connection.disconnect();
		} catch (Throwable error) {
		}
		for (AbstractTask task : tasks)
			task.cancel();
	}

	public <T extends ServiceEvent> void onEvent(T event)
	{
		if (event instanceof ServiceSession.Event.Auth) {
			ServiceSession ses = ((ServiceSession.Event) event).getSession();

			if (session != null && session == ses)
				session.setAuthenticated(true);
		}
	}


	Runnable newConnectTask()
	{
		return new Runnable() {

			public void run()
			{
				try {
					connection.connect(host.getValue(), port.getValue());
					connection.startReaderThread();
				} catch (Throwable error) {
					error(error);
				}
			}

		};
	}

	private class Connection {

		private final Socket socket = new Socket();
		private volatile boolean readFromSocket = true;
		private Throwable error = null;
		private DataOutputStream out;

		private Connection()
		{
		}

		void connect(String host, int port) throws IOException
		{
			socket.connect(new InetSocketAddress(host, port));
			out = new DataOutputStream(socket.getOutputStream());
		}

		void disconnect()
		{
			readFromSocket = false;
			if (session != null) {
				session.setAuthenticated(false);
				fireEvent(ServiceSession.Event.Exit.with(session));
			}
			try {
				socket.close();
			} catch (IOException ex) {
			}
		}

		ClientMsg recvMsg(DataInputStream is) throws Exception
		{
			MsgPayload message = new MsgPayload();

			message.read(is);
			ClientMsg msg = new ClientMsg();
			msg.setBytes(message.getData());
			return msg;
		}

		private void handleClientConfirmMessage(MsgClientConfirm confirm) throws Exception
		{
			switch (confirm.getCommand().getValue()) {
			case MsgClientCommand.MSGCLIENTCOMMAND_GETMOTECONTROL:
				for (AbstractTask task : tasks)
					if (task instanceof GetControlTask &&
					    task.handle(confirm, null))
						break;
				break;
			case MsgClientCommand.MSGCLIENTCOMMAND_DROPMOTECONTROL:
				for (AbstractTask task : tasks)
					if (task instanceof DropControlTask &&
					    task.handle(confirm, null))
						break;
				break;
			case MsgClientCommand.MSGCLIENTCOMMAND_MOTEMESSAGE:
				MoteMsg msg = new MoteMsg();
				msg.setBytes(confirm.getMoteMsg().getData());

				switch (msg.getType().getValue()) {
				case MoteMsgType.CONFIRM:
					for (AbstractTask task : tasks)
						if (task instanceof CommandControlTask &&
						    task.handle(confirm, msg))
							break;
					break;
				case MoteMsgType.DATA:
					for (AbstractTask task : tasks)
						if (task instanceof ReadConsoleTask &&
						    task.handle(confirm, msg))
							break;
					break;
				default:
					throw new ServiceException("Invalid mote message!");
				}
				break;
			default:
				throw new ServiceException("Got unknown message from server!");
			}
		}

		private void handleSessionMessage(MsgSession msg)
		{
			if (session != null)
				throw new ServiceException("Session message received twice");

			session = new Session(socket, msg.getSessionId().getValue());
			fireEvent(ServiceSession.Event.Init.with(session));
		}

		private void handleMessage(DataInputStream is) throws Exception
		{
			ClientMsg msg = recvMsg(is);

			switch (msg.getType().getValue()) {
			case ClientMsgType.SESSION:
				handleSessionMessage(msg.getSession());
				break;
			case ClientMsgType.CLIENTCONFIRM:
				handleClientConfirmMessage(msg.getClientConfirm());
				break;
			default:
				throw new ServiceException("Unknown message: " +
							   msg.getType().toString());
			}
		}

		private void startReaderThread()
		{
			Thread thread = new Thread() {

				@Override
				public void run()
				{
					final DataInputStream is;

					try {
						is = new DataInputStream(socket.getInputStream());
						while (readFromSocket)
							handleMessage(is);
					} catch (Throwable error) {
						if (readFromSocket)
							error(error);
					}
				}

			};
			thread.start();
		}

		protected void requestControl(int ids[]) throws Throwable
		{
			sendMsg(MsgClientCommand.MSGCLIENTCOMMAND_GETMOTECONTROL,
				null, ids);
		}

		protected void releaseControl(int[] ids) throws Throwable
		{
			sendMsg(MsgClientCommand.MSGCLIENTCOMMAND_DROPMOTECONTROL,
				null, ids);
		}

		protected void sendMoteMsg(MoteMsg moteMsg, int[] ids) throws Throwable
		{
			sendMsg(MsgClientCommand.MSGCLIENTCOMMAND_MOTEMESSAGE, moteMsg, ids);
		}

		void sendMsg(short command, MoteMsg moteMsg, int[] ids) throws Exception
		{
			ClientMsg message = new ClientMsg();
			message.getType().setValue(ClientMsgType.CLIENTREQUEST);
			message.getClientRequest().getCommand().setValue(command);

			if (moteMsg != null)
				message.getClientRequest().getMoteMsg().
					setData(moteMsg.getBytes());

			MsgMoteIdList idlist = message.getClientRequest().getMoteIdList();
			for (int i = 0; i < ids.length; i++)
				idlist.addMoteId(new MsgUint32(ids[i]));

			MsgPayload payload = new MsgPayload();
			payload.setData(message.getBytes());
			payload.write(out);
		}

	}

	private static class Session implements ServiceSession {

		private final String id;
		private final Socket socket;
		private boolean authenticated;

		private Session(Socket socket, long id)
		{
			// FIXME: signed/unsigned problem for large session ids
			this.id = Long.toString(id);
			this.socket = socket;
		}

		public String getId()
		{
			return id;
		}

		public boolean isAuthenticated()
		{
			return authenticated && isConnected();
		}

		public boolean isConnected()
		{
			return socket.isConnected();
		}

		private void setAuthenticated(boolean authenticated)
		{
			this.authenticated = authenticated;
		}

	}

	private static List<AbstractTask> tasks = new LinkedList<AbstractTask>();

	private abstract class AbstractTask<T> implements Task, Runnable {

		protected final Callback<T> callback;
		protected final int[] motes;
		private int responses;

		AbstractTask(Callback<T> callback, int[] motes)
		{
			this.callback = callback;
			this.motes = motes;
			tasks.add(this);
		}

		protected abstract void start() throws Throwable;
		protected abstract T getResponse();
		protected abstract boolean handle(int i, Result result, MoteMsg msg);

		public boolean handle(MsgClientConfirm confirm, MoteMsg msg)
		{
			int i = getMoteIndex((int) confirm.getMote_id().getValue());

			if (i == -1)
				return false;

			Result result;
			switch (confirm.getResult().getValue()) {
			case MsgResult.SUCCESS:
				result = Result.SUCCESS;
				break;
			default:
				result = Result.FAILURE;
			}

			if (handle(i, result, msg))
				responses--;
			if (responses == 0) {
				cancel();
				callback.onSuccess(getResponse());
			}
			return true;
		}

		private int getMoteIndex(int mote)
		{
			for (int i = 0; i < motes.length; i++)
				if (motes[i] == mote)
					return i;
			return -1;
		}

		public void run()
		{
			if (session == null || !session.isAuthenticated()) {
				die(new ServiceException());
				return;
			}

			try {
				start();
				responses = motes.length;
			} catch (Throwable error) {
				die(error);
			}
		}

		public boolean isDone()
		{
			return tasks.contains(this);
		}

		public void cancel()
		{
			tasks.remove(this);
		}

		protected void die(Throwable error)
		{
			cancel();
			callback.onError(error);
		}

	};

	private abstract class AbstractReqTask<T> extends AbstractTask<T> {

		AbstractReqTask(Callback<T> callback, int[] motes)
		{
			super(callback, motes);
		}

		protected abstract boolean accept(int i, Result result);

		@Override
		protected boolean handle(int i, Result result, MoteMsg msg)
		{
			return accept(i, result);
		}
		
	}

	private abstract class AbstractMsgTask<T> extends AbstractTask<T> {

		AbstractMsgTask(Callback<T> callback, int[] motes)
		{
			super(callback, motes);
		}

		protected abstract boolean accept(int i, Result result, MoteMsg message);

		@Override
		protected boolean handle(int i, Result result, MoteMsg msg)
		{
			return accept(i, result, msg);
		}

	}

	private class GetControlTask extends AbstractReqTask<Boolean[]> {

		private final Boolean[] response;

		private GetControlTask(int[] motes, Callback<Boolean[]> callback)
		{
			super(callback, motes);
			this.response = new Boolean[motes.length];
			manager.addTask(this);
		}

		@Override
		protected void start() throws Throwable
		{
			connection.requestControl(motes);
		}

		@Override
		protected boolean accept(int i, Result result)
		{
			response[i] = result == Result.SUCCESS;
			return true;
		}

		@Override
		protected Boolean[] getResponse()
		{
			return response;
		}

	}

	private class DropControlTask extends AbstractReqTask<Void> {

		private DropControlTask(int[] motes, Callback<Void> callback)
		{
			super(callback, motes);
			manager.addTask(this);
		}

		@Override
		protected void start() throws Throwable
		{
			connection.releaseControl(motes);
		}

		@Override
		protected boolean accept(int i, Result result)
		{
			return true;
		}

		@Override
		protected Void getResponse()
		{
			return null;
		}

	}

	private class CommandControlTask extends AbstractMsgTask<Response[]> {

		private final Command command;
		private final String[] args;
		private final Response[] response;

		private CommandControlTask(int[] motes, Command command, String[] args, Callback<Response[]> callback)
		{
			super(callback, motes);
			this.command = command;
			this.args = args;
			this.response = new Response[motes.length];
			manager.addTask(this);
		}

		@Override
		protected void start() throws Throwable
		{
			MoteMsg msg = new MoteMsg();
			msg.getType().setValue(MoteMsgType.REQUEST);

			switch (command) {
			case START:
				msg.getRequest().getCommand().setValue(MsgCommand.START);
				break;
			case STOP:
				msg.getRequest().getCommand().setValue(MsgCommand.STOP);
				break;
			case RESET:
				msg.getRequest().getCommand().setValue(MsgCommand.RESET);
				break;
			case STATUS:
				msg.getRequest().getCommand().setValue(MsgCommand.STATUS);
				break;
			case PROGRAM:
				msg.getRequest().getFlashImage().setData(args[0]);
				msg.getRequest().getCommand().setValue(MsgCommand.PROGRAM);
				break;
			}

			connection.sendMoteMsg(msg, motes);
		}

		@Override
		protected boolean accept(int i, final Result result, MoteMsg message)
		{
			final Status status = toStatus(message);
			response[i] = new Response() {

				public Command getCommand()
				{
					return command;
				}

				public Result getResult()
				{
					return result;
				}

				public Status getStatus()
				{
					return status;
				}
				
			};
			return true;
		}

		@Override
		protected Response[] getResponse()
		{
			return response;
		}

		Status toStatus(MoteMsg msg)
		{
			switch (msg.getConfirm().getStatus().getValue()) {
			case MsgStatus.PROGRAMMING:
				return Status.PROGRAMMING;
			case MsgStatus.RUNNING:
				return Status.RUNNING;
			case MsgStatus.STOPPED:
				return Status.STOPPED;
			case MsgStatus.UNAVAILABLE:
				return Status.UNAVAILABLE;
			default:
				return Status.UNKNOWN;
			}
		}

	}

	private class ReadConsoleTask extends AbstractMsgTask<Void> {

		private final ReadCallback reader;

		private ReadConsoleTask(int[] motes, ReadCallback callback)
		{
			super(callback, motes);
			reader = callback;
			manager.addTask(this);
		}

		@Override
		protected void start() throws Throwable
		{
		}

		@Override
		protected boolean accept(int i, Result result, MoteMsg msg)
		{
			reader.onRead(motes[i], msg.getData().getData());
			return false;
		}

		@Override
		protected Void getResponse()
		{
			return null;
		}

	}

	private class WriteConsoleTask extends AbstractReqTask<Void> {

		private final byte[] data;

		private WriteConsoleTask(int[] motes, byte[] data, Callback<Void> callback)
		{
			super(callback, motes);
			this.data = data;
			manager.addTask(this);
		}

		@Override
		protected void start() throws Throwable
		{
			MoteMsg msg = new MoteMsg();

			msg.getType().setValue(MoteMsgType.DATA);
			msg.getData().setData(data);

			connection.sendMoteMsg(msg, motes);
		}

		@Override
		protected boolean accept(int i, Result result)
		{
			return true;
		}

		@Override
		protected Void getResponse()
		{
			return null;
		}

	}

}
