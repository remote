package remote.client.net;

import remote.client.spi.Service;
import remote.client.spi.ServiceProvider;
import remote.client.spi.ServiceContext;
import remote.client.spi.ServiceManager;

/**
 * The main entry point for the net SPI.
 */
public class NetServiceProvider implements ServiceProvider {

	public boolean hasService(Class<? extends Service> service)
	{
		return NetServiceContext.hasService(service);
	}

	public ServiceContext getContext(ServiceManager manager)
	{
		return new NetServiceContext(manager);
	}

}
