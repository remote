/**
 * Mote control and console service provider implementation.
 *
 * Provides the motecontrol and moteconsole SPI interfaces.
 */
package remote.client.net;
