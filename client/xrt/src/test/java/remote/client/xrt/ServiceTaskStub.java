package remote.client.xrt;

import remote.client.spi.Service;

class ServiceTaskStub<R> implements Service.Task {

	Service.Callback<R> callback;
	boolean cancelled = false;
	boolean done = false;

	ServiceTaskStub(Service.Callback<R> callback)
	{
		super();
		this.callback = callback;
	}

	public void cancel()
	{
		cancelled = true;
		done = true;
	}

	public boolean isCancelled()
	{
		return cancelled;
	}

	public boolean isDone()
	{
		return done;
	}

	public void success(R result)
	{
		if (callback == null)
			throw new IllegalStateException("No callback");
		callback.onSuccess(result);
		callback = null;
	}

	public void failure(Throwable caught)
	{
		if (callback == null)
			throw new IllegalStateException("No callback");
		callback.onError(caught);
		callback = null;
	}
}
