package remote.client.xrt;

import java.util.Collection;
import java.util.Iterator;
import org.junit.Before;
import org.junit.Test;
import remote.client.control.Mote;
import remote.client.control.MoteHost;
import remote.client.control.MoteList;
import remote.client.control.MoteManager;
import remote.client.control.MoteManagerListener;
import remote.client.control.MoteSite;
import remote.client.session.SessionListener;
import remote.client.session.Session;
import remote.client.spi.Service;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceSession;
import remote.client.spi.service.AuthenticationService;
import static org.junit.Assert.*;

public class SessionModelTest {

	@SuppressWarnings("unchecked")
	final static Collection<Class<? extends Service>> supports = Utils.supports(AuthenticationService.class);

	class TestSessionListener implements SessionListener {

		int connectedCalls = 0;
		int authenticatedCalls = 0;
		int disconnectedCalls = 0;

		public boolean check(int conn, int authd, int dis)
		{
			if (conn != connectedCalls)
				return false;
			if (authd != authenticatedCalls)
				return false;
			return dis == disconnectedCalls;
		}

		public void onAuthenticated(Session session)
		{
			authenticatedCalls++;
			assertTrue(authenticatedCalls == 1);
		}

		public void onError(Session session, Throwable caught)
		{
			disconnectedCalls++;
			assertTrue(disconnectedCalls == 1);
			assertNotNull(caught);
		}

	}

	class TestService implements AuthenticationService {

		ServiceTaskStub<Boolean> authRequest = null;
		boolean connected = false;

		public Task authenticate(ServiceSession session, AuthenticationCallback callback)
		{
			authRequest = new ServiceTaskStub<Boolean>(callback);
			return authRequest;
		}

		public void authenticateFailure(String reason)
		{
			authRequest.failure(new Throwable(reason));
			authRequest = null;
		}

		private void authenticateSuccess(Boolean result)
		{
			authRequest.success(result);
			authRequest = null;
		}

	}

	Session session = null;
	ServiceManager services = null;
	ServiceSession serviceSession;
	TestService service = null;
	TestSessionListener listener = null;
	private RuntimeSystemStub system;
	private MoteManager motes;

	@Before
	public void setUp()
	{
		service = new TestService();
		services = Utils.newServiceManager(supports, service);
		listener = new TestSessionListener();
		system = new RuntimeSystemStub();
		motes = new MoteManager() {

			public Iterator<Mote> getMotes()
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public Iterator<Mote> getControlledMotes()
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public Iterator<MoteHost> getHosts()
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public Iterator<MoteSite> getSites()
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public boolean update()
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public MoteList newMoteList()
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public void addEventListener(MoteManagerListener listener)
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public void removeEventListener()
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

		};
		serviceSession = new ServiceSession() {

			public String getId()
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public boolean isConnected()
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public boolean isAuthenticated()
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}


		};
		session = new SessionModel(services, motes, serviceSession) {

			@Override
			protected RuntimeSystem system()
			{
				return system;
			}

		};
	}

	@Test
	public void defaultValues()
	{
		assertFalse(session.isConnected());
		assertFalse(session.isAuthenticated());
		assertTrue(listener.check(0, 0, 0));
/*
		service.authenticateSuccess(true);

		assertTrue(session.isConnected());
		assertTrue(listener.check(1, 0, 0));
 */
	}

	/*
	@Test
	public void createFailure()
	{
		service.createSuccess(Boolean.FALSE);

		assertFalse(session.isConnected());
		assertFalse(service.isCreatePending());

		assertTrue(listener.check(0, 0, 1));
	}

	@Test
	public void authenticateSuccess()
	{
		service.createNotify(SID, true);
		assertTrue(listener.check(1, 0, 0));
		assertTrue(session.isConnected());
		assertFalse(session.isAuthenticated());

		assertTrue(compareCreds(AUTH_CRED, service.getAuthCreds()));
		assertTrue(listener.check(1, 0, 0));
		assertFalse(session.isAuthenticated());

		service.authenticateNotify(Boolean.TRUE, true);

		assertTrue(session.isAuthenticated());
		assertTrue(session.isConnected());
		assertTrue(compareCreds(AUTH_CRED, session.getCredentials()));
		assertTrue(listener.check(1, 1, 0));
	}

	@Test
	public void authenticateFailure()
	{
		service.createNotify(SID, true);
		assertTrue(listener.check(1, 0, 0));
		service.authenticateNotify(Boolean.FALSE, false);

		assertFalse(session.isAuthenticated());
		assertFalse(session.isConnected());
		assertTrue(listener.check(1, 0, 1));
	}

	@Test
	public void authenticateNoSuccess()
	{
		service.createNotify(SID, true);
		assertTrue(listener.check(1, 0, 0));

		service.authenticateNotify(Boolean.FALSE, true);
		assertFalse(session.isAuthenticated());
		assertFalse(session.isConnected());
		assertTrue(listener.check(1, 0, 1));
	}

	@Test
	public void createServiceFailure()
	{
		service.createFailure(new ServiceException());

		assertFalse(session.isConnected());
		assertFalse(service.isCreatePending());

		assertTrue(listener.getError() instanceof ServiceException);
		assertTrue(listener.check(0, 1));
	}

	@Test
	public void createSessionCredentialException()
	{
		Credential[] shortCredentials = {
			new Credential("a", "b", true)
		};

		Throwable error = null;
		try {
			sessionManager.create(serviceManager, shortCredentials, listener);

		} catch (Throwable caught) {
			error = caught;
		}

		assertNotNull(error);
		assertTrue(error instanceof SessionCredentialException);
	}

	@Test
	public void createIllegalArgumentException()
	{
		Throwable[] errors = { null, null, null };

		try {
			sessionManager.create(null, listener);
		} catch (Throwable caught) {
			errors[0] = caught;
		}

		try {
			sessionManager.create(serviceManager, null, listener);
		} catch (Throwable caught) {
			errors[1] = caught;
		}

		try {
			sessionManager.create(serviceManager, auth, null);
		} catch (Throwable caught) {
			errors[2] = caught;
		}

		for (Throwable error : errors) {
			assertNotNull(error);
			assertTrue(error instanceof IllegalArgumentException);
		}
	}

	@Test
	public void destroy()
	{
		service.createSuccess(Boolean.TRUE);
		assertTrue(session.isConnected());
		assertTrue(listener.check(1, 0));

		session.destroy();
		assertFalse(session.isConnected());
		assertTrue(listener.check(1, 0));
	}

	@Test
	public void destroyBeforeConnected()
	{
		assertNotNull(session);
		assertFalse(session.isConnected());
		assertTrue(service.isCreatePending());
		assertTrue(listener.check(0, 0));

		session.destroy();

		assertFalse(session.isConnected());
		assertFalse(service.isCreatePending());
	}

	@Test
	public void destroySessionStateException()
	{
		service.createSuccess(Boolean.TRUE);
		session.destroy();
		assertFalse(session.isConnected());
		assertTrue(listener.check(1, 0));

		Throwable error = null;
		try {
			session.destroy();

		} catch (Throwable caught) {
			error = caught;
		}

		assertNotNull(error);
		assertTrue(error instanceof SessionStateException);
	}

	*/
}
