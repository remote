package remote.client.xrt;

import org.junit.Assert;
import remote.util.Cancellable;

public class RuntimeSystemStub implements RuntimeSystem {

	public void handleError(Throwable error)
	{
		Assert.fail("Error handler called: " + error.getMessage());
	}

	public Cancellable postTask(Runnable runnable)
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void stopTasks()
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
