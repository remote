package remote.client.xrt;

import java.util.Collection;
import org.junit.Before;
import org.junit.Test;
import remote.client.control.Mote;
import remote.client.control.MoteList;
import remote.client.control.MoteManager;
import remote.client.control.MoteManagerListener;
import remote.client.spi.Service;
import remote.client.spi.ServiceManager;
import remote.client.spi.service.MoteControlService;
import remote.client.spi.service.MoteDataService;
import static org.junit.Assert.*;

public class MoteManagerModelTest {

	@SuppressWarnings("unchecked")
	final static Collection<Class<? extends Service>> supports = Utils.supports(
		MoteDataService.class, MoteControlService.class);
	private TestService service;
	private TestMoteManagerListener moteManagerListener;
	private ServiceManager services;
	private MoteManager model;
	private RuntimeSystemStub system;

	@Before
	public void setUp()
	{
		service = new TestService();
		services = Utils.newServiceManager(supports, service);
		moteManagerListener = new TestMoteManagerListener();
		system = new RuntimeSystemStub();
		model = new MoteManagerModel(services) {

			@Override
			protected RuntimeSystem system()
			{
				return system;
			}

		};
	}

	class TestMoteManagerListener implements MoteManagerListener {

		private int onUpdateCalls;
		private int onErrorCalls;
		private Throwable caught;
		private MoteManager manager;

		public void check(MoteManager manager, int onUpdateCalls, int onErrorCalls)
		{
			assertEquals(manager, this.manager);
			assertEquals(onUpdateCalls, this.onUpdateCalls);
			assertEquals(onErrorCalls, this.onErrorCalls);
		}

		public Throwable getError()
		{
			return caught;
		}

		public void onUpdate(MoteManager manager, Collection<Mote> newMotes, Collection<Mote> delMotes, Collection<Mote> changedMotes)
		{
			this.manager = manager;
			assertNotNull(manager);
			onUpdateCalls++;
			assertTrue(onUpdateCalls >= 1);
		}

		public void onError(MoteManager manager, Throwable caught)
		{
			onErrorCalls++;
			assertTrue(onErrorCalls == 1);
			assertNotNull(caught);
			assertNotNull(manager);
			this.caught = caught;
			this.manager = manager;
		}

	}

	class TestService implements MoteDataService, MoteControlService {

		private ServiceTaskStub<MoteDataService.Response> getMoteDataRequest;
		private ServiceTaskStub<Boolean[]> controlMotesRequest;
		private ServiceTaskStub<MoteControlService.Response[]> executeRequest;
		private ServiceTaskStub<Void> releaseMotesRequest;

		public Task getMoteData(Callback<MoteDataService.Response> callback)
		{
			getMoteDataRequest = new ServiceTaskStub<MoteDataService.Response>(callback);
			return getMoteDataRequest;
		}

		boolean getMoteDataIsPending()
		{
			return getMoteDataRequest != null && !getMoteDataRequest.isDone();
		}

		public void getMoteDataSuccess(MoteDataService.Response result)
		{
			getMoteDataRequest.success(result);
			getMoteDataRequest = null;
		}

		public void getMoteDataFailure(Throwable caught)
		{
			getMoteDataRequest.failure(caught);
			getMoteDataRequest = null;
		}

		public Task requestControl(int[] ids, Callback<Boolean[]> callback)
		{
			controlMotesRequest = new ServiceTaskStub<Boolean[]>(callback);
			return controlMotesRequest;
		}

		boolean controlMotesIsPending()
		{
			return controlMotesRequest != null && !controlMotesRequest.isDone();
		}

		public void controlMotesSuccess(Boolean[] result)
		{
			controlMotesRequest.success(result);
			controlMotesRequest = null;
		}

		public void controlMotesFailure(Throwable caught)
		{
			controlMotesRequest.failure(caught);
			controlMotesRequest = null;
		}

		public Task execute(Command command, int[] ids, String[] args, Callback<MoteControlService.Response[]> callback)
		{
			executeRequest = new ServiceTaskStub<MoteControlService.Response[]>(callback);
			return executeRequest;
		}

		boolean executeIsPending()
		{
			return executeRequest != null && !executeRequest.isDone();
		}

		public void executeSuccess(MoteControlService.Response[] result)
		{
			executeRequest.success(result);
			executeRequest = null;
		}

		public void executeFailure(Throwable caught)
		{
			executeRequest.failure(caught);
			executeRequest = null;
		}

		public Task releaseControl(int[] ids, Callback<Void> callback)
		{
			releaseMotesRequest = new ServiceTaskStub<Void>(callback);
			return releaseMotesRequest;
		}

		boolean releaseMotesIsPending()
		{
			return releaseMotesRequest != null && !releaseMotesRequest.isDone();
		}

		public void releaseMotesSuccess(Void result)
		{
			releaseMotesRequest.success(result);
			releaseMotesRequest = null;
		}

		public void releaseMotesFailure(Throwable caught)
		{
			releaseMotesRequest.failure(caught);
			releaseMotesRequest = null;
		}

	}

	static MoteDataService.Response simpleMoteDataResponse()
	{
		return new MoteDataService.Response() {

			public void visitDescriptors(MoteDataService.Visitor visitor)
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public boolean hasAttribute(MoteDataService.Attribute attribute)
			{
				return attribute == MoteDataService.Attribute.mote_id;
			}

			public int getMoteSize()
			{
				return 1;
			}

			@SuppressWarnings("unchecked")
			public <T> T getAttribute(int index, MoteDataService.Attribute attribute, T defaultValue)
			{
				if (index == 1 && attribute == MoteDataService.Attribute.mote_id)
					return (T) new Long(1);
				return defaultValue;
			}

		};
	}

	static MoteDataService.Response invalidMoteDataResponse()
	{
		return new MoteDataService.Response() {

			public void visitDescriptors(MoteDataService.Visitor visitor)
			{
				throw new UnsupportedOperationException("Not supported yet.");
			}

			public boolean hasAttribute(MoteDataService.Attribute attribute)
			{
				return false;
			}

			public int getMoteSize()
			{
				return 1;
			}

			@SuppressWarnings("unchecked")
			public <T> T getAttribute(int index, MoteDataService.Attribute attribute, T defaultValue)
			{
				return defaultValue;
			}

		};
	}

	@Test
	public void newMoteList()
	{
		MoteList list = model.newMoteList();

		assertNotNull(list);
		assertTrue(list.size() == 0);
	}

	@Test
	public void update()
	{
		assertFalse(service.getMoteDataIsPending());
		moteManagerListener.check(null, 0, 0);

		assertTrue(model.update());

		assertTrue(service.getMoteDataIsPending());
		moteManagerListener.check(null, 0, 0);

		service.getMoteDataSuccess(simpleMoteDataResponse());

		assertFalse(service.getMoteDataIsPending());
		moteManagerListener.check(model, 1, 0);
	}

	@Test
	public void updateSingleton()
	{
		assertFalse(service.getMoteDataIsPending());
		moteManagerListener.check(null, 0, 0);

		assertTrue(model.update());
		assertTrue(service.getMoteDataIsPending());
		moteManagerListener.check(null, 0, 0);

		assertFalse(model.update());
		assertTrue(service.getMoteDataIsPending());
		moteManagerListener.check(null, 0, 0);

		service.getMoteDataSuccess(simpleMoteDataResponse());

		assertFalse(service.getMoteDataIsPending());
		moteManagerListener.check(model, 1, 0);
	}

	@Test
	public void updateContinously()
	{
		for (int i = 0; i < 100; i++) {
			assertFalse(service.getMoteDataIsPending());
			assertTrue(model.update());
			assertTrue(service.getMoteDataIsPending());

			service.getMoteDataSuccess(simpleMoteDataResponse());

			assertFalse(service.getMoteDataIsPending());
			moteManagerListener.check(model, i + 1, 0);
		}
	}

	@Test
	public void updateWithInvalidMoteTable()
	{
		assertFalse(service.getMoteDataIsPending());
		moteManagerListener.check(null, 0, 0);

		assertTrue(model.update());

		assertTrue(service.getMoteDataIsPending());
		moteManagerListener.check(null, 0, 0);

		service.getMoteDataSuccess(invalidMoteDataResponse());

		assertFalse(service.getMoteDataIsPending());
		moteManagerListener.check(model, 0, 1);
	}

}
