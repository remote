package remote.client.xrt;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import remote.client.control.Mote;
import remote.client.control.MoteManager;
import remote.client.control.MoteManagerListener;
import remote.client.spi.Service;
import remote.client.spi.ServiceContext;
import remote.client.spi.ServiceEvent;
import remote.client.spi.ServiceEventListener;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceProperty;
import remote.util.Cancellable;

public class Utils {

	private static class TestServiceManager implements ServiceManager {

		private final Service service;
		private final Collection<Class<? extends Service>> supports;

		TestServiceManager(Collection<Class<? extends Service>> supports, Service service)
		{
			this.supports = supports;
			this.service = service;
		}

		@SuppressWarnings("unchecked")
		public <T extends Service> T get(Class<T> service)
		{
			return supports.contains(service) ? (T) this.service : null;
		}

		public void error(ServiceContext context, Throwable caught)
		{
			throw new UnsupportedOperationException("Not supported yet.");
		}

		public void shutdown()
		{
		}

		public <T> ServiceProperty<T> addProperty(String name, T defaultValue, String label)
		{
			throw new UnsupportedOperationException("Not supported yet.");
		}

		public <T extends ServiceEvent> void fireEvent(T event)
		{
			throw new UnsupportedOperationException("Not supported yet.");
		}

		public <T> Service.Task addRequest(Service.Callback<T> callback, Request<T> task)
		{
			throw new UnsupportedOperationException("Not supported yet.");
		}

		public Cancellable addTask(Runnable task)
		{
			throw new UnsupportedOperationException("Not supported yet.");
		}

	}

	public static ServiceManager newServiceManager(Collection<Class<? extends Service>> supports,
						       Service services)
	{
		return new TestServiceManager(supports, services);
	}

	public static Collection<Class<? extends Service>> supports(Class<? extends Service>... classes)
	{
		List<Class<? extends Service>> list = Arrays.asList(classes);
		return Collections.unmodifiableCollection(list);
	}

	public static MoteManagerListener newMoteManagerListener()
	{
		return new MoteManagerListener() {

			public void onUpdate(MoteManager manager, Collection<Mote> newMotes, Collection<Mote> delMotes, Collection<Mote> changedMotes)
			{
			}

			public void onError(MoteManager manager, Throwable caught)
			{
			}
			
		};
	}

}
