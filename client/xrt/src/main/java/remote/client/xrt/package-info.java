/**
 * Extendable runtime implementation of the client API. The provided
 * implementations comes with builders, which should allow them to be
 * integrated in most applications.
 */

package remote.client.xrt;
