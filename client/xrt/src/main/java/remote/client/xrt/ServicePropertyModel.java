package remote.client.xrt;

import remote.client.spi.ServiceProperty;

/**
 * Simple implementation of the service property interface.
 *
 * @param <T> Type of the property.
 */
class ServicePropertyModel<T> implements ServiceProperty<T> {

	protected String name;
	protected T value;
	protected String label;

	/**
	 * Create a service property.
	 * @param name of the property.
	 * @param label of the property.
	 * @param value of the property.
	 */
	protected ServicePropertyModel(String name, String label, T value)
	{
		this.name = name;
		this.label = label;
		this.value = value;
	}

	public String getName()
	{
		return name;
	}

	public String getLabel()
	{
		return label;
	}

	public T getValue()
	{
		return value;
	}

	public boolean isInstanceOf(Object value)
	{
		return this.value.getClass() == value.getClass();
	}

	@Override
	public String toString()
	{
		return "ServiceProperty[" + name + "=" + value + "]";
	}

}
