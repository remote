package remote.client.xrt;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import remote.client.spi.Service;
import remote.client.spi.ServiceContext;
import remote.client.spi.ServiceEvent;
import remote.client.spi.ServiceEventListener;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceProperty;
import remote.client.spi.ServiceProvider;
import remote.util.Cancellable;
import remote.util.Property;

abstract class ServiceManagerModel extends RuntimeModel implements ServiceManager {

	private final Map<String, String> settings;
	private final Map<String, ServicePropertyModel<?>> properties;
	private final Collection<ServiceContext> contexts;
	private final Collection<ServiceEventListener> listeners;
	private volatile boolean shuttingDown = false;

	protected ServiceManagerModel(Map<String, String> settings,
				      Collection<ServiceEventListener> eventListeners,
				      Collection<? extends ServiceProvider> providers)
	{
		this.settings = settings;
		this.properties = new HashMap<String, ServicePropertyModel<?>>();
		this.contexts = new HashSet<ServiceContext>();
		this.listeners = new HashSet<ServiceEventListener>(eventListeners);

		fireEvent(ServiceManager.Event.Init.with(this));

		for (ServiceProvider provider : providers) {
			ServiceContext context = provider.getContext(this);

			contexts.add(context);
			fireEvent(ServiceContext.Event.Init.with(context));
			listeners.add(context);
		}
	}

	public void error(ServiceContext context, Throwable error)
	{
		system().handleError(error);
	}

	public void shutdown()
	{
		if (shuttingDown)
			return;
		shuttingDown = true;
		for (ServiceContext context : contexts) {
			fireEvent(ServiceContext.Event.Exit.with(context));
			context.shutdown();
		}

		fireEvent(ServiceManager.Event.Exit.with(this));
		system().stopTasks();
	}

	public <T extends Service> T get(Class<T> serviceClass)
	{
		for (ServiceContext context : contexts) {
			T service = context.getService(serviceClass);
			if (service != null)
				return service;
		}
		return null;
	}

	private static final Property.Converter converter = new Property.Converter() {

		@SuppressWarnings("unchecked")
		public <T> T getValue(Object value, T defaultValue)
		{
			return defaultValue;
		}

	};

	@SuppressWarnings("unchecked")
	public <T> ServiceProperty<T> addProperty(String name, T defaultValue, String label)
	{
		ServicePropertyModel<?> known = properties.get(name);
		if (known != null) {
			return known.isInstanceOf(defaultValue)
				? (ServiceProperty<T>) known : null;
		}

		String strValue = settings.get(name);
		T value = defaultValue;
		if (strValue != null)
			value = converter.valueOf(strValue, defaultValue);

		ServicePropertyModel<T> property = new ServicePropertyModel<T>(name, label, value);

		properties.put(name, property);
		return property;
	}

	public <T extends ServiceEvent> void fireEvent(T event)
	{
		if (event instanceof ServiceContext.Event.Exit && !shuttingDown) {
			ServiceContext context = ((ServiceContext.Event) event).getContext();

			if (contexts.remove(context))
				listeners.remove(context);
		}
		for (ServiceEventListener listener : listeners)
			try {
				listener.onEvent(event);
			} catch (Throwable exception) {
				/* Ignore */
			}
	}

	private class RequestTask<T> implements Service.Task, Runnable {

		private final Cancellable task;
		private final Service.Callback<T> callback;
		private final Request<T> request;

		RequestTask(Service.Callback<T> callback, Request<T> request)
		{
			this.request = request;
			this.callback = callback;
			this.task = system().postTask(this);
		}

		public void run()
		{
			T result = null;

			cancel();
			try {
				result = request.run();
			} catch (Throwable error) {
				callback.onError(error);
				return;
			}

			callback.onSuccess(result);
		}

		public boolean isDone()
		{
			return task != null && task.isDone();
		}

		public void cancel()
		{
			if (task != null)
				task.cancel();
		}

	}

	public <T> Service.Task addRequest(Service.Callback<T> callback, Request<T> request)
	{
		return new RequestTask<T>(callback, request);
	}

	public Cancellable addTask(Runnable task)
	{
		return system().postTask(task);
	}

}
