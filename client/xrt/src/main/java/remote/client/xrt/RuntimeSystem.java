package remote.client.xrt;

import remote.util.Cancellable;

/**
 * Provides the application to tailor the client runtime to the system
 * on which it is running.
 */
public interface RuntimeSystem {

	/**
	 * Handle an error that has occurred during a service request.
	 *
	 * @param error that has occurred.
	 */
	void handleError(Throwable error);

	/**
	 * Post a runnable task for execution in the future.
	 *
	 * @param runnable task that should be scheduled.
	 * @return cancellable reference.
	 */
	Cancellable postTask(Runnable runnable);

	/**
	 * Halt the system and stop tasks from executing.
	 */
	void stopTasks();

}
