package remote.client.xrt;

import remote.client.control.Mote;
import remote.client.control.MoteCommand;
import remote.client.control.MoteListener;
import remote.client.control.MoteStatus;

abstract class MoteBaseModel extends MoteControlBaseModel implements Mote {

	protected boolean controlled = false;
	protected boolean available = false;
	protected MoteStatus status = MoteStatus.UNKNOWN;
	protected MoteCommand command = MoteCommand.UNKNOWN;

	MoteBaseModel(MoteListener listener)
	{
		super(listener);
	}

	abstract public int getId();


	public boolean isControlled()
	{
		return controlled;
	}

	public boolean isAvailable()
	{
		return available;
	}

	public MoteStatus getStatus()
	{
		return status;
	}

	public MoteCommand getLastCommand()
	{
		return command;
	}

}
