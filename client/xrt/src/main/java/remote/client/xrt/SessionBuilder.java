package remote.client.xrt;

import remote.client.control.MoteManager;
import remote.client.session.Session;
import remote.client.session.SessionListener;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceSession;

/**
 * Builder for creating a session.
 */
public class SessionBuilder {

	private final RuntimeSystem system;
	private SessionListener listener;
	private ServiceManager services;
	private ServiceSession session;
	private MoteManager motes;

	/**
	 * Get the service manager.
	 * @return the service manager.
	 */
	public ServiceManager serviceManager()
	{
		return services;
	}

	/**
	 * Set the service manager.
	 * @param serviceManager to use for the session.
	 * @return the builder.
	 */
	public SessionBuilder manager(ServiceManager serviceManager)
	{
		this.services = serviceManager;
		return this;
	}

	/**
	 * Get the mote manager.
	 * @return the mote manager.
	 */
	public MoteManager moteManager()
	{
		return motes;
	}

	/**
	 * Set the mote manager. If no mote manager has been set at build time
	 * one will be created automatically.
	 * @param moteManager to use for the session.
	 * @return the builder.
	 */
	public SessionBuilder manager(MoteManager moteManager)
	{
		this.motes = moteManager;
		return this;
	}

	/**
	 * Get the event listener.
	 * @return the event listener.
	 */
	public SessionListener listener()
	{
		return listener;
	}

	/**
	 * Set the event listener.
	 * @param listener to use for the session.
	 * @return the builder.
	 */
	public SessionBuilder listener(SessionListener listener)
	{
		this.listener = listener;
		return this;
	}

	/**
	 * Get the service session.
	 * @return the service session.
	 */
	public ServiceSession session()
	{
		return session;
	}

	/**
	 * Set the service session.
	 * @param session that the session should wrap.
	 * @return the builder.
	 */
	public SessionBuilder session(ServiceSession session)
	{
		this.session = session;
		return this;
	}

	/**
	 * Check whether calling {@link #build()} will cause an error.
	 * @return true if the builder can build.
	 */
	public boolean valid()
	{
		return services != null && session != null;
	}

	/**
	 * Build the session.
	 * @return the new session.
	 * @throws IllegalStateException if some variables have not been set.
	 */
	public Session build() throws IllegalStateException
	{
		if (!valid())
			throw new IllegalStateException();

		if (motes == null)
			motes = MoteManagerBuilder.create(system).manager(services).build();

		Session result = new SessionModel(services, motes, session) {

			@Override
			protected RuntimeSystem system()
			{
				return system;
			}

		};

		if (listener != null)
			result.addEventListener(listener);

		return result;
	}

	/**
	 * Create a new builder.
	 * @param system that the client runtime is running on.
	 * @return the builder.
	 */
	public static SessionBuilder create(RuntimeSystem system)
	{
		return new SessionBuilder(system);
	}

	private SessionBuilder(RuntimeSystem system)
	{
		this.system = system;
	}

}
