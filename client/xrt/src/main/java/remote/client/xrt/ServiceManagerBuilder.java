package remote.client.xrt;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import remote.client.spi.ServiceEventListener;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceProvider;

/**
 * Builder for creating a service manager.
 */
public class ServiceManagerBuilder
{

	private final RuntimeSystem system;
	private Map<String, String> settings;
	private Collection<ServiceEventListener> listeners = new HashSet<ServiceEventListener>();
	private Collection<ServiceProvider> providers = new HashSet<ServiceProvider>();

	/**
	 * Get the collection of all event listeners.
	 * @return the collection of all event listeners.
	 */
	public Collection<ServiceEventListener> listeners()
	{
		return listeners;
	}

	/**
	 * Add an event listener.
	 * @param listener to add to the collection of event listeners.
	 * @return the builder.
	 */
	public ServiceManagerBuilder listener(ServiceEventListener listener)
	{
		this.listeners.add(listener);
		return this;
	}

	/**
	 * Get the collection of all service providers.
	 * @return the collection of all service providers.
	 */
	public Collection<ServiceProvider> providers()
	{
		return providers;
	}

	/**
	 * Add a service provider.
	 * @param provider to add to the collection of service providers.
	 * @return the builder.
	 */
	public ServiceManagerBuilder provider(ServiceProvider provider)
	{
		this.providers.add(provider);
		return this;
	}

	/**
	 * Add a collection of service provider.
	 * @param providers to add to the collection of service providers.
	 * @return the builder.
	 */
	public ServiceManagerBuilder providers(Collection<? extends ServiceProvider> providers)
	{
		this.providers.addAll(providers);
		return this;
	}

	/**
	 * Get the settings map.
	 * @return the map of settings.
	 */
	public Map<String, String> settings()
	{
		return settings;
	}

	/**
	 * Set the settings map.
	 * @param settings that contains name-value pairs for the providers.
	 * @return the builder.
	 */
	public ServiceManagerBuilder settings(Map<String, String> settings)
	{
		this.settings = settings;
		return this;
	}

	/**
	 * Check whether calling {@link #build()} will cause an error.
	 * @return true if the builder can build.
	 */
	public boolean valid()
	{
		return settings != null && !providers.isEmpty();
	}

	/**
	 * Build the service manager.
	 * @return the new service manager.
	 * @throws IllegalStateException if some variables have not been set.
	 */
	public ServiceManager build() throws IllegalStateException
	{
		if (!valid())
			throw new IllegalStateException();

		return new ServiceManagerModel(settings, listeners, providers) {

			@Override
			protected RuntimeSystem system()
			{
				return system;
			}

		};
	}

	/**
	 * Create a new builder.
	 * @param system that the client runtime is running on.
	 * @return the builder.
	 */
	public static ServiceManagerBuilder create(RuntimeSystem system)
	{
		return new ServiceManagerBuilder(system);
	}

	private ServiceManagerBuilder(RuntimeSystem system)
	{
		this.system = system;
	}

}
