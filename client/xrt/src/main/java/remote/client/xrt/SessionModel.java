package remote.client.xrt;

import remote.client.control.MoteManager;
import remote.client.session.Session;
import remote.client.session.SessionListener;
import remote.client.session.SessionListenerAdapter;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceSession;

/**
 * This class acts as a link between the session that is exposed
 * and the internal bookkeeping done related to service handling.
 */
abstract class SessionModel extends RuntimeModel implements Session {

	private final MoteManager moteManager;
	private final ServiceManager service;
	private SessionListener listener = new SessionListenerAdapter();
	private ServiceSession serviceSession;

	SessionModel(ServiceManager serviceManager, MoteManager moteManager,
		     ServiceSession serviceSession)
	{
		this.service = serviceManager;
		this.moteManager = moteManager;
		this.serviceSession = serviceSession;
	}

	public MoteManager getMoteManager()
	{
		return moteManager;
	}

	public void close()
	{
		service.shutdown();
	}

	public boolean isConnected()
	{
		return serviceSession != null && serviceSession.isConnected();
	}

	public boolean isAuthenticated()
	{
		return serviceSession != null && serviceSession.isAuthenticated();
	}

	public void addEventListener(SessionListener listener)
	{
		this.listener = listener;
	}

	public void removeEventListener()
	{
		this.listener = new SessionListenerAdapter();
	}


}
