package remote.client.xrt;

import remote.client.control.MoteManager;
import remote.client.spi.ServiceManager;

/**
 * Builder for creating a mote manager.
 */
public class MoteManagerBuilder {

	private final RuntimeSystem system;
	private ServiceManager manager;

	/**
	 * Get the service manager.
	 * @return the service manager.
	 */
	public ServiceManager manager()
	{
		return manager;
	}

	/**
	 * Set the service manager.
	 * @param serviceManager to use for the session.
	 * @return the builder.
	 */
	public MoteManagerBuilder manager(ServiceManager serviceManager)
	{
		this.manager = serviceManager;
		return this;
	}

	/**
	 * Build the session.
	 * @return the new mote manager.
	 * @throws IllegalStateException if some variables have not been set.
	 */
	public MoteManager build()
	{
		return new MoteManagerModel(manager) {

			@Override
			protected RuntimeSystem system()
			{
				return system;
			}

		};
	}

	/**
	 * Create a new builder.
	 * @param system that the client runtime is running on.
	 * @return the builder.
	 */
	public static MoteManagerBuilder create(RuntimeSystem system)
	{
		return new MoteManagerBuilder(system);
	}

	private MoteManagerBuilder(RuntimeSystem system)
	{
		this.system = system;
	}

}
