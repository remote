package remote.client.xrt;

/**
 * The base for all runtime models which makes use of the system services.
 */
abstract class RuntimeModel {

	protected abstract RuntimeSystem system();

}
