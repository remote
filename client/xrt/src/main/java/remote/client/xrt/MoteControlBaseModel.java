package remote.client.xrt;

import remote.client.control.MoteCommand;
import remote.client.control.MoteControl;
import remote.client.control.MoteListener;
import remote.client.control.MoteListenerAdapter;
import remote.client.control.MoteResult;
import remote.util.Cancellable;

abstract class MoteControlBaseModel implements MoteControl {

	protected MoteListener listener;
	protected Cancellable request = null;

	MoteControlBaseModel(MoteListener listener)
	{
		this.listener = listener;
	}

	MoteControlBaseModel()
	{
		this(new MoteListenerAdapter());
	}

	abstract public boolean isControlled();

	abstract public boolean isAvailable();

	abstract protected Cancellable requestControl();

	abstract protected Cancellable requestRelease();

	abstract protected Cancellable requestCommand(MoteCommand command, String[] args);

	public void addEventListener(MoteListener listener)
	{
		this.listener = listener;
	}

	public void removeEventListener()
	{
		listener = new MoteListenerAdapter();
	}

	public MoteResult start()
	{
		return doCommand(MoteCommand.START);
	}

	public MoteResult stop()
	{
		return doCommand(MoteCommand.STOP);
	}

	public MoteResult reset()
	{
		return doCommand(MoteCommand.RESET);
	}

	public MoteResult program(String bin)
	{
		return doCommand(MoteCommand.PROGRAM, bin);
	}

	public MoteResult cancel()
	{
		if (isDone())
			return MoteResult.FAILURE;
		request.cancel();
		request = null;
		return MoteResult.SUCCESS;
	}

	protected boolean isDone()
	{
		return request == null || request.isDone();
	}

	private MoteResult doCommand(MoteCommand command, String... args)
	{
		if (!isControlled())
			return MoteResult.FAILURE;

		if (!isDone())
			return MoteResult.FAILURE;

		request = requestCommand(command, args);
		return request != null ? MoteResult.SUCCESS : MoteResult.FAILURE;
	}

	public MoteResult control()
	{
		if (!isDone())
			return MoteResult.FAILURE;

		if (!isAvailable())
			return MoteResult.FAILURE;

		if (!isControlled())
			request = requestControl();

		return request != null ? MoteResult.SUCCESS : MoteResult.FAILURE;
	}

	public void release()
	{
		if (isControlled() && isDone())
			request = requestRelease();
	}

}