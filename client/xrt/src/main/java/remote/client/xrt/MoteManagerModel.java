package remote.client.xrt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import remote.client.console.MoteConsole;
import remote.client.console.MoteConsoleListener;
import remote.client.control.Mote;
import remote.client.control.MoteCommand;
import remote.client.control.MoteHost;
import remote.client.control.MoteList;
import remote.client.control.MoteListener;
import remote.client.control.MoteListenerAdapter;
import remote.client.control.MoteManager;
import remote.client.control.MoteManagerListener;
import remote.client.control.MoteManagerListenerAdapter;
import remote.client.control.MoteResult;
import remote.client.control.MoteSite;
import remote.client.control.MoteStatus;
import remote.client.spi.ServiceException;
import remote.client.spi.ServiceManager;
import remote.client.spi.ServiceResultException;
import remote.client.spi.service.MoteAccessService;
import remote.client.spi.service.MoteConsoleService;
import remote.client.spi.service.MoteControlService;
import remote.client.spi.service.MoteDataService;
import remote.util.Cancellable;
import remote.util.IteratorAdapter;

/**
 * Provides a database of mote information.
 */
abstract class MoteManagerModel extends RuntimeModel implements MoteManager {

	private final ServiceManager service;
	private final Map<Integer, MoteModel> motes = new HashMap<Integer, MoteModel>();
	private final Map<Integer, SiteModel> sites = new HashMap<Integer, SiteModel>();
	private final Map<String, HostModel> hosts = new HashMap<String, HostModel>();
	private final List<MoteListModel> lists = new ArrayList<MoteListModel>();
	private final UpdateRequest update = new UpdateRequest();
	private final MoteListenerAdapter emptyMoteListener = new MoteListenerAdapter();
	private final HostModel defaultMoteHost = new HostModel("0.0.0.0", "default.motehost");
	private final SiteModel defaultMoteSite = new SiteModel(-1, "default.motesite");
	private MoteManagerListener listener = new MoteManagerListenerAdapter();
	private int updateCount;

	MoteManagerModel(ServiceManager serviceManager)
	{
		this.service = serviceManager;
	}

	public MoteList newMoteList()
	{
		return new MoteListModel();
	}

	public Iterator<Mote> getMotes()
	{
		return new IteratorAdapter<Mote, MoteModel>(motes.values());
	}

	public Iterator<Mote> getControlledMotes()
	{
		return new Iterator<Mote>() {

			Iterator<MoteModel> it = motes.values().iterator();
			MoteModel mote = null;

			private MoteModel nextControlled()
			{
				while (it.hasNext()) {
					MoteModel next = it.next();

					if (next.isControlled())
						return next;
				}

				return null;
			}

			public boolean hasNext()
			{
				mote = nextControlled();
				return mote != null;
			}

			public Mote next()
			{
				Mote next = mote;

				mote = null;
				if (next == null)
					next = nextControlled();
				if (next == null)
					throw new NoSuchElementException();
				return next;
			}

			public void remove()
			{
				throw new UnsupportedOperationException();
			}

		};
	}

	public Iterator<MoteHost> getHosts()
	{
		return new IteratorAdapter<MoteHost, HostModel>(hosts.values()) {

			@Override
			public boolean acceptNext(HostModel host)
			{
				/* XXX: Only expose when motes are attached. */
				return !host.motes.isEmpty();
			}

		};

	}

	public Iterator<MoteSite> getSites()
	{
		return new IteratorAdapter<MoteSite, SiteModel>(sites.values()) {

			@Override
			public boolean acceptNext(SiteModel site)
			{
				/* XXX: Only expose when motes are attached. */
				return !site.motes.isEmpty();
			}

		};
	}

	public void addEventListener(MoteManagerListener listener)
	{
		this.listener = listener;
	}

	public void removeEventListener()
	{
		listener = new MoteManagerListenerAdapter();
	}

	public boolean update()
	{
		return update.send();
	}

	private boolean updateMoteHost(MoteModel mote, MoteDataService.Response data, int row)
	{
		String dns = data.getAttribute(row, MoteDataService.Attribute.hostip, "localhost");
		String ip = data.getAttribute(row, MoteDataService.Attribute.hostdns, "127.0.0.1");
		HostModel host = hosts.get(ip);

		if (host == null)
			host = new HostModel(ip, dns);
		else
			host.dns = dns;

		return host.add(mote);
	}

	private boolean updateMoteSite(MoteModel mote, MoteDataService.Response data, int row)
	{
		int id = data.getAttribute(row, MoteDataService.Attribute.site_id, (int) -1);
		String name = data.getAttribute(row, MoteDataService.Attribute.site, "unknown");
		SiteModel site = sites.get(id);

		if (site == null)
			site = new SiteModel(id, name);
		else
			site.name = name;

		return site.add(mote);
	}

	private boolean updateMoteUsage(MoteModel mote, MoteDataService.Response data, int row)
	{
		switch (data.getAttribute(row, MoteDataService.Attribute.mote_usage, MoteDataService.Usage.UNKNOWN)) {
		case AVAILABLE:
			if (!mote.available) {
				mote.available = true;
				return true;
			}
			break;

		case OCCUPIED:
			if (mote.available) {
				mote.available = false;
				return true;
			}
			break;

		case CONTROLLED:
			if (!mote.controlled) {
				/* TODO: Something is wrong */
			}
			break;

		case UNKNOWN:
		default:
		}

		return false;
	}

	private boolean updateMote(MoteModel mote, MoteDataService.Response data, int row)
	{
		boolean changed = false;

		if (updateMoteUsage(mote, data, row))
			changed = true;

		if (updateMoteSite(mote, data, row))
			changed = true;

		if (updateMoteHost(mote, data, row))
			changed = true;

		/*
			case netaddress:
			case macaddress:
			case platform:
			case UNKNOWN:
				String value = (String) data.getData(row, col);
				String name = (String) data.getHeaderName(col);
				String orig = mote.configMap.put(name, value);

				if (orig == null || !orig.equals(value))
					changed = true;
		 */
		return changed;
	}

	private ServiceResultException updateDatabase(MoteDataService.Response data)
	{
		final int UNKNOWN_MOTE_ID = -1;
		MoteListModel newMotes = new MoteListModel();
		MoteListModel oldMotes = new MoteListModel();
		MoteListModel changedMotes = new MoteListModel();

		if (!data.hasAttribute(MoteDataService.Attribute.mote_id))
			return new ServiceResultException("No mote id column");

		updateCount++;

		for (int row = 0; row < data.getMoteSize(); row++) {
			Integer id = data.getAttribute(row, MoteDataService.Attribute.mote_id, UNKNOWN_MOTE_ID);

			MoteModel mote = motes.get(id);

			if (mote == null) {
				mote = new MoteModel(id);
				newMotes.addMote(mote);
				updateMote(mote, data, row);

			} else if (updateMote(mote, data, row))
				changedMotes.addMote(mote);

			mote.updateCount = updateCount;
		}

		/*
		 * Reap non-updated motes.
		 */

		for (MoteModel mote : motes.values())
			if (mote.updateCount != updateCount)
				oldMotes.add(mote);

		listener.onUpdate(this, newMotes, oldMotes, changedMotes);

		for (int i = 0; i < oldMotes.size(); i++)
			oldMotes.motes[i].delete();

		return null;
	}

	private final class MoteModel extends MoteBaseModel {

		private CommandRequest cmdRequest = null;
		private ConsoleModel console = null;
		private Map<String, String> configMap = new HashMap<String, String>();
		private int updateCount;
		private final int id;
		private HostModel host;
		private SiteModel site;

		MoteModel(int id)
		{
			super(emptyMoteListener);
			this.id = id;
			defaultMoteSite.add(this);
			defaultMoteHost.add(this);
			motes.put(id, this);
		}

		private void delete()
		{
			listener = emptyMoteListener;
			// cancel(); // FIXME: May send cmd for unknown mote.
			available = false;
			controlled = false;

			for (MoteListModel list : lists)
				if (list.remove(this))
					list.listener.onLost(this);

			site.remove(this);
			host.remove(this);
			motes.remove(this);
		}

		public int getId()
		{
			return id;
		}

		@Override
		protected Cancellable requestCommand(MoteCommand command, String[] args)
		{
			this.command = command;
			if (cmdRequest == null)
				cmdRequest = new CommandRequest(this);
			return cmdRequest.send(command, args);
		}

		@Override
		protected Cancellable requestControl()
		{
			return new ControlRequest(this);
		}

		@Override
		protected Cancellable requestRelease()
		{
			return new ReleaseRequest(this);
		}

		public MoteHost getHost()
		{
			return host;
		}

		public MoteSite getSite()
		{
			return site;
		}

		public MoteConsole getConsole()
		{
			if (console == null)
				console = new ConsoleModel(this);
			return console;
		}

	}

	private final class MoteListModel extends MoteListBaseModel {

		static final int GRANULARITY = 10;
		private int size = 0;
		private MoteModel[] motes = new MoteModel[GRANULARITY];
		private CommandRequest cmdRequest = null;
		private boolean immutable = false;

		private MoteListModel()
		{
			this(emptyMoteListener);
			this.immutable = true;
		}

		private MoteListModel(MoteListener listener)
		{
			super(listener);
			lists.add(this);
		}

		private void reallocMotes()
		{
			MoteModel[] array = new MoteModel[size + GRANULARITY];
			int i = 0;

			for (MoteModel mote : motes)
				if (mote != null)
					array[i++] = motes[i];

			motes = array;
		}

		private void updateSize(int change)
		{
			size += change;
			cmdRequest = null;
		}

		@Override
		public int size()
		{
			return size;
		}

		/* This is for internally working with immutable lists. */
		private boolean removeMote(MoteModel mote)
		{
			for (int i = 0; i < motes.length; i++)
				if (motes[i] == mote) {
					motes[i] = null;
					updateSize(-1);
					return true;
				}

			return false;
		}

		/* This is for internally working with immutable lists. */
		private boolean addMote(MoteModel newMote)
		{
			for (MoteModel mote : motes)
				if (mote == newMote)
					return false;

			if (size + 1 >= motes.length)
				reallocMotes();

			for (int i = 0; i < motes.length; i++)
				if (motes[i] == null)
					motes[i] = newMote;

			updateSize(+1);
			return true;
		}

		@Override
		public boolean add(Mote arg0)
		{
			if (!(arg0 instanceof MoteModel))
				throw new IllegalArgumentException();
			if (arg0 == null)
				throw new NullPointerException();
			if (immutable)
				throw new UnsupportedOperationException();
			return addMote((MoteModel) arg0);
		}

		@Override
		public Iterator<Mote> iterator()
		{
			return new Iterator<Mote>() {

				int i = 0;

				public boolean hasNext()
				{
					while (i < motes.length) {
						if (motes[i] != null)
							return true;
						i++;
					}

					return false;
				}

				public Mote next()
				{
					if (i >= motes.length || motes[i] == null)
						throw new NoSuchElementException();
					return motes[i++];
				}

				public void remove()
				{
					if (immutable)
						throw new UnsupportedOperationException();
					if (i == 0 || i > motes.length || motes[i - 1] == null)
						throw new IllegalStateException();
					motes[i - 1] = null;
					updateSize(-1);
				}

			};
		}

		@Override
		protected Cancellable requestCommand(MoteCommand command, String[] args)
		{
			if (immutable)
				return null;
			if (cmdRequest == null)
				cmdRequest = new CommandRequest(this);
			return cmdRequest.send(command, args);
		}

		@Override
		protected Cancellable requestRelease()
		{
			if (immutable)
				return null;

			MoteModel[] moteModels = new MoteModel[size];
			int i = 0;

			/* Only request for motes currently controlled. */
			for (MoteModel mote : motes) {
				if (mote == null)
					continue;
				assert mote.isAvailable();
				if (!mote.isControlled())
					moteModels[i++] = mote;
			}

			if (i == 0)
				return null;

			int[] moteIds = new int[i];
			for (int j = 0; j < moteIds.length; j++)
				moteIds[j] = moteModels[j].getId();

			return new ReleaseRequest(moteModels, moteIds, listener);
		}

		@Override
		protected Cancellable requestControl()
		{
			if (immutable)
				return null;

			MoteModel[] moteModels = new MoteModel[motes.length];
			int i = 0;

			/* Only request for motes not currently controlled. */
			for (MoteModel mote : motes) {
				if (mote == null)
					continue;
				assert mote.isAvailable();
				if (!mote.isControlled())
					moteModels[i] = mote;
			}

			if (i == 0)
				return null;

			int[] moteIds = new int[i];

			for (int j = 0; j < moteIds.length; j++)
				moteIds[j] = moteModels[j].getId();

			return new ControlRequest(moteModels, moteIds, listener);
		}

	}

	private final class ConsoleModel implements MoteConsole {

		private MoteModel mote;
		private MoteConsoleListener listener;
		private MoteConsoleService.Task reader;

		private ConsoleModel(MoteModel mote)
		{
			this.mote = mote;
		}

		public void addEventListener(MoteConsoleListener listener)
		{
			this.listener = listener;
			if (reader == null)
				reader = new ConsoleReadRequest(this);
		}

		public void removeEventListener()
		{
			this.listener = null;
			reader.cancel();
		}

		public void write(byte[] data)
		{
			new ConsoleWriteRequest(this, data);
		}

	}

	private final class SiteModel implements MoteSite {

		private final HashSet<MoteModel> motes = new HashSet<MoteModel>();
		private final int id;
		private String name;

		SiteModel(int id, String name)
		{
			this.id = id;
			this.name = name;
			sites.put(id, this);
		}

		private void remove(MoteModel mote)
		{
			if (motes.remove(mote))
				mote.site = null;
		}

		private boolean add(MoteModel mote)
		{
			if (mote.site == this)
				return false;
			if (mote.site != null)
				mote.site.remove(mote);
			motes.add(mote);
			mote.site = this;
			return true;
		}

		public int getId()
		{
			return id;
		}

		public String getName()
		{
			return name;
		}

		public Iterator<Mote> getMotes()
		{
			return new IteratorAdapter<Mote, MoteModel>(motes);
		}

	}

	private final class HostModel implements MoteHost {

		private final HashSet<MoteModel> motes = new HashSet<MoteModel>();
		private final String ip;
		private String dns;

		HostModel(String ip, String dns)
		{
			this.ip = ip;
			this.dns = dns;
			hosts.put(ip, this);
		}

		private void remove(MoteModel mote)
		{
			if (motes.remove(mote))
				mote.host = null;
		}

		private boolean add(MoteModel mote)
		{
			if (mote.host == this)
				return false;
			if (mote.host != null)
				mote.host.remove(mote);
			motes.add(mote);
			mote.host = this;
			return true;
		}

		public String getIp()
		{
			return ip;
		}

		public String getDns()
		{
			return dns;
		}

		public Iterator<Mote> getMotes()
		{
			return new IteratorAdapter<Mote, MoteModel>(motes);
		}

		public Iterator<MoteSite> getSites()
		{
			return new Iterator<MoteSite>() {

				Iterator<MoteModel> iterator = motes.iterator();

				public boolean hasNext()
				{
					return iterator.hasNext();
				}

				public MoteSite next()
				{
					return iterator.next().getSite();
				}

				public void remove()
				{
					throw new UnsupportedOperationException();
				}

			};
		}

	}

	private abstract class MoteRequest<T> extends RuntimeRequest<T> {

		protected MoteRequest()
		{
			super(system());
		}

		protected MoteRequest(boolean allowNullResults)
		{
			super(system(), allowNullResults);
		}

	}

	private final class UpdateRequest extends MoteRequest<MoteDataService.Response> {

		public boolean send()
		{
			if (!isDone())
				return false;
			setTask(service.get(MoteDataService.class).getMoteData(this));
			return true;
		}

		public void success(MoteDataService.Response result)
		{
			Throwable caught = null;
			try {
				caught = updateDatabase(result);

			} catch (Throwable exception) {
				caught = exception;
			}

			if (caught != null)
				onError(caught);
		}

	}

	private final class ControlRequest extends MoteRequest<Boolean[]> {

		private final MoteListener listener;
		private final MoteModel[] motes;
		private boolean hasRequestedControl = false;

		ControlRequest(MoteModel mote)
		{
			this(new MoteModel[] { mote }, new int[] { mote.getId() }, mote.listener);
		}

		ControlRequest(MoteModel[] motes, int[] ids, MoteListener listener)
		{
			this.motes = motes;
			this.listener = listener;
			setTask(service.get(MoteAccessService.class).
				getPrivileges(ids, this));
		}

		public void success(Boolean[] result)
		{
			if (!hasRequestedControl) {
				hasRequestedControl = true;
				int[] ids = new int[motes.length];
				for (int i = 0; i < ids.length; i++)
					ids[i] = motes[i].getId();
				setTask(service.get(MoteControlService.class).
					requestControl(ids, this));
				return;
			}

			for (int i = 0; i < result.length; i++)
				if (result[i].booleanValue()) {
					motes[i].controlled = true;
					listener.onControl(motes[i]);
				}
		}

	}

	private final class ConsoleWriteRequest extends MoteRequest<Void> {

		private final ConsoleModel console;

		ConsoleWriteRequest(ConsoleModel console, byte[] data)
		{
			int[] ids = new int[] { console.mote.getId() };
			this.console = console;
			setTask(service.get(MoteConsoleService.class).write(ids, data, this));
		}

		@Override
		protected void success(Void result)
		{
			if (console.listener != null)
				console.listener.onWrite(console.mote);
		}

	}

	private final class ConsoleReadRequest extends MoteRequest<Void>
				implements MoteConsoleService.ReadCallback {

		private final ConsoleModel console;

		ConsoleReadRequest(ConsoleModel console)
		{
			int[] ids = new int[] { console.mote.getId() };
			this.console = console;
			setTask(service.get(MoteConsoleService.class).read(ids, this));
		}

		@Override
		protected void success(Void result)
		{
		}

		public void onRead(int id, byte[] data)
		{
			console.listener.onRead(console.mote, data);
		}

	}

	private final class ReleaseRequest extends MoteRequest<Void> {

		private final MoteListener listener;
		private final MoteModel[] motes;

		ReleaseRequest(MoteModel mote)
		{
			this(new MoteModel[] { mote }, new int[] { mote.getId() }, mote.listener);
		}

		ReleaseRequest(MoteModel[] motes, int[] ids, MoteListener listener)
		{
			super(true);
			this.motes = motes;
			this.listener = listener;
			setTask(service.get(MoteControlService.class).releaseControl(ids, this));
		}

		public void success(Void result)
		{
		}

	}

	private final class CommandRequest extends MoteRequest<MoteControlService.Response[]> {

		private final MoteListener listener;
		private final MoteModel[] motes;
		private final int[] ids;
		private MoteCommand command;

		CommandRequest(MoteModel mote)
		{
			this.listener = mote.listener;
			this.motes = new MoteModel[] { mote };
			this.ids = new int[] { mote.getId() };
		}

		CommandRequest(MoteListModel list)
		{
			this.listener = list.listener;
			this.motes = new MoteModel[list.size()];
			this.ids = new int[list.size()];

			int i = 0;
			for (MoteModel mote : list.motes)
				if (mote != null) {
					motes[i] = mote;
					ids[i] = mote.getId();
					i++;
				}
		}

		public Cancellable send(MoteCommand command, String[] args)
		{
			if (!isDone())
				return null;

			if (command == MoteCommand.PROGRAM && args.length != 1)
				throw new IllegalArgumentException("No program binary");

			MoteControlService.Command cmd = getMoteControlCommand(command);
			if (cmd == MoteControlService.Command.INVALID)
				throw new IllegalArgumentException("Invalid command");

			setTask(service.get(MoteControlService.class).execute(cmd, ids, args, this));

			if (motes.length > 1)
				setupMoteListCallbacks();
			this.command = command;
			return this;
		}

		public void success(MoteControlService.Response[] result)
		{
			clearMoteListCallbacks();
			for (int index = 0; index < motes.length; index++) {
				MoteControlService.Response response = result[index];
				MoteModel mote = motes[index];

				if (response.getCommand() != getMoteControlCommand(command))
					onError(new ServiceResultException("Bad command"));

				switch (response.getStatus()) {
				case UNAVAILABLE:
					lostControl(mote, false);
					return;
				case OCCUPIED:
					lostControl(mote, true);
					return;
				case STOPPED:
					mote.status = MoteStatus.STOPPED;
					break;
				case RUNNING:
					mote.status = MoteStatus.RUNNING;
					break;
				case PROGRAMMING:
					mote.status = MoteStatus.PROGRAMMING;
					break;
				case AVAILABLE:
					mote.status = MoteStatus.UNKNOWN;
					onError(new Exception("Uncontrolled mote"));
					break;
				case UNKNOWN:
					break;
				}

				switch (response.getResult()) {
				case SUCCESS:
					notifyListener(mote, MoteResult.SUCCESS);
					break;
				case FAILURE:
					notifyListener(mote, MoteResult.FAILURE);
					break;
				case NOT_SUPPORTED:
				default:
					onError(new ServiceException("Command not supported"));
				}
			}
		}

		private void lostControl(MoteModel mote, boolean moteIsAvailable)
		{
			// TODO: Remove MoteModel and signal onUpdate(...)
			if (!moteIsAvailable)
				mote.available = moteIsAvailable;
			mote.controlled = false;
			listener.onLost(mote);
		}

		private void notifyListener(MoteModel mote, MoteResult result)
		{
			switch (command) {
			case RESET:
				listener.onReset(mote, result);
				break;
			case START:
				listener.onStart(mote, result);
				break;
			case STOP:
				listener.onStop(mote, result);
				break;
			case PROGRAM:
				listener.onProgram(mote, result);
				break;
			case STATUS:
				break;
			case UNKNOWN:
			default:
				break;
			}
		}

		@Override
		public void error(Throwable caught)
		{
			clearMoteListCallbacks();
		}

		/**
		 * Get the corresponding mote control service command enum.
		 *
		 * @return The mote control service command enum.
		 */
		MoteControlService.Command getMoteControlCommand(MoteCommand command)
		{
			switch (command) {
			case RESET:
				return MoteControlService.Command.RESET;
			case START:
				return MoteControlService.Command.START;
			case STOP:
				return MoteControlService.Command.STOP;
			case PROGRAM:
				return MoteControlService.Command.PROGRAM;
			case STATUS:
				return MoteControlService.Command.STATUS;
			case UNKNOWN:	/* and */
			default:
				return MoteControlService.Command.INVALID;
			}
		}

		private void clearMoteListCallbacks()
		{
			for (int i = 0; i < motes.length; i++)
				motes[i].request = null;
		}

		private void setupMoteListCallbacks()
		{
			for (int i = 0; i < motes.length; i++)
				motes[i].request = newCancellable(i);
		}

		private Cancellable newCancellable(final int index)
		{
			return new Cancellable() {

				public boolean isDone()
				{
					return motes[index].request != null;
				}

				public void cancel()
				{
					if (motes[index].request != null)
						motes[index].request = null;
				}

			};
		}

	}

}
