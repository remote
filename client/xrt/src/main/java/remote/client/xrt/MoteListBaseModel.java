package remote.client.xrt;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import remote.client.control.MoteList;
import remote.client.control.MoteListener;
import remote.client.control.Mote;

abstract class MoteListBaseModel extends MoteControlBaseModel implements MoteList {

	private final Set<Mote> set = new AbstractSet<Mote>() {

		@Override
		public boolean add(Mote mote)
		{
			return MoteListBaseModel.this.add(mote);
		}

		@Override
		public Iterator<Mote> iterator()
		{
			return MoteListBaseModel.this.iterator();
		}

		@Override
		public int size()
		{
			return MoteListBaseModel.this.size();
		}

	};

	MoteListBaseModel(MoteListener listener)
	{
		super(listener);
	}

	/*
	 * Identity.
	 */

	@Override
	public int hashCode()
	{
		return set.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		return set.equals(obj);
	}

	/*
	 * Mote control
	 */

	public boolean isControlled()
	{
		for (Mote mote : set) {
			if (!mote.isControlled())
				return false;
		}

		return true;
	}

	public boolean isAvailable()
	{
		for (Mote mote : set) {
			if (!mote.isAvailable())
				return false;
		}
		return true;
	}

	/*
	 * Collection
	 */

	public abstract int size();
	public abstract Iterator<Mote> iterator();
	public abstract boolean add(Mote mote);

	public boolean isEmpty()
	{
		return set.isEmpty();
	}

	public boolean contains(Object arg0)
	{
		return set.contains(arg0);
	}

	public Object[] toArray()
	{
		return set.toArray();
	}

	public <T> T[] toArray(T[] arg0)
	{
		return set.toArray(arg0);
	}

	public boolean remove(Object arg0)
	{
		return set.remove(arg0);
	}

	public boolean containsAll(Collection<?> arg0)
	{
		return set.containsAll(arg0);
	}

	public boolean addAll(Collection<? extends Mote> arg0)
	{
		return set.addAll(arg0);
	}

	public boolean removeAll(Collection<?> arg0)
	{
		return set.removeAll(arg0);
	}

	public boolean retainAll(Collection<?> arg0)
	{
		return set.retainAll(arg0);
	}

	public void clear()
	{
		set.clear();
	}

}

