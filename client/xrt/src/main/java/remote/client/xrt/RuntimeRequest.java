package remote.client.xrt;

import remote.client.spi.Service;
import remote.client.spi.ServiceResultException;

/**
 * Abstract request that can be extended in order to simplify
 * handling of callbacks.
 *
 * @param <T> Type of the service result object.
 */
abstract class RuntimeRequest<T> implements Service.Callback<T>, Service.Task {

	private final RuntimeSystem system;
	private final boolean allowNullResult;
	private Service.Task requestTask;

	/**
	 * Default constructor that will reject null results from requests.
	 *
	 * @param system handler for processing request errors.
	 */
	public RuntimeRequest(RuntimeSystem system)
	{
		this(system, false);
	}

	/**
	 * Constructor where it can be specified whether null results from
	 * requests is allowed.
	 *
	 * @param system handler for processing request errors.
	 * @param allowNullResult Whether to allow null results for services.
	 */
	public RuntimeRequest(RuntimeSystem system, boolean allowNullResult)
	{
		this.system = system;
		this.allowNullResult = allowNullResult;
	}

	/**
	 * Callback for receiving the result on success.
	 * @param result from the service request.
	 */
	protected abstract void success(T result);

	/**
	 * Callback for receiving errors. Request processors who needs it
	 * can override to do extra error cleanup.
	 * @param caught error during service request handling.
	 */
	protected void error(Throwable caught)
	{
	}

	/**
	 * Set the service task of this request.
	 * @param task associated with the request.
	 */
	protected final void setTask(Service.Task task)
	{
		if (requestTask != null && !requestTask.isDone())
			requestTask.cancel();			
		requestTask = task;
	}

	/**
	 * Get the service task of this request.
	 * @return The task associated with the request.
	 */
	protected final Service.Task getTask()
	{
		return requestTask;
	}

	public final void onSuccess(T result)
	{
		requestTask = null;
		if (result == null && !allowNullResult) {
			onError(new ServiceResultException());
			return;
		}

		success(result);
	}

	public final void onError(Throwable caught)
	{
		requestTask = null;
		system.handleError(caught);
		error(caught);
	}

	public boolean isDone()
	{
		return requestTask == null || requestTask.isDone();
	}

	public void cancel()
	{
		if (requestTask != null)
			requestTask.cancel();
	}

}
