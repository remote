/** Utility and Helper Package.
 *
 * <p>This package provides mostly server specific utility classes and
 * helper methods.</p>
 */
package remote.mci.service.util;
