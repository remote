package remote.mci.service.motedata;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Table column header.
 *
 * Stores information about the information available in the
 * entire SimpleTable column.
 */
@XmlRootElement(name = "header")
public class MoteDataHeader implements Serializable
{
	private static final long serialVersionUID = -4526017460513320190L;
	private String title;
	private String name;
	private boolean visible;
	private String valueclass;

	/** Create column header.
	 *
	 * @param title		The column title.
	 * @param name		The column name.
	 * @param visible	The column visibility.
	 * @param valueclass	The column valueclass.
	 */
	public MoteDataHeader(String title, String name, boolean visible, String valueclass)
	{
		this.title = title;
		this.name = name;
		this.visible = visible;
		this.valueclass = valueclass;
	}

	/** Dummy constructor to silence compiler warnings. */
	public MoteDataHeader()
	{
		this(null, null, false, null);
	}

	/** Get column header title.
	 *
	 * @return The column title. */
	@XmlElement
	public String getTitle()
	{
		return title;
	}

	/** Set column header title.
	 *
	 * @param title	The new column title.
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/** Get column header name.
	 *
	 * @return The column name.
	 */
	@XmlElement
	public String getName()
	{
		return name;
	}

	/** Set column header name.
	 *
	 * @param name	The new column name.
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/** Is the column header visible?
	 *
	 * @return Whether the column is visible.
	 */
	@XmlElement
	public boolean isVisible()
	{
		return visible;
	}

	/** Set column header visibility.
	 *
	 * @param visible The new column visibility.
	 */
	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}

	/** Get value class of column header.
	 *
	 * @return The value class of the column.
	 */
	@XmlElement
	public String getValueclass()
	{
		return valueclass;
	}

	/** Set the column header value class.
	 *
	 * @param valueclass The column's new value class.
	 */
	public void setValueclass(String valueclass)
	{
		this.valueclass = valueclass;
	}

}
