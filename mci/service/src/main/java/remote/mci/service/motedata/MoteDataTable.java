package remote.mci.service.motedata;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/** Simple data table.
 *
 * A simple container for storing data from a database query so it can
 * be exchanged between the server and client.
 */
@XmlRootElement(name = "motedata")
public class MoteDataTable implements Serializable {

	private static final long serialVersionUID = -6733140214602762727L;
	private MoteDataHeader[] headers;
	private Object[][] data;

	/** Create a table with initial given size.
	 *
	 * @param rowCount	The number of rows in the table.
	 * @param colCount	The number of columns in the table.
	 */
	public MoteDataTable(int rowCount, int colCount)
	{
		headers = new MoteDataHeader[colCount];
		data = new Object[rowCount][colCount];
	}

	/** Dummy constructor to silence compiler warnings. */
	public MoteDataTable()
	{
		this(0, 0);
	}

	/** Get table column headers.
	 *
	 * @return The column header of the table.
	 */
	@XmlElementWrapper(name="headers")
	@XmlElementRef
	public MoteDataHeader[] getHeaders()
	{
		return headers;
	}

	/** Set table column headers.
	 *
	 * @param headers The column header.
	 */
	public void setHeaders(MoteDataHeader[] headers)
	{
		this.headers = headers;
	}

	/** Get table data.
	 *
	 * @return The object matrix.
	 */
	@XmlElementWrapper(name="motes")
	public Object[][] getData()
	{
		return data;
	}

	/** Set table data.
	 *
	 * @param data The object matrix.
	 */
	public void setData(Object[][] data)
	{
		this.data = data;
	}

}
