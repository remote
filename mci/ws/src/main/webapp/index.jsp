<html>
	<body>

		<h1>Re·Mote Testbed Mote Control Infrastructure Web Application</h1>

		<h2>Axis Web Services</h2>
		<ul>
		<li><a href="axis/authentication?wsdl">authentication</a></li>
		<li><a href="axis/motedata?wsdl">motedata</a></li>
		<li><a href="axis/moteaccess?wsdl">moteaccess</a></li>
		</ul>

		<h2>RESTful Web Services</h2>
		<ul>
		<li><a href="rest/application.wadl">all</a></li>
		</ul>

	</body>
</html>
