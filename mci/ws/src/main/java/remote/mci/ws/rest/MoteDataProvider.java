package remote.mci.ws.rest;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import remote.mci.service.motedata.MoteData;
import remote.mci.service.motedata.MoteDataTable;

/**
 * RESTful service for getting mote data.
 * @see {@link remote.mci.service.motedata.MoteData}
 */
@Path("/motedata")
public class MoteDataProvider {

	private final MoteData moteData = new MoteData();

	/**
	 * Get status information about all motes in the testbed.
	 *
	 * @see {@link remote.mci.service.motedata.MoteData#getMoteData}
	 */
	@GET
	@Produces({"application/json", "application/xml"})
	public MoteDataTable getMoteData(@CookieParam("session_id") @DefaultValue("")
				       String session_id) throws Exception
	{
		return moteData.getMoteData(session_id);
	}

}
