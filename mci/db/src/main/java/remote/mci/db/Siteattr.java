package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "siteattr")
@NamedQueries({ @NamedQuery(name = "Siteattr.findById", query = "SELECT s FROM Siteattr s WHERE s.id = :id") })
public class Siteattr implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Lob
	@Column(name = "val")
	private String val;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "siteattrId")
	private Collection<SiteSiteattr> siteSiteattrCollection;
	@JoinColumn(name = "siteattrtype_id", referencedColumnName = "id")
	@ManyToOne
	private Siteattrtype siteattrtypeId;

	public Siteattr()
	{
	}

	public Siteattr(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getVal()
	{
		return val;
	}

	public void setVal(String val)
	{
		this.val = val;
	}

	public Collection<SiteSiteattr> getSiteSiteattrCollection()
	{
		return siteSiteattrCollection;
	}

	public void setSiteSiteattrCollection(Collection<SiteSiteattr> siteSiteattrCollection)
	{
		this.siteSiteattrCollection = siteSiteattrCollection;
	}

	public Siteattrtype getSiteattrtypeId()
	{
		return siteattrtypeId;
	}

	public void setSiteattrtypeId(Siteattrtype siteattrtypeId)
	{
		this.siteattrtypeId = siteattrtypeId;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Siteattr))
			return false;
		Siteattr other = (Siteattr) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Siteattr[id=" + id + "]";
	}

}
