package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "moteattr")
@NamedQueries({ @NamedQuery(name = "Moteattr.findById", query = "SELECT m FROM Moteattr m WHERE m.id = :id") })
public class Moteattr implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Lob
	@Column(name = "val")
	private String val;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "moteattrId")
	private Collection<MoteMoteattr> moteMoteattrCollection;
	@JoinColumn(name = "moteattrtype_id", referencedColumnName = "id")
	@ManyToOne
	private Moteattrtype moteattrtypeId;

	public Moteattr()
	{
	}

	public Moteattr(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getVal()
	{
		return val;
	}

	public void setVal(String val)
	{
		this.val = val;
	}

	public Collection<MoteMoteattr> getMoteMoteattrCollection()
	{
		return moteMoteattrCollection;
	}

	public void setMoteMoteattrCollection(Collection<MoteMoteattr> moteMoteattrCollection)
	{
		this.moteMoteattrCollection = moteMoteattrCollection;
	}

	public Moteattrtype getMoteattrtypeId()
	{
		return moteattrtypeId;
	}

	public void setMoteattrtypeId(Moteattrtype moteattrtypeId)
	{
		this.moteattrtypeId = moteattrtypeId;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Moteattr))
			return false;
		Moteattr other = (Moteattr) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Moteattr[id=" + id + "]";
	}

}
