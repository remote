package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "session")
@NamedQueries({ @NamedQuery(name = "Session.findById", query = "SELECT s FROM Session s WHERE s.id = :id"), @NamedQuery(name = "Session.findByAuth", query = "SELECT s FROM Session s WHERE s.auth = :auth") })
public class Session implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "auth", nullable = false)
	private boolean auth;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "sessionId")
	private Collection<SessionSessionattr> sessionSessionattrCollection;
	@OneToMany(mappedBy = "currSessionId")
	private Collection<Mote> moteCollection;
	@OneToMany(mappedBy = "privSessionId")
	private Collection<Mote> moteCollection1;

	public Session()
	{
	}

	public Session(Integer id)
	{
		this.id = id;
	}

	public Session(Integer id, boolean auth)
	{
		this.id = id;
		this.auth = auth;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public boolean getAuth()
	{
		return auth;
	}

	public void setAuth(boolean auth)
	{
		this.auth = auth;
	}

	public Collection<SessionSessionattr> getSessionSessionattrCollection()
	{
		return sessionSessionattrCollection;
	}

	public void setSessionSessionattrCollection(Collection<SessionSessionattr> sessionSessionattrCollection)
	{
		this.sessionSessionattrCollection = sessionSessionattrCollection;
	}

	public Collection<Mote> getMoteCollection()
	{
		return moteCollection;
	}

	public void setMoteCollection(Collection<Mote> moteCollection)
	{
		this.moteCollection = moteCollection;
	}

	public Collection<Mote> getMoteCollection1()
	{
		return moteCollection1;
	}

	public void setMoteCollection1(Collection<Mote> moteCollection1)
	{
		this.moteCollection1 = moteCollection1;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Session))
			return false;
		Session other = (Session) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Session[id=" + id + "]";
	}

}
