package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "host")
@NamedQueries({ @NamedQuery(name = "Host.findById", query = "SELECT h FROM Host h WHERE h.id = :id"), @NamedQuery(name = "Host.findByDnsname", query = "SELECT h FROM Host h WHERE h.dnsname = :dnsname"), @NamedQuery(name = "Host.findByIp", query = "SELECT h FROM Host h WHERE h.ip = :ip") })
public class Host implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "dnsname")
	private String dnsname;
	@Column(name = "ip")
	private String ip;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "hostId")
	private Collection<Path> pathCollection;

	public Host()
	{
	}

	public Host(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getDnsname()
	{
		return dnsname;
	}

	public void setDnsname(String dnsname)
	{
		this.dnsname = dnsname;
	}

	public String getIp()
	{
		return ip;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public Collection<Path> getPathCollection()
	{
		return pathCollection;
	}

	public void setPathCollection(Collection<Path> pathCollection)
	{
		this.pathCollection = pathCollection;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Host))
			return false;
		Host other = (Host) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Host[id=" + id + "]";
	}

}
