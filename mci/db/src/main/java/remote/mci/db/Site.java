package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "site")
@NamedQueries({ @NamedQuery(name = "Site.findById", query = "SELECT s FROM Site s WHERE s.id = :id"), @NamedQuery(name = "Site.findBySitename", query = "SELECT s FROM Site s WHERE s.sitename = :sitename") })
public class Site implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "sitename", nullable = false)
	private String sitename;
	@JoinColumn(name = "position_id", referencedColumnName = "id")
	@ManyToOne
	private Position positionId;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "siteId")
	private Collection<SiteSiteattr> siteSiteattrCollection;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "siteId")
	private Collection<Path> pathCollection;
	@OneToMany(mappedBy = "siteId")
	private Collection<Mote> moteCollection;

	public Site()
	{
	}

	public Site(Integer id)
	{
		this.id = id;
	}

	public Site(Integer id, String sitename)
	{
		this.id = id;
		this.sitename = sitename;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getSitename()
	{
		return sitename;
	}

	public void setSitename(String sitename)
	{
		this.sitename = sitename;
	}

	public Position getPositionId()
	{
		return positionId;
	}

	public void setPositionId(Position positionId)
	{
		this.positionId = positionId;
	}

	public Collection<SiteSiteattr> getSiteSiteattrCollection()
	{
		return siteSiteattrCollection;
	}

	public void setSiteSiteattrCollection(Collection<SiteSiteattr> siteSiteattrCollection)
	{
		this.siteSiteattrCollection = siteSiteattrCollection;
	}

	public Collection<Path> getPathCollection()
	{
		return pathCollection;
	}

	public void setPathCollection(Collection<Path> pathCollection)
	{
		this.pathCollection = pathCollection;
	}

	public Collection<Mote> getMoteCollection()
	{
		return moteCollection;
	}

	public void setMoteCollection(Collection<Mote> moteCollection)
	{
		this.moteCollection = moteCollection;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Site))
			return false;
		Site other = (Site) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Site[id=" + id + "]";
	}

}
