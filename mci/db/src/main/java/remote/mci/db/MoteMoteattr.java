package remote.mci.db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "mote_moteattr")
@NamedQueries({ @NamedQuery(name = "MoteMoteattr.findById", query = "SELECT m FROM MoteMoteattr m WHERE m.id = :id") })
public class MoteMoteattr implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@JoinColumn(name = "moteattr_id", referencedColumnName = "id")
	@ManyToOne
	private Moteattr moteattrId;
	@JoinColumn(name = "mote_id", referencedColumnName = "id")
	@ManyToOne
	private Mote moteId;

	public MoteMoteattr()
	{
	}

	public MoteMoteattr(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Moteattr getMoteattrId()
	{
		return moteattrId;
	}

	public void setMoteattrId(Moteattr moteattrId)
	{
		this.moteattrId = moteattrId;
	}

	public Mote getMoteId()
	{
		return moteId;
	}

	public void setMoteId(Mote moteId)
	{
		this.moteId = moteId;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof MoteMoteattr))
			return false;
		MoteMoteattr other = (MoteMoteattr) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.MoteMoteattr[id=" + id + "]";
	}

}
