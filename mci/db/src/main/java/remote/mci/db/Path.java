package remote.mci.db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "path")
@NamedQueries({ @NamedQuery(name = "Path.findById", query = "SELECT p FROM Path p WHERE p.id = :id"), @NamedQuery(name = "Path.findByPath", query = "SELECT p FROM Path p WHERE p.path = :path") })
public class Path implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "path", nullable = false)
	private String path;
	@JoinColumn(name = "host_id", referencedColumnName = "id")
	@ManyToOne
	private Host hostId;
	@JoinColumn(name = "site_id", referencedColumnName = "id")
	@ManyToOne
	private Site siteId;

	public Path()
	{
	}

	public Path(Integer id)
	{
		this.id = id;
	}

	public Path(Integer id, String path)
	{
		this.id = id;
		this.path = path;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public Host getHostId()
	{
		return hostId;
	}

	public void setHostId(Host hostId)
	{
		this.hostId = hostId;
	}

	public Site getSiteId()
	{
		return siteId;
	}

	public void setSiteId(Site siteId)
	{
		this.siteId = siteId;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Path))
			return false;
		Path other = (Path) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Path[id=" + id + "]";
	}

}
