package remote.mci.db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "site_siteattr")
@NamedQueries({ @NamedQuery(name = "SiteSiteattr.findById", query = "SELECT s FROM SiteSiteattr s WHERE s.id = :id") })
public class SiteSiteattr implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@JoinColumn(name = "siteattr_id", referencedColumnName = "id")
	@ManyToOne
	private Siteattr siteattrId;
	@JoinColumn(name = "site_id", referencedColumnName = "id")
	@ManyToOne
	private Site siteId;

	public SiteSiteattr()
	{
	}

	public SiteSiteattr(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Siteattr getSiteattrId()
	{
		return siteattrId;
	}

	public void setSiteattrId(Siteattr siteattrId)
	{
		this.siteattrId = siteattrId;
	}

	public Site getSiteId()
	{
		return siteId;
	}

	public void setSiteId(Site siteId)
	{
		this.siteId = siteId;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof SiteSiteattr))
			return false;
		SiteSiteattr other = (SiteSiteattr) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.SiteSiteattr[id=" + id + "]";
	}

}
