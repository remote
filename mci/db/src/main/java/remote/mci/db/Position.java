package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "position")
@NamedQueries( { @NamedQuery(name = "Position.findById", query = "SELECT p FROM Position p WHERE p.id = :id"), @NamedQuery(name = "Position.findByX", query = "SELECT p FROM Position p WHERE p.x = :x"), @NamedQuery(name = "Position.findByY", query = "SELECT p FROM Position p WHERE p.y = :y"), @NamedQuery(name = "Position.findByZ", query = "SELECT p FROM Position p WHERE p.z = :z") })
public class Position implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "x", nullable = false)
	private float x;
	@Column(name = "y", nullable = false)
	private float y;
	@Column(name = "z", nullable = false)
	private float z;
	@OneToMany(mappedBy = "positionId")
	private Collection<Site> siteCollection;

	public Position()
	{
	}

	public Position(Integer id)
	{
		this.id = id;
	}

	public Position(Integer id, float x, float y, float z)
	{
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public float getZ()
	{
		return z;
	}

	public void setZ(float z)
	{
		this.z = z;
	}

	public Collection<Site> getSiteCollection()
	{
		return siteCollection;
	}

	public void setSiteCollection(Collection<Site> siteCollection)
	{
		this.siteCollection = siteCollection;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Position))
			return false;
		Position other = (Position) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Position[id=" + id + "]";
	}

}
