package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "moteattrtype")
@NamedQueries({ @NamedQuery(name = "Moteattrtype.findById", query = "SELECT m FROM Moteattrtype m WHERE m.id = :id"), @NamedQuery(name = "Moteattrtype.findBySortseq", query = "SELECT m FROM Moteattrtype m WHERE m.sortseq = :sortseq"), @NamedQuery(name = "Moteattrtype.findByName", query = "SELECT m FROM Moteattrtype m WHERE m.name = :name") })
public class Moteattrtype implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "sortseq")
	private Integer sortseq;
	@Column(name = "name", nullable = false)
	private String name;
	@Lob
	@Column(name = "description")
	private String description;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "moteattrtypeId")
	private Collection<Moteattr> moteattrCollection;

	public Moteattrtype()
	{
	}

	public Moteattrtype(Integer id)
	{
		this.id = id;
	}

	public Moteattrtype(Integer id, String name)
	{
		this.id = id;
		this.name = name;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getSortseq()
	{
		return sortseq;
	}

	public void setSortseq(Integer sortseq)
	{
		this.sortseq = sortseq;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Collection<Moteattr> getMoteattrCollection()
	{
		return moteattrCollection;
	}

	public void setMoteattrCollection(Collection<Moteattr> moteattrCollection)
	{
		this.moteattrCollection = moteattrCollection;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Moteattrtype))
			return false;
		Moteattrtype other = (Moteattrtype) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Moteattrtype[id=" + id + "]";
	}

}
