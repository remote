package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mote")
@NamedQueries({ @NamedQuery(name = "Mote.findById", query = "SELECT m FROM Mote m WHERE m.id = :id") })
public class Mote implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "moteId")
	private Collection<MoteMoteattr> moteMoteattrCollection;
	@JoinColumn(name = "curr_session_id", referencedColumnName = "id")
	@ManyToOne
	private Session currSessionId;
	@JoinColumn(name = "priv_session_id", referencedColumnName = "id")
	@ManyToOne
	private Session privSessionId;
	@JoinColumn(name = "site_id", referencedColumnName = "id")
	@ManyToOne
	private Site siteId;
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "moteId")
	private MoteReservation moteReservation;

	public Mote()
	{
	}

	public Mote(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Collection<MoteMoteattr> getMoteMoteattrCollection()
	{
		return moteMoteattrCollection;
	}

	public void setMoteMoteattrCollection(Collection<MoteMoteattr> moteMoteattrCollection)
	{
		this.moteMoteattrCollection = moteMoteattrCollection;
	}

	public Session getCurrSessionId()
	{
		return currSessionId;
	}

	public void setCurrSessionId(Session currSessionId)
	{
		this.currSessionId = currSessionId;
	}

	public Session getPrivSessionId()
	{
		return privSessionId;
	}

	public void setPrivSessionId(Session privSessionId)
	{
		this.privSessionId = privSessionId;
	}

	public Site getSiteId()
	{
		return siteId;
	}

	public void setSiteId(Site siteId)
	{
		this.siteId = siteId;
	}

	public MoteReservation getMoteReservation()
	{
		return moteReservation;
	}

	public void setMoteReservation(MoteReservation moteReservation)
	{
		this.moteReservation = moteReservation;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Mote))
			return false;
		Mote other = (Mote) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Mote[id=" + id + "]";
	}

}
