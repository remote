package remote.mci.db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mote_reservation")
@NamedQueries({ @NamedQuery(name = "MoteReservation.findById", query = "SELECT m FROM MoteReservation m WHERE m.id = :id") })
public class MoteReservation implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@JoinColumn(name = "mote_id", referencedColumnName = "id")
	@OneToOne
	private Mote moteId;
	@JoinColumn(name = "reservation_id", referencedColumnName = "id")
	@ManyToOne
	private Reservation reservationId;

	public MoteReservation()
	{
	}

	public MoteReservation(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Mote getMoteId()
	{
		return moteId;
	}

	public void setMoteId(Mote moteId)
	{
		this.moteId = moteId;
	}

	public Reservation getReservationId()
	{
		return reservationId;
	}

	public void setReservationId(Reservation reservationId)
	{
		this.reservationId = reservationId;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof MoteReservation))
			return false;
		MoteReservation other = (MoteReservation) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.MoteReservation[id=" + id + "]";
	}

}
