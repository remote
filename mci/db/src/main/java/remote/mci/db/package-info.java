/** MCI Database Persistence.
 *
 * Provides access to the MCI database using JPA.
 */
package remote.mci.db;
