package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sessionattr")
@NamedQueries({ @NamedQuery(name = "Sessionattr.findById", query = "SELECT s FROM Sessionattr s WHERE s.id = :id") })
public class Sessionattr implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Lob
	@Column(name = "val")
	private String val;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "sessionattrId")
	private Collection<SessionSessionattr> sessionSessionattrCollection;
	@JoinColumn(name = "sessionattrtype_id", referencedColumnName = "id")
	@ManyToOne
	private Sessionattrtype sessionattrtypeId;

	public Sessionattr()
	{
	}

	public Sessionattr(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getVal()
	{
		return val;
	}

	public void setVal(String val)
	{
		this.val = val;
	}

	public Collection<SessionSessionattr> getSessionSessionattrCollection()
	{
		return sessionSessionattrCollection;
	}

	public void setSessionSessionattrCollection(Collection<SessionSessionattr> sessionSessionattrCollection)
	{
		this.sessionSessionattrCollection = sessionSessionattrCollection;
	}

	public Sessionattrtype getSessionattrtypeId()
	{
		return sessionattrtypeId;
	}

	public void setSessionattrtypeId(Sessionattrtype sessionattrtypeId)
	{
		this.sessionattrtypeId = sessionattrtypeId;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Sessionattr))
			return false;
		Sessionattr other = (Sessionattr) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Sessionattr[id=" + id + "]";
	}

}
