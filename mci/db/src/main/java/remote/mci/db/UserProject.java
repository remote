package remote.mci.db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "user_project")
@NamedQueries({ @NamedQuery(name = "UserProject.findById", query = "SELECT u FROM UserProject u WHERE u.id = :id") })
public class UserProject implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@JoinColumn(name = "project_id", referencedColumnName = "id")
	@ManyToOne
	private Project projectId;
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	@ManyToOne
	private User userId;

	public UserProject()
	{
	}

	public UserProject(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Project getProjectId()
	{
		return projectId;
	}

	public void setProjectId(Project projectId)
	{
		this.projectId = projectId;
	}

	public User getUserId()
	{
		return userId;
	}

	public void setUserId(User userId)
	{
		this.userId = userId;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof UserProject))
			return false;
		UserProject other = (UserProject) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.UserProject[id=" + id + "]";
	}

}
