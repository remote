package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "reservation")
@NamedQueries({ @NamedQuery(name = "Reservation.findById", query = "SELECT r FROM Reservation r WHERE r.id = :id"), @NamedQuery(name = "Reservation.findByBegin", query = "SELECT r FROM Reservation r WHERE r.begin = :begin"), @NamedQuery(name = "Reservation.findByEnd", query = "SELECT r FROM Reservation r WHERE r.end = :end") })
public class Reservation implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "begin", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date begin;
	@Column(name = "end", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date end;
	@JoinColumn(name = "project_id", referencedColumnName = "id")
	@OneToOne
	private Project projectId;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "reservationId")
	private Collection<MoteReservation> moteReservationCollection;

	public Reservation()
	{
	}

	public Reservation(Integer id)
	{
		this.id = id;
	}

	public Reservation(Integer id, Date begin, Date end)
	{
		this.id = id;
		this.begin = begin;
		this.end = end;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Date getBegin()
	{
		return begin;
	}

	public void setBegin(Date begin)
	{
		this.begin = begin;
	}

	public Date getEnd()
	{
		return end;
	}

	public void setEnd(Date end)
	{
		this.end = end;
	}

	public Project getProjectId()
	{
		return projectId;
	}

	public void setProjectId(Project projectId)
	{
		this.projectId = projectId;
	}

	public Collection<MoteReservation> getMoteReservationCollection()
	{
		return moteReservationCollection;
	}

	public void setMoteReservationCollection(Collection<MoteReservation> moteReservationCollection)
	{
		this.moteReservationCollection = moteReservationCollection;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Reservation))
			return false;
		Reservation other = (Reservation) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Reservation[id=" + id + "]";
	}

}
