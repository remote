package remote.mci.db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "session_sessionattr")
@NamedQueries({ @NamedQuery(name = "SessionSessionattr.findById", query = "SELECT s FROM SessionSessionattr s WHERE s.id = :id") })
public class SessionSessionattr implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@JoinColumn(name = "sessionattr_id", referencedColumnName = "id")
	@ManyToOne
	private Sessionattr sessionattrId;
	@JoinColumn(name = "session_id", referencedColumnName = "id")
	@ManyToOne
	private Session sessionId;

	public SessionSessionattr()
	{
	}

	public SessionSessionattr(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Sessionattr getSessionattrId()
	{
		return sessionattrId;
	}

	public void setSessionattrId(Sessionattr sessionattrId)
	{
		this.sessionattrId = sessionattrId;
	}

	public Session getSessionId()
	{
		return sessionId;
	}

	public void setSessionId(Session sessionId)
	{
		this.sessionId = sessionId;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof SessionSessionattr))
			return false;
		SessionSessionattr other = (SessionSessionattr) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.SessionSessionattr[id=" + id + "]";
	}

}
