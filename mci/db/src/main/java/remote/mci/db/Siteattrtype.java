package remote.mci.db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "siteattrtype")
@NamedQueries( { @NamedQuery(name = "Siteattrtype.findById", query = "SELECT s FROM Siteattrtype s WHERE s.id = :id"), @NamedQuery(name = "Siteattrtype.findBySortseq", query = "SELECT s FROM Siteattrtype s WHERE s.sortseq = :sortseq"), @NamedQuery(name = "Siteattrtype.findByName", query = "SELECT s FROM Siteattrtype s WHERE s.name = :name") })
public class Siteattrtype implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "sortseq")
	private Integer sortseq;
	@Column(name = "name", nullable = false)
	private String name;
	@Lob
	@Column(name = "description")
	private String description;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "siteattrtypeId")
	private Collection<Siteattr> siteattrCollection;

	public Siteattrtype()
	{
	}

	public Siteattrtype(Integer id)
	{
		this.id = id;
	}

	public Siteattrtype(Integer id, String name)
	{
		this.id = id;
		this.name = name;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getSortseq()
	{
		return sortseq;
	}

	public void setSortseq(Integer sortseq)
	{
		this.sortseq = sortseq;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Collection<Siteattr> getSiteattrCollection()
	{
		return siteattrCollection;
	}

	public void setSiteattrCollection(Collection<Siteattr> siteattrCollection)
	{
		this.siteattrCollection = siteattrCollection;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Siteattrtype))
			return false;
		Siteattrtype other = (Siteattrtype) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "remote.mci.db.Siteattrtype[id=" + id + "]";
	}

}
