drop database if exists REMOTE;
create database REMOTE;

GRANT ALL PRIVILEGES ON REMOTE.* TO '${remote.jdbc.username}'@'localhost'
IDENTIFIED BY '${remote.jdbc.password}' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO '${remote.jdbc.username}'@'localhost'
IDENTIFIED BY '${remote.jdbc.password}' WITH GRANT OPTION;
GRANT CREATE ON *.* TO 'remote_admin'@'localhost'
IDENTIFIED BY '${remote.jdbc.password}';

GRANT ALL PRIVILEGES ON *.* TO '${remote.jdbc.username}'@'%'
IDENTIFIED BY '${remote.jdbc.password}' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON REMOTE.* TO '${remote.jdbc.username}'@'%'
IDENTIFIED BY '${remote.jdbc.password}' WITH GRANT OPTION;
GRANT CREATE ON *.* TO '${remote.jdbc.username}'@'%'
IDENTIFIED BY '${remote.jdbc.password}';
