set FOREIGN_KEY_CHECKS=0;
set @@sql_mode='';

drop table if exists host;
CREATE TABLE host (
	`id` int(11) unsigned NOT NULL auto_increment,
	`dnsname` varchar(255) default '',
	`ip` varchar(46) default '',
	PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

drop table if exists moteattr;
CREATE TABLE moteattr (
	`id` int(11) unsigned NOT NULL auto_increment,
	`moteattrtype_id` int(11) unsigned NOT NULL,
	`val` text(500) default '',
	PRIMARY KEY  (`id`),
	CONSTRAINT `fk_moteattr_type` FOREIGN KEY (`moteattrtype_id`) REFERENCES `moteattrtype` (`id`)
) ENGINE = InnoDB;

drop table if exists moteattrtype;
CREATE TABLE moteattrtype (
	`id` int(11) unsigned NOT NULL auto_increment,
	`sortseq` int(11) unsigned default 10,
	`name` varchar(10) NOT NULL,
	`description` text(500) /* default '' */,
	PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

drop table if exists mote_moteattr;
CREATE TABLE mote_moteattr (
	`id` int(11) unsigned NOT NULL auto_increment,
	`mote_id` int(11) unsigned NOT NULL,
	`moteattr_id` int(11) unsigned NOT NULL,
	PRIMARY KEY  (`id`),
	CONSTRAINT `fk_moteattr_moteid` FOREIGN KEY (`mote_id`) REFERENCES `mote` (`id`) ON DELETE CASCADE,
	CONSTRAINT `fk_moteattr_moteattrid` FOREIGN KEY (`moteattr_id`) REFERENCES `moteattr` (`id`),
	CONSTRAINT `cs_mote_moteattr_unique`
		UNIQUE INDEX `ndx_mote_moteattr_unique` (`mote_id`,`moteattr_id`)
) ENGINE = InnoDB;

drop table if exists mote;
CREATE TABLE mote (
	`id` int(11) unsigned NOT NULL auto_increment,
	`site_id` int(11) unsigned default NULL,
	`curr_session_id` int(11) unsigned default NULL,
	`priv_session_id` int(11) unsigned default NULL,
	PRIMARY KEY  (`id`),
	CONSTRAINT `fk_mote_site` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`),
	CONSTRAINT `fk_mote_curr_session` FOREIGN KEY (`curr_session_id`) REFERENCES `session` (`id`) ON DELETE SET NULL,
	CONSTRAINT `fk_mote_priv_session` FOREIGN KEY (`priv_session_id`) REFERENCES `session` (`id`) ON DELETE SET NULL
) ENGINE = InnoDB;

drop table if exists path;
CREATE TABLE path (
	`id` int(11) unsigned NOT NULL auto_increment,
	`host_id` int(11) unsigned NOT NULL,
	`path` varchar(255) NOT NULL,
	`site_id` int(11) unsigned NOT NULL default 1,
	PRIMARY KEY  (`id`),
	CONSTRAINT `fk_path_host` FOREIGN KEY (`host_id`) REFERENCES `host` (`id`) ON DELETE CASCADE,
	CONSTRAINT `fk_path_site` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`)
) ENGINE = InnoDB;

drop table if exists position;
CREATE TABLE position (
	`id` int(11) unsigned NOT NULL auto_increment,
	`x` float NOT NULL,
	`y` float NOT NULL,
	`z` float NOT NULL,
	PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

drop table if exists sessionattr;
CREATE TABLE sessionattr (
	`id` int(11) unsigned NOT NULL auto_increment,
	`sessionattrtype_id` int(11) unsigned NOT NULL,
	`val` text(500) /* default '' */,
	PRIMARY KEY  (`id`),
	CONSTRAINT `fk_sessionattr_type` FOREIGN KEY (`sessionattrtype_id`) REFERENCES `sessionattrtype` (`id`)
) ENGINE = InnoDB;

drop table if exists sessionattrtype;
CREATE TABLE sessionattrtype (
	`id` int(11) unsigned NOT NULL auto_increment,
	`sortseq` int(11) unsigned default 10,
	`name` varchar(10) NOT NULL,
	`description` text(500) /* default '' */,
	PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

drop table if exists session_sessionattr;
CREATE TABLE session_sessionattr (
	`id` int(11) unsigned NOT NULL auto_increment,
	`session_id` int(11) unsigned NOT NULL,
	`sessionattr_id` int(11) unsigned NOT NULL,
	PRIMARY KEY  (`id`),
	CONSTRAINT `fk_sessionattr_sessionid` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`) ON DELETE CASCADE,
	CONSTRAINT `fk_sessionattr_sessionattrid` FOREIGN KEY (`sessionattr_id`) REFERENCES `sessionattr` (`id`),
	CONSTRAINT `cs_session_sessionattr_unique`
		UNIQUE INDEX `ndx_session_sessionattr_unique` (`session_id`,`sessionattr_id`)
) ENGINE = InnoDB;

drop table if exists `session`;
CREATE TABLE `session` (
	`id` int(11) unsigned NOT NULL auto_increment,
	`auth` boolean NOT NULL default 0,
	PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

drop table if exists siteattr;

CREATE TABLE siteattr (
	`id` int(11) unsigned NOT NULL auto_increment,
	`siteattrtype_id` int(11) unsigned NOT NULL,
	`val` text(500) /* default '' */,
	PRIMARY KEY  (`id`),
	CONSTRAINT `fk_siteattr_type` FOREIGN KEY (`siteattrtype_id`) REFERENCES `siteattrtype` (`id`)
) ENGINE = InnoDB;

drop table if exists siteattrtype;
CREATE TABLE siteattrtype (
	`id` int(11) unsigned NOT NULL auto_increment,
	`sortseq` int(11) unsigned default 10,
	`name` varchar(10) NOT NULL,
	`description` text(500) /* default '' */,
	PRIMARY KEY  (`id`)
) ENGINE = InnoDB;

drop table if exists site_siteattr;
CREATE TABLE site_siteattr (
	`id` int(11) unsigned NOT NULL auto_increment,
	`site_id` int(11) unsigned NOT NULL,
	`siteattr_id` int(11) unsigned NOT NULL,
	PRIMARY KEY  (`id`),
	CONSTRAINT `fk_siteattr_siteid` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`) ON DELETE CASCADE,
	CONSTRAINT `fk_siteattr_siteattrid` FOREIGN KEY (`siteattr_id`) REFERENCES `siteattr` (`id`),
	CONSTRAINT `cs_site_siteattr_unique`
		UNIQUE INDEX `ndx_site_siteattr_unique` (`site_id`,`siteattr_id`)
) ENGINE = InnoDB;

drop table if exists site;
CREATE TABLE site (
	`id` int(11) unsigned NOT NULL auto_increment,
	`sitename` varchar(50) NOT NULL,
	`position_id` int(11) unsigned default NULL,
	PRIMARY KEY  (`id`),
	CONSTRAINT `fk_site_pos` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`)
) ENGINE = InnoDB;

set FOREIGN_KEY_CHECKS=1;
