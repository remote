set FOREIGN_KEY_CHECKS=0;

drop table if exists user;
CREATE TABLE user (
	`id` int(11) unsigned NOT NULL auto_increment,
	`login` varchar(255) NOT NULL,
	`password` varchar(41) NOT NULL,
	`name` varchar(255) NOT NULL,
	`email` varchar(255) NOT NULL,
	PRIMARY KEY  (`id`),
	CONSTRAINT `cs_user_login_unique`
		UNIQUE INDEX `ndx_user_login_unique` (`login`)
) ENGINE = InnoDB;

drop table if exists `project`;
CREATE TABLE `project` (
	`id` int(11) unsigned NOT NULL auto_increment,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY  (`id`),
	CONSTRAINT `cs_project_name_unique`
		UNIQUE INDEX `ndx_project_name_unique` (`name`)
) ENGINE = InnoDB;

drop table if exists user_project;
CREATE TABLE user_project (
	`id` int(11) unsigned NOT NULL auto_increment,
	`user_id` int(11) unsigned NOT NULL,
	`project_id` int(11) unsigned NOT NULL,
	PRIMARY KEY  (`id`),
	CONSTRAINT `fk_user_prj_usr` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
	CONSTRAINT `fk_user_prj_prj` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
	CONSTRAINT `cs_user_prj_unique`
		UNIQUE INDEX `ndx_user_prj_unique` (`user_id`,`project_id`)
) ENGINE = InnoDB;

set FOREIGN_KEY_CHECKS=1;
